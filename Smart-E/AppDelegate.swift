
//  AppDelegate.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 22/11/17.
//  Copyright © 2017 Hitesh Dhawan. All rights reserved.
//

import UIKit
import CoreData
import GooglePlaces
import GoogleMaps
import IQKeyboardManagerSwift
import UserNotifications
import Ligero
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

@objc protocol toUpdatedataViaNotification:class {
   @objc func updatedatawithnotifcation(dict:NSDictionary)
   // @objc func bookingcancelledByDriver()

}
   weak var delegateUpdateData:toUpdatedataViaNotification?
@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate {
    var viewnotification = UIView()
    var reachability: Reachability?
    var window: UIWindow?
    var activityView1  = UIView()
    var imageForR = UIImageView ()
    var str_mobile = ""
    var str_GmailId = ""
    var str_fullName = ""
    var str_email = ""
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        //Thread.sleep(forTimeInterval: 5.0)

        GMSServices.provideAPIKey(KEYS.GoogleKey)//"AIzaSyD8M2HwmULyJH_QCvh0Rqtwl9w5TniDBBs")
        GMSPlacesClient.provideAPIKey(KEYS.GoogleKey)//"AIzaSyANzHhSLuag5TipI6aJJHTYJHz-PFVYr1Q")
        
      //  IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.shared.enable = true
        GIDSignIn.sharedInstance().clientID = "52008339392-9n8tindbiknq29al4t7h6fvj3lp3jegh.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
       // self.createIndicator()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        // iOS 10 support
        application.applicationIconBadgeNumber = 0
     //   if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
//        }

        // iOS 9 support
//        else if #available(iOS 9, *) {
//            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
//            UIApplication.shared.registerForRemoteNotifications()
//        }

        //let merchantInfo = PPSMerchantInfo.init(merchantId: "M2306160483220675579140", appId: "5b0713a3e9a34fc6a585b3541823cc82") //for development
        let merchantInfo = PPSMerchantInfo.init(merchantId: "SMARTE", appId: "d465bd6b4a0d4862aaa3e0a8301fde5d") // for Production
        PhonePeSDK.shared().merchantInfo = merchantInfo
        PhonePeSDK.shared().enableLogging = true
    
      //  Helper.saveDataInNsDefault(object: [:] as AnyObject, key: Constants.bookingRideDetails)
     //  UserDefaults.standard.set("2", forKey: Constants.RideStatus)

        return true
    }
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        UserDefaults.standard.set(deviceTokenString, forKey: Constants.DevToken)

        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }

    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [AnyHashable : Any], completionHandler: @escaping () -> Void) {
        if let nav = self.window?.rootViewController as? ENSideMenuNavigationController{
            if nav.topViewController is RideStartViewController{
                let vc = nav.topViewController as! RideStartViewController
                vc.toUpdateUIAfterGotNotification()
            }

        }

        //print("Push notification received:)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo);
        let aps = userInfo[AnyHashable("aps")]! as! NSDictionary

        let customData = userInfo[AnyHashable("customData")] as? String
        //let customData = userInfo[AnyHashable("alert")] as? String
        if customData! == "From CMS"{
            UserDefaults.standard.set(true, forKey: "FromNotification")
            UserDefaults.standard.synchronize()


        }                        
        else
        {


        if (aps["alert"] as! String).uppercased() == "SMARTE"{


            viewnotification.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 60)
            viewnotification.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
            //tap.delegate = self
            viewnotification.addGestureRecognizer(tap)
            let lbl = UILabel()
            lbl.frame = CGRect(x: 10, y: 10, width: SCREEN_WIDTH-20, height: 60)
            lbl.font = UIFont.init(name: FONTS.Roboto_Regular, size: 15.0)
            lbl.textColor = UIColor.white
            lbl.numberOfLines = 0
            lbl.text = customData
            viewnotification.addSubview(lbl)

            //UIView.animate(withDuration: 1.0, delay: 1.5, options: .layoutSubviews, animations: , completion: nil)
            UIView.animate(withDuration: 1.0){
                self.window?.addSubview(self.viewnotification)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 30.0)
            {
                self.viewnotification.removeFromSuperview()
            }
        }
        else
        {
        let data: Data? = customData!.data(using: .utf8)
        var jsonResponse: [AnyHashable: Any]? = nil
        if let aData = data {
            jsonResponse = (try? JSONSerialization.jsonObject(with: aData, options: [])) as? [AnyHashable: Any]
            print(jsonResponse!)
        }
        let dic_customdata  = jsonResponse! as NSDictionary
        if (aps["alert"] as! String).uppercased() == "YOUR SMARTE  BOOKING CANCELLED"{

            let MessageCode = dic_customdata["MessageCode"] as! String
            let message = dic_customdata["message"] as! String

            print(message,MessageCode)
            if (application.applicationState == .active) {
                // Nothing to do if applicationState is Inactive, the iOS already displayed an alert view.
             //  delegateUpdateData?.updatedatawithnotifcation(dict: dic_customdata)
                if let nav = self.window?.rootViewController as? ENSideMenuNavigationController{
                    if nav.topViewController is RouteMapVC{
                        let vc = nav.topViewController as! RouteMapVC
                        vc.str_message = message
                        vc.bookingCancelledByDriver()
                    }
                }
            }
        }
       else if aps["alert"] as! String == "Completed Ride from  Driver"{
//            let BookingID = dic_customdata["BookingID"] as! String
//            let DriverID = dic_customdata["DriverID"] as! Int
//            let FinalFareamount = dic_customdata["FinalFareamount"] as! String
//            let RideType = dic_customdata["RideType"] as! String
//            let TransID = dic_customdata["TransID"] as! String
//            let TravelDistance = dic_customdata["TravelDistance"] as! String

            if (application.applicationState == .active) {
                // Nothing to do if applicationState is Inactive, the iOS already displayed an alert view.
                delegateUpdateData?.updatedatawithnotifcation(dict: dic_customdata)
            }
        }
       else if aps["alert"] as! String == "Payment Acceptance"{

            let MessageCode = dic_customdata["MessageCode"] as! String
            let message = dic_customdata["message"] as! String
            print(message,MessageCode)

            if (application.applicationState == .active) {
                // Nothing to do if applicationState is Inactive, the iOS already displayed an alert view.
                delegateUpdateData?.updatedatawithnotifcation(dict: dic_customdata)
                PKSAlertController.alert(Constants.appName, message: message);
            }
        }
        }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let nav = self.window?.rootViewController as? ENSideMenuNavigationController{
            if nav.topViewController is RideStartViewController{
                let vc = nav.topViewController as! RideStartViewController
                vc.toUpdateUIAfterGotNotification()
            }

        }

        //print("Push notification received:)")

    }
    func myMethodToHandleTap(_ sender: UITapGestureRecognizer) {

        DispatchQueue.main.async {
            self.viewnotification.removeFromSuperview()
        }
    }
    //MARK: SignIn Social

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication =  options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        let annotation = options[UIApplicationOpenURLOptionsKey.annotation]

        let googleHandler = GIDSignIn.sharedInstance().handle(
            url,
            sourceApplication: sourceApplication,
            annotation: annotation )

        let facebookHandler = FBSDKApplicationDelegate.sharedInstance().application (
            app,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation )

        return googleHandler || facebookHandler
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        let str_login: String = UserDefaults.standard.value(forKey: "loginfrom") as! String

        var handled = GIDSignIn.sharedInstance().handle(url as URL?, sourceApplication: sourceApplication,  annotation: annotation)

        var _: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
                                      UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]

        if str_login  == "Google"
        {
            handled = GIDSignIn.sharedInstance().handle(url as URL?, sourceApplication: sourceApplication,  annotation: annotation)
        }

        if str_login  == "Facebook"
        {
            handled =  FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return handled

    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            self.str_GmailId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
             str_fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            str_email = user.profile.email
            // ...
//            "Name": dict_user["name"] as! String,
//            "EmailID": dict_user["email"] as? String ?? "",

//            let dict_user = [
//                "name": fullName!,
//                "email": email,
//                "Socialid": "2",
//                "id": str_GmailId
//                ] as! [String : String]
            //self.AlertWithTextfield(message: "Enter Your Mobile Number")

            print("userId ",str_GmailId)
            print("IdToken ",idToken!)
            print("fullname ",str_fullName)
            print("givenname ", givenName!)
            print("familyName",familyName!)
            print("email ",str_email)
             self.callApiSocialRegister()
        }
    }

    //MARK:- for Alert with Textfield
    func AlertWithTextfield(message:String){


        let alertController = UIAlertController(title: Constants.appName, message: message, preferredStyle: .alert)
        alertController.addTextField { tf in
            tf.addTarget(self, action: #selector(self.textChange), for: .editingChanged)
            tf.keyboardType = .numberPad
        }

        let confirmAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//            let dict_user = [String:Any]() //UserDefaults.standard.object(forKey:"dict_user") as! [String:Any]
//            print(dict_user)
           self.callApiSocialRegister()


        })
        alertController.addAction(confirmAction)
        alertController.actions[0].isEnabled = false
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)

       // present(alertController, animated: true, completion: { _ in })
    }

    func callApiSocialRegister()
    {
        let urlstring = Constants.BASEURL+"SocialRegisterLoginV2"

        let dict_param = [
        "Name": self.str_fullName,
        "EmailID": self.str_email,
        "Socialid": "2",
        "FaceBookID": "",
        "GmailID": self.str_GmailId,

        "MobileNumber": self.str_mobile,
        "DeviceID": Constants.Device_UUID! ,
        "DeviceToken":UserDefaults.standard.value(forKey: Constants.DevToken)  as! String,
        "IPAddress": "IP Address",
        "DeviceName" : "IOS"
        ] as [String : Any]

        Server.PostDataInDictionary(urlstring, dict_data: dict_param as [String:AnyObject], completionHandler: {(response) in
        if (response != nil) && (response?["MessageCode"] as! String == "Act13"){
            print(response!)
            self.str_mobile =  response?["MobileNumber"] as! String
            DispatchQueue.main.async {
                let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                viewController.dict_Register = ["MobileNumber" : self.str_mobile]
                viewController.OTPScreen = true
                //viewController.toDecideWeatherOTPScreenORLoginScreen()
                UIApplication.shared.keyWindow?.rootViewController = viewController;
            }
        }
        else if response?["MessageCode"] as! String == "Act03"
        {
            DispatchQueue.main.async {
                let dict_profile = [
                    "Email": response?["Email"] as! String,
                    "Name": response?["Name"] as! String ,
                    "MobileNumber": response?["MobileNumber"] as! String,
                    "ImageURL": response?["ImagePath"] as? String ?? ""
                ]

                print("dict_profile ==== \(dict_profile.description)")
                Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
                UserDefaults.standard.set(response?["Userid"] as! NSNumber, forKey: Constants.USERID)
                // self.timer.invalidate()


                let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")

                UIApplication.shared.keyWindow?.rootViewController = viewController;
            }
        }
        else if response?["MessageCode"] as! String == "Act21"
        {
            DispatchQueue.main.async {
//                let dict_profile = [
//                    "Email": response?["Email"] as! String,
//                    "Name": response?["Name"] as! String ,
//                    "MobileNumber": response?["MobileNumber"] as! String,
//                    "ImageURL": response?["ImagePath"] as? String ?? ""
//                ]
//
//                print("dict_profile ==== \(dict_profile.description)")

//                Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
//                UserDefaults.standard.set(response?["Userid"] as! NSNumber, forKey: Constants.USERID)
                // self.timer.invalidate()
                self.AlertWithTextfield(message: "Enter Your Mobile Number")
//                let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
//
//                UIApplication.shared.keyWindow?.rootViewController = viewController;
            }
        }
        else{
            print(response!)
            PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
        }

    })

    }

    func textChange(_ sender: Any) {
        let tf = sender as! UITextField
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        //alert.actions[1].isEnabled = (tf.text != "")
        if Helper.isValidPhoneNumber(phoneNumber: tf.text!) {
            alert.actions[0].isEnabled = true
            str_mobile = tf.text!

        }
        else
        {
            alert.actions[0].isEnabled = false
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
      //  FBSDKAppEvents.activateApp()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
       // self.saveContext()
    }
    
    //MARK:- RETURN TOP VIEW
   func returnTopViewController()-> (UIViewController){
        let naviController = self.window?.rootViewController as! SideMenuNavigation;
        return  naviController.topViewController!
    }
 
    
    
    //MARK:- INDICATOR VIEW
    func createIndicator()
    {
        var imageForR = UIImageView ()
        var load = UILabel()
        activityView1 = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        activityView1.backgroundColor = UIColor.clear
        let back = UIImageView.init(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
        back.backgroundColor = UIColor.black
        back.alpha = 0.5
        activityView1.addSubview(back)
        
        let bgView = UIView.init(frame:CGRect(x: (ScreenSize.SCREEN_WIDTH - 120)/2, y: (ScreenSize.SCREEN_HEIGHT - 150)/2, width: 120, height: 150))
        bgView.backgroundColor = Colors.appNavigationColor
        bgView.layer.cornerRadius = 18.0
        bgView.layer.shadowColor = Colors.Lighttextcolor.cgColor
        bgView.clipsToBounds = true
        
        let bgImage = UIImageView.init(frame: bgView.frame) //(36,36, 48,48))
        bgImage.backgroundColor = UIColor.clear
        bgImage.image = UIImage.init(named: "backGroundImage")
        
        
        imageForR = UIImageView.init(frame: CGRect(x:25,y:25,width: 70,height:70))
        imageForR.image = UIImage.init(named: "mouse.png")
        imageForR.backgroundColor = UIColor.clear
        
        
        load = UILabel.init(frame: CGRect(x:0,y: 120,width: 120,height: 25))
        load.text = "LOADING..."
        load.textColor = UIColor.white
        load.font = UIFont.boldSystemFont(ofSize: 14.0)
        load.textAlignment = NSTextAlignment.center
        
        bgView.addSubview(bgImage)
        bgView.addSubview(imageForR)
        bgView.addSubview(load)
        
        activityView1.addSubview(bgView)
        self.window?.addSubview(activityView1)
        self.window?.sendSubview(toBack: activityView1)
    }
 
    func startIndicator(){
        
        DispatchQueue.main.async() {
            self.window!.addSubview(self.activityView1)
            self.rotate360Degrees()
        }
    }
    
    func stopIndicator(){
        DispatchQueue.main.async() {
            self.imageForR.layer.removeAllAnimations()
            self.window?.willRemoveSubview(self.activityView1)
            self.activityView1.removeFromSuperview()
        }
    }
    
    func rotate360Degrees(duration: CFTimeInterval = 3.5, completionDelegate: AnyObject? = nil){
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * -2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = 125
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        imageForR.layer.add(rotateAnimation, forKey: nil)
    }
    

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Smart_E")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


