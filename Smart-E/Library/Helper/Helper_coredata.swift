//
//  Helper_coredata.swift
//  MakeNMake
//
//  Created by Hitesh Dhawan on 16/10/17.
//  Copyright © 2017 Hitesh Dhawan. All rights reserved.
//

import UIKit
import CoreData

class Helper_coredata: NSObject{

    
 /*static func save(planID:Float,planName:String,description:String,status:Bool,cost:Float,visitRequired:Bool,onlyForAdmin:Bool,NoofCalls:Int32, ServiceGroupName:String,ServiceParentId:Int32 , parentCategory:String, rateType:String,additionalrate:Float)
    {
        let request = NSFetchRequest<Category>(entityName: "Plan")
        request.returnsObjectsAsFaults = false
        do {
             let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            // 1
            let managedContext = appDelegate.persistentContainer.viewContext
            // 2
            let entity =
                NSEntityDescription.entity(forEntityName: "Plan", in: managedContext)!
            let category = NSManagedObject(entity: entity, insertInto: managedContext)
            // 3
            Plan.setValue(planID, forKeyPath: "categoryName")
            category.setValue(planName, forKeyPath: "serviceID")
            category.setValue(description, forKeyPath: "notes")
            category.setValue(status, forKeyPath: "parentCategory")
            category.setValue(cost, forKeyPath: "rate")
            category.setValue(rateType, forKeyPath: "ratetype")
            category.setValue(visitRequired, forKeyPath: "visitRequired")
            category.setValue(onlyForAdmin, forKeyPath: "onlyForAdmin")
            category.setValue(NoofCalls, forKeyPath: "NoofCalls")
            category.setValue(ServiceGroupName, forKeyPath: "ServiceGroupName")
            category.setValue(ServiceParentId, forKeyPath: "ServiceParentId")
            category.setValue(parentCategory, forKeyPath: "parentCategory")
            category.setValue(additionalrate, forKeyPath: "serviceGroupName")
            //category.setValue(serviceGroupId, forKeyPath: "serviceGroupId")
            do {
                try managedContext.save()
               // arr_category1.append(category)
              
                
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    catch let error as NSError {
    print("Could not fetch. \(error), \(error.userInfo)")
    }
}

}
*/
    


/*func save(categoryName:String,serviceGroupName:String,serviceGroupId:String,date:String,time:String,timeSlotId:String,addressId:String,address:String, serviceId:String,notes:String , parentCategory:String, rate:String,ratetype:String,serviceDesc:String,serviceGroup:String,serviceName:String ,totalPrice:String, quantity:String) {
        
        
        let request = NSFetchRequest<Category>(entityName: "Category")
        request.returnsObjectsAsFaults = false
        do {
            let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
            
            
            if searchResults.count == 0 {
                guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                    return
                }
                
                // 1
                let managedContext = appDelegate.persistentContainer.viewContext
                // 2
                let entity =
                    NSEntityDescription.entity(forEntityName: "Category", in: managedContext)!
                let category = NSManagedObject(entity: entity, insertInto: managedContext)
                // 3
                category.setValue(categoryName, forKeyPath: "categoryName")
                category.setValue(serviceId, forKeyPath: "serviceID")
                category.setValue(notes, forKeyPath: "notes")
                category.setValue(parentCategory, forKeyPath: "parentCategory")
                category.setValue(rate, forKeyPath: "rate")
                category.setValue(ratetype, forKeyPath: "ratetype")
                category.setValue(serviceDesc, forKeyPath: "serviceDesc")
                category.setValue(serviceGroup, forKeyPath: "serviceGroup")
                category.setValue(serviceName, forKeyPath: "serviceName")
                category.setValue(quantity, forKeyPath: "quantity")
                category.setValue(totalPrice, forKeyPath: "totalPrice")
                category.setValue(date, forKeyPath: "date")
                category.setValue(serviceGroupName, forKeyPath: "serviceGroupName")
                category.setValue(serviceGroupId, forKeyPath: "serviceGroupId")
                category.setValue(time, forKeyPath: "time")
                category.setValue(timeSlotId, forKeyPath: "timeSlotId")
                category.setValue(address, forKeyPath: "address")
                category.setValue(addressId, forKeyPath: "addressId")
                
                
                // 4
                do {
                    try managedContext.save()
                    arr_category1.append(category)
                    let script = self.parent as! BookingVC
                    script.lbl_cartvalue.text = "\(arr_category1.count)"
                    
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }
                
            }
            else
            {
                var arr:[String] = []
                for task in searchResults {
                    
                    arr.append(task.serviceID!)
                }
                
                if arr .contains(serviceId) {
                    
                }
                else
                {
                    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                        return
                    }
                    
                    // 1
                    let managedContext = appDelegate.persistentContainer.viewContext
                    // 2
                    let entity =
                        NSEntityDescription.entity(forEntityName: "Category", in: managedContext)!
                    let category = NSManagedObject(entity: entity, insertInto: managedContext)
                    // 3
                    category.setValue(categoryName, forKeyPath: "categoryName")
                    category.setValue(serviceId, forKeyPath: "serviceID")
                    category.setValue(notes, forKeyPath: "notes")
                    category.setValue(parentCategory, forKeyPath: "parentCategory")
                    category.setValue(rate, forKeyPath: "rate")
                    category.setValue(ratetype, forKeyPath: "ratetype")
                    category.setValue(serviceDesc, forKeyPath: "serviceDesc")
                    category.setValue(serviceGroup, forKeyPath: "serviceGroup")
                    category.setValue(serviceName, forKeyPath: "serviceName")
                    category.setValue(quantity, forKeyPath: "quantity")
                    category.setValue(totalPrice, forKeyPath: "totalPrice")
                    category.setValue(date, forKeyPath: "date")
                    category.setValue(serviceGroupName, forKeyPath: "serviceGroupName")
                    category.setValue(serviceGroupId, forKeyPath: "serviceGroupId")
                    category.setValue(time, forKeyPath: "time")
                    category.setValue(timeSlotId, forKeyPath: "timeSlotId")
                    
                    category.setValue(address, forKeyPath: "address")
                    category.setValue(addressId, forKeyPath: "addressId")
                    
                    // 4
                    do {
                        
                        try managedContext.save()
                        arr_category1.append(category)
                        let script = self.parent as! BookingVC
                        script.lbl_cartvalue.text = "\(arr_category1.count)"
                        
                    } catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }
                    
                }
                
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }*/
    
}


