    //
    //  Constant.swift
    //  MyRecipeApp
    //
    //  Created by Surbhi on 24/10/16.
    //  Copyright © 2016 Neuronimbus. All rights reserved.
    //

    import Foundation
    import UIKit
    import GoogleMaps

    struct KEYS {
        static let GoogleKey = "AIzaSyDZ4ertCpls2SRH30JJcAFqnPWbC133vq0"
        //AIzaSyCUSI2vBTEa9MVWXqyCc1aJYVpXNcDV8a0
    }

    struct State {
        let name: String
        let long: CLLocationDegrees
        let lat: CLLocationDegrees
    }

    struct ScreenSize {
        static let SCREEN               = UIScreen.main.bounds
        static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }

    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    }

    struct iOSVersion {
        static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
        static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
        static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
        static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
        static let iOS10 = (iOSVersion.SYS_VERSION_FLOAT >= 10.0 && iOSVersion.SYS_VERSION_FLOAT < 11.0)

    }


    struct DeviceOrientation {
        static let IS_portT = UIDevice.current.orientation.isPortrait
        static let IS_Land = UIDevice.current.orientation.isLandscape

    }

    struct Constants {

        static let pushNotification = "com.neuro.pushNotification"
        static let pushNotificationToUpdate = "sideMenuPushNotification"
        static let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        static let mainStoryboard2: UIStoryboard = UIStoryboard(name: "Main2",bundle: nil)
        //////// for  Live
            static let BASEURL = "https://app.getsmarte.in/api/PassengerAccount/"
            static let BASEURLImage = "https://app.getsmarte.in/"
        //////// for  development

//        static let BASEURL =   "http://13.127.170.151/api/PassengerAccount/"
//        static let BASEURLImage = "http://13.127.170.151/"
        //static let BASEURL = "http://13.127.89.106/api/PassengerAccount/"
        //static let BASEURLImage = "http://13.127.89.106/"

        static let Device_UUID = UIDevice.current.identifierForVendor?.uuidString.replacingOccurrences(of: "-", with: "")
        static let PROFILE = "data"
        static let PASSWORD = "pwd_Blank"
        static let SelectedMenuIndex = "selectedIndex"
        static let DevToken = "deviceToken"
        static let AUTHID = "auth_id"
        static let AUTHType = "auth_type"
        static let DEVId = "device_id"
        static let USERID = "id"
        static let isLoggedIn = "loggedin"

        static let MOB = "mobile"
        static let NAME = "name"
        static let PROFILEPIC = "profile_pic"
        static let PASS = "password"
        static let STATUS = "success"
        static let EMAIL = "email"

        static let AuthType = "auth_type"
        static var AuthId = "auth_id"
        static let RUPEE = "₹"

        static let MESS =  "messageCode"
        static let Message =  "message"
        static let USERNAme = "userName"
        static let USERPASSWORD = "USERPASSWORD"

        static let header = "header"
        static let appDelegate = UIApplication.shared.delegate as! AppDelegate

        static let SERVICEGROUPNAME = "serviceGroupname"
        static let SERVICEGROUPID = "serviceGroupId"
        static let REFRENCEID =  "referenceId"
        static let appName = "SmartE"

        static let RideStatus = "rideStatus"
        static let RideDetail = "rideDetail"
        static let bookingRideDetails = "bookingRideDetails"
        static let SAVEADDRESS =  "saveAddress"

    }

    struct MethodName {
        static let Register                          =  "Register"
        static let Login                             =  "LoginRegister"//"Login"
        static let mobileValidateRegister            =  "MobileValidateOTP"
        static let mobileValidateLogin               =  "LoginValidateOTP"
        static let passengerBookingReport            =  "PassengerBookingReport"
    }


    struct Colors {
        static let appNavigationColor =  UIColor(red: 158.0/255.0, green: 158.0/255.0, blue: 160.0/255.0, alpha: 1.0)
        static let Apptextcolor =  UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0) // also to be used for TextColor
        static let appThemeColorText = UIColor(red: 24.0/255.0, green:  176.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        static let appThemeColorTextgray = UIColor(red: 33.0/255.0, green:  33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        static let AppSubtextcolor = UIColor(red: 78.0/255.0, green:  55.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        static let WindowBackgroudColor = UIColor(red: 78.0/255.0, green:  55.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        static let ShadowColor = UIColor(red: 85.0/255.0, green:  85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
        static let Lighttextcolor = UIColor(red: 240.0/255.0, green:  240.0/255.0, blue: 245.0/255.0, alpha: 1.0)
        static let textPlaceHolderGray = UIColor.init(red: 154.0/255.0, green: 153.0/255.0, blue: 151.0/255.0, alpha: 1.0)
     }

    struct FONTS {
        static let Helvetica_Regular = "HelveticaNeue"
        static let Helvetica_Light = "HelveticaNeue-Light"
        static let Helvetica_Medium = "HelveticaNeue-Medium"
        static let Helvetica_Bold = "HelveticaNeue-Bold"
        static let Helvetica_Thin = "HelveticaNeue-Thin"
        static let Helvetica_UltraLight = "HelveticaNeue-UltraLight"
        static let Helvetica_Italic = "HelveticaNeue-Italic"
        static let Helvetica_LightItalic = "HelveticaNeue-LightItalic"
        static let Helvetica_MediumItalic = "HelveticaNeue-MediumItalic"
        static let Helvetica_ThinItalic = "HelveticaNeue-ThinItalic"
        static let Helvetica_UltraLightItalic = "HelveticaNeue-UltraLightItalic"
        static let Helvetica_BoldItalic = "HelveticaNeue-BoldItalic"
        static let Helvetica_CondensedBlack = "HelveticaNeue-CondensedBlack"
        static let Helvetica_CondensedBold = "HelveticaNeue-CondensedBold"
        static let Roboto_Light = "Roboto-Light"
        static let Roboto_Medium = "Roboto-Medium"
        static let Roboto_Regular = "Roboto-Regular"
        static let Roboto_Italic = "Roboto-Italic"
        static let Roboto_LightItalic = "Roboto-LightItalic"
        static let Roboto_MediumItalic = "Roboto-MediumItalic"
        static let Roboto_Black = "Roboto-Black"
        static let Roboto_Bold = "Roboto-Bold"
        static let Roboto_BlackItalic = "Roboto-BlackItalic"
        static let Roboto_BoldItalic = "Roboto-BoldItalic"
        static let Roboto_Thin = "Roboto-Thin"
        static let Roboto_ThinItalic = "Roboto-ThinItalic"
        static let RobotoCondensed_Light = "RobotoCondensed-Light"
        static let RobotoCondensed_Regular = "RobotoCondensed-Regular"
        static let RobotoCondensed_Italic = "RobotoCondensed-Italic"
        static let RobotoCondensed_LightItalic = "RobotoCondensed-LightItalic"
        static let RobotoCondensed_Bold = "RobotoCondensed-Bold"
        static let RobotoCondensed_BoldItalic = "RobotoCondensed-BoldItalic"
    }




