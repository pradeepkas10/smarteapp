//
//  PPSUserInfo.h
//  Ligero
//
//  Created by Jatin Arora on 23/05/17.
//  Copyright © 2017 PhonePe Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class PPSFetchUserDetailsRequest;
@class PPSFetchUserDetailsResult;
@class PPSTransactionCompletionResult;
@class PPSTransactionRequest;

NS_ASSUME_NONNULL_BEGIN

typedef void (^PPSFetchUserDetailsCompletion)(PPSFetchUserDetailsRequest *, PPSFetchUserDetailsResult *);
typedef void (^PPSTransactionCompletion)(PPSTransactionRequest *, PPSTransactionCompletionResult *);


@interface PPSMerchantInfo : NSObject

@property (nonatomic, nonnull, readonly, copy) NSString *merchantId;
@property (nonatomic, nonnull, readonly, copy) NSString *appId;


/*!
 
 @brief Object for Merchant Information
 
 @param merchantId (Mandatory) Unique Merchant ID assigned to the merchant by PhonePe
 
 @param appId (Mandatory) Unique app ID assigned to the merchant by PhonePe
 
 @return An instance of Merchant Information object
 
 **/

- (nonnull instancetype)initWithMerchantId:(nonnull NSString *)merchantId
                                     appId:(nonnull NSString *)appId;

@end


@interface PPSTransactionRequest: NSObject

@property (nonatomic, nonnull, readonly, copy) NSString *base64EncodedBody;
@property (nonatomic, nonnull, readonly, copy) NSString *apiEndPoint;
@property (nonatomic, nonnull, readonly, copy) NSString *checksum;
@property (nonatomic, nullable, readonly, strong) NSDictionary<NSString *, NSString *> *headers;


/*!
 
 @brief Object for transaction request
 
 @discussion This method accepts the parameters required to setup a transaction request object
 
 @param base64EncodedBody (Mandatory) Base64 representation of the serailized json
 
 @param apiEndPoint The PhonePe end point that SDK should hit
 
 @param checksum Checksum for this transaction received by querying your server
 
 @return An instance of transaction request object
 
 **/
    
- (nonnull instancetype)initWithBase64EncodedBody:(NSString *)base64EncodedBody
                                      apiEndPoint:(NSString *)apiEndPoint
                                         checksum:(NSString *)checksum
                                          headers:(nullable NSDictionary<NSString *, NSString *> *)headers;


@end



@interface PPSTransactionCompletionResult: NSObject

/*!
 Error object describing the failure, nil if the transaction was a success
 **/

@property (nonatomic, nullable, strong) NSError *error;

/*!
 A context sent by PhonePe server
 **/

@property (nonatomic, nonnull, strong) NSDictionary <NSString *, id> *context;

/*!
 A boolean value describing success or failure of transaction. Success means that the transaction went through on the client but you still have to query your merchant server to know the actual transaction status.
 **/

@property (nonatomic, assign) BOOL successful;

@end



@interface PPSFetchUserDetailsRequest: NSObject

@property (nonatomic, nonnull, readonly, copy) NSString *apiEndPoint;
@property (nonatomic, nonnull, readonly, copy) NSString *checksum;



/*!
 
 @brief Object for user details request
 
 @discussion This method accepts the parameters required to setup a FetchUserDetailsRequest object
 
 @param apiEndPoint The PhonePe end point that SDK should hit
 
 @param checksum Checksum for this transaction received by querying your server
 
 @return An instance of PPSFetchUserDetailsRequest object
 
 **/


- (nonnull instancetype)initWithApiEndPoint:(NSString *)apiEndPoint
                                   checksum:(NSString *)checksum;

@end


@interface PPSFetchUserDetailsResult: NSObject

@property (nonatomic, nullable, strong) NSError *error;

@property (nonatomic, assign) BOOL isUserOnPhonePe;

@end


NS_ASSUME_NONNULL_END

