//
//  PhonePeSDK.h
//  Ligero
//
//  Created by Jatin Arora on 23/05/17.
//  Copyright © 2017 PhonePe Private Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PhonePeSDKModels.h"
@class PhonePeSDK;
@class PPSTransactionCompletionResult;

NS_ASSUME_NONNULL_BEGIN

@interface PhonePeSDK : NSObject

/*!
 (Mandatory) Unique Merchant Info assigned to the merchant by PhonePe
 */
@property (nonatomic, strong) PPSMerchantInfo *merchantInfo;


/*!
 (Optional) Set this to YES to enableLogging in Xcode. Logging is disabled in final SDK by default.
 */
@property (nonatomic, assign) BOOL enableLogging;
    

/*!
 (Optional) Set this whenever a deep-link opens your application.
 PhonePe selectively logs this information only for Phonepe promotion related links and doesn't log any other URLs
 */
@property (nonatomic, nullable, strong) NSURL *deeplinkUrl;


/*!
 @return Returns the shared singleton instance.
 */

+ (nonnull instancetype)shared;

/*!
 
 @brief Opens up the PhonePe SDK screen
 
 @discussion This method accepts the parameters required to start a PhonePe transaction
             (every call to PhonePe server is a transaction) and then
             presents a ViewController with the desired flow
 
 @param request object for this transaction
 
 @param presentingViewController ViewController to present the PhonePe screen on
 
 @param animated Show PhonePe screen with or without animation
 
 @param completion callback with the original request and the transaction result
 
 **/


- (void)startPhonePeTransactionRequest:(PPSTransactionRequest *)request
                      onViewController:(UIViewController *)presentingViewController
                              animated:(BOOL)animated
                            completion:(PPSTransactionCompletion)completion;




/*!
 
 @brief Fetches merchants specific user details
 
 @discussion This method accepts the parameters required to fetch the details of a user
 
 @param request Request object for this transaction
 
 @param completion completion callback with the original request and the result
 
 **/


- (void)fetchUserDetailsForRequest:(PPSFetchUserDetailsRequest *)request
                        completion:(PPSFetchUserDetailsCompletion)completion;

/*!
 
 @brief Cleans up data related to user. You may call this in case user on your app logs out.
 
 **/

- (void)logout;



/*!
 
 @brief Ligero's framework version (Usually separated by 2 dots, for eg. 0.8.7)
 
 @return Returns the ligero framework version
 
 **/

- (NSString *)version;


@end


NS_ASSUME_NONNULL_END
