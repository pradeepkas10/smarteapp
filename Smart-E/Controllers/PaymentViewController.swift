//
//  PaymentViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 18/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import Ligero

class PaymentViewController: UIViewController {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var tblView: UITableView!

    var arrPaymentOption = ["by cash" , "upi"]
    

    var comingFromPayment = false


    var transactionID = ""
    var amountToPay = ""
    var BookingID = ""
    var dict_profile = [String:String]()
    var transLogId = ""
    var StrUserID = ""

    var Status = ""


    //MARK:- view controller life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.StrUserID = String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
        dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as! [String : String]
        tblView.tableFooterView = UIView()
        if comingFromPayment{
            arrPaymentOption.remove(at: 0)
            DispatchQueue.main.async{
            self.lblHeader.text = "Rs. \(self.amountToPay) SmartE Fare Is Pending"
            self.tblView.reloadData()
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
                UIApplication.shared.statusBarStyle = .lightContent
               self.setNeedsStatusBarAppearanceUpdate()
                delegateProfile?.SetData()
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- button actions

    @IBAction func btnBackClicked(_ sender: UIButton) {
        if comingFromPayment{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnPaymentClicked(sender:UIButton)
    {
        self.paymentWithPhonePE()
    }

    func paymentWithPhonePE(){

        //        var dict = [String:Any]()
        //        dict["merchantId"] = "M2306160483220675579140"
        //        dict["transactionId"] = String(arc4random_uniform(100000))
        //        dict["amount"] = 100
        //        dict["merchantOrderId"] = "1"
        //        dict["merchantUserId"] = "1dfer2"
        //        dict["message"] = "new ORder"
        //        dict["mobileNumber"] = "8130514507"
        //        dict["email"] = "nandani2719mishra@gmail.com"
        //        dict["shortName"] = "NandaniMishra"
        //
        //        print(dict)
        //
        //
        //        let dictBase64Data: Data = try! JSONSerialization.data(withJSONObject:dict,options: JSONSerialization.WritingOptions.prettyPrinted)
        //        let dictBase64 = dictBase64Data.base64EncodedString()
        //
        //        print(dictBase64)
        //
        //        let saltProcee = (dictBase64 + "/v3/debit" + "8289e078-be0b-484d-ae60-052f117f8deb").sha256()
        //        print(saltProcee)
        //
        //        let checkSum = saltProcee + "###1"
        //
        //        print(checkSum)

        let headers = ["Content-type": "application/json"]

        toGetSaltCheckSumValue { (SaltCheckSum, base64,apiEndPoints,TranslogID) in
            if SaltCheckSum != nil && base64 != nil{
                let req = PPSTransactionRequest.init(base64EncodedBody: base64!, apiEndPoint: apiEndPoints!, checksum: SaltCheckSum!, headers: headers)

                PhonePeSDK.shared().startPhonePeTransactionRequest(req, on: self, animated: true) { (request, result) in
                    print(result.context)
                    self.Status = result.context["statusCode"] as! String
                    print(result.description)
                    print(result.successful)
                    self.toGetTransactionStatus(completionHandler: { (response) in
                        print("respse")
                    })
                }
            }
        }
    }

    func toGetTransactionStatus(completionHandler:@escaping (_ result:[String:AnyObject]?)->Void){
Helper.ToShowIndicator()
        let url_str = Constants.BASEURL+"PassengerPaymentRecieptance"
        var dict = [String:Any]()
        dict["UserID"] = StrUserID
        dict["BookingID"] = BookingID
        dict["TransID"] = transactionID
        dict["TransLogID"] = transLogId


        Server.PostDataInDictionary(url_str, dict_data: dict as Dictionary<String, AnyObject>, completionHandler: { (response) in
            print(response!)
            if (response != nil) && (response?["payResponseCode"] as! String == "PAYMENT_SUCCESS") {
                print(response!)

                     //PKSAlertController.alert(Constants.appName, message:response?["paymentState"] as! String)
                   // self.navigationController?.popViewController(animated: true)
                    Helper.ToHideIndicator()
                DispatchQueue.main.async {


                    PKSAlertController.alert(Constants.appName, message: (response?["paymentState"] as! String), buttons: ["OK"]) { (alertAction, index) in
                        print("index === \(index)")
                        if index == 0 {

                            self.dismiss(animated: true, completion: nil)                        }
                    }

                    Helper.ToHideIndicator()
                    // lblStatus.text = "YOUR RIDE IS COMPLETED"
                    UIView.animate(withDuration: 0.5){
                        self.view.layoutIfNeeded()
                    }
 }
            }else{

                PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : "Your transaction is failed, Please try again later.")
            }
        })
    }



    func toGetSaltCheckSumValue(completionHandler:@escaping (_ saltValue:String?,_ base64Str:String?,_ apiEndPoints:String?, _ TranslogID:String?)->Void){
        if Helper.isConnectedToNetwork(){
            Helper.ToShowIndicator()
            //  transactionID = String(arc4random_uniform(100000))
            let amount =   Double(self.amountToPay)!*100
            print(amount)
            var dict = [String:Any]()
            //  dict["merchantId"] = "M2306160483220675579140"
            dict["transactionId"] = self.transactionID
            dict["amount"] = "1" //"\(forTrailingZero(temp: amount))"
            dict["merchantOrderId"] = self.BookingID
            dict["merchantUserId"] = StrUserID
            dict["message"] = "iOS"
            dict["mobileNumber"] = dict_profile["MobileNumber"]
            dict["email"] = dict_profile["Email"]
            dict["shortName"] = dict_profile["Name"]
            print(dict)

            let url_str = Constants.BASEURL + "GetCheckSum"
            print(dict)
            Server.PostDataInDictionary(url_str, dict_data: dict as Dictionary<String, AnyObject>, completionHandler: { (response) in
               Helper.ToHideIndicator()
                if (response != nil) && (response?["MessageCode"] as! String == "CHECK01") {
                    print(response!)
                    DispatchQueue.main.async {
                        completionHandler(response?["Checksum"] as? String, response?["Base64"] as? String,response?["apiEndPoints"] as? String,response?["TranslogID"] as? String)
                        self.transLogId = String(describing:(response?["TranslogID"] as! NSNumber))
                    }
                }else{
                    completionHandler(nil, nil,nil,nil)

                    PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
                }
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
    }

    func forTrailingZero(temp: Double) -> String {
        let tempVar = String(format: "%g", temp)
        return tempVar
    }
}


extension PaymentViewController :UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1//arrPaymentOption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath)
        cell.selectionStyle = .none
        let lbl = cell.viewWithTag(100) as! UILabel
        let imgView = cell.viewWithTag(101) as! UIImageView
        print(imgView)

        lbl.text = "PhonePe/BHIM UPI" //arrPaymentOption[indexPath.item].uppercased()
        //imgView.backgroundColor = Colors.appThemeColorText
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 0{
            UIApplication.shared.statusBarStyle = .default
            setNeedsStatusBarAppearanceUpdate()
            paymentWithPhonePE()
        }
    }
    
}

//extension String {
//
//    func sha256() -> String{
//        if let stringData = self.data(using: String.Encoding.utf8) {
//            return hexStringFromData(input: digest(input: stringData as NSData))
//        }
//        return ""
//    }
//
//    private func digest(input : NSData) -> NSData {
//        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
//        var hash = [UInt8](repeating: 0, count: digestLength)
//        CC_SHA256(input.bytes, UInt32(input.length), &hash)
//        return NSData(bytes: hash, length: digestLength)
//    }
//
//    private  func hexStringFromData(input: NSData) -> String {
//        var bytes = [UInt8](repeating: 0, count: input.length)
//        input.getBytes(&bytes, length: input.length)
//
//        var hexString = ""
//        for byte in bytes {
//            hexString += String(format:"%02x", UInt8(byte))
//        }
//
//        return hexString
//    }
//
//}

