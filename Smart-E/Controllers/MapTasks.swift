//
//  MapTasks.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 24/11/17.
//  Copyright © 2017 Hitesh Dhawan. All rights reserved.
//

import UIKit

class MapTasks: NSObject {
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    var lookupAddressResults: Dictionary<String, AnyObject>!
    var fetchedFormattedAddress: String!
    var fetchedAddressLongitude: Double!
    var fetchedAddressLatitude: Double!
    override init() {
        super.init()
    }
  /*
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
  
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress
       

             let defaultConfigObject = URLSessionConfiguration.default
             let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
            
          
            
            
            let task = defaultSession.dataTask(with: geocodeURLString as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        print(json)
                        completionHandler(json[Any] , true)
                    
                        // handle json...
                        let status = json["status"] as! String
                        
                        if status == "OK" {
                            let allResults = json["results"] as! Array<Dictionary<String, AnyObject>>
                            self.lookupAddressResults = allResults[0] as Dictionary<String, AnyObject>
                            
                            // Keep the most important values.
                            self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as! String
                            let geometry = self.lookupAddressResults["geometry"] as! Dictionary<String, AnyObject>
                            self.fetchedAddressLongitude = ((geometry["location"] as! Dictionary<String, AnyObject>)["lng"] as! NSNumber).doubleValue
                            self.fetchedAddressLatitude = ((geometry["location"] as! Dictionary<String, AnyObject>)["lat"] as! NSNumber).doubleValue
                            
                            completionHandler(status, true)
                        }
                        else {
                            completionHandler(status, false)
                        }
    
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                }
            })
            
            task.resume()
           
        }
        else {
            completionHandler("No valid address.", false)
        }
    }
*/
}
