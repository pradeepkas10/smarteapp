 //
//  SplashView.swift
//  MyRecipeApp
//
//  Created by Surbhi on 26/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit

class SplashView: UIViewController,ENSideMenuDelegate {

    var superCatArray = ["kitchen","laundry","living-space"]
    var categoryIDArray = Array<NSInteger>()
    var productIDArray = Array<NSInteger>()
    var imagArray = Array<Any>()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.isNavigationBarHidden = true
    

    }
    
    @IBAction func toggleSideMenu(_ sender: AnyObject) {
        toggleSideMenuView()
    }
 /*   func getImagesToDisplay(){
        
        if Reachability.isConnectedToNetwork() {
           // Constants.appDelegate.startIndicator()
            
            let urlStr = "http://neuronimbusinteractive.com/screensaver/get_image.php"
            Server.getRequestWithURL(urlString: urlStr, completionHandler: { (response) in
             //   Constants.appDelegate.stopIndicator()
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    var imgArray = response["data_ios"] as! Array<AnyObject>
                    
                    for img in imgArray{
                        let dc = img as! Dictionary<String,Any>
                        let imgStr = dc["image"] as! String
                        self.imagArray.append(imgStr)

                    }
                    
                    DispatchQueue.main.async {
                        
                        if imgArray.count > 1{
                            
                            let newImg = imgArray[imgArray.count - 1] as! Dictionary<String,Any>
                            let newmm = newImg["image"] as! String
                            
                            let newvid = ["video" : "http://neuronimbusinteractive.com/screensaver/videos/Whirlpool%20AC.mp4",
                                          "image" : newmm]
                            
                            let count = imgArray.count
                            imgArray.insert(newvid as AnyObject, at: count)
                            
                          //  UserDefaults.standard.set(imgArray, forKey: Constants.SCREENSAVER)
                            self.getSubCategoriesforType("kitchen")
                        }
                    }
                }
                else {
                    self.getImagesToDisplay()
                }
                
            })
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }

    func downloadImages(){
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        for obj in 0 ..< self.imagArray.count{
            
            let imgname = self.imagArray[obj] as! String
            
            let imageUrl = URL.init(string: imgname)
            if !FileManager.default.fileExists(atPath: documentsDirectoryURL.appendingPathComponent((imageUrl!.lastPathComponent)).path) {
                //Helper.downloadImageLinkAndCreateAsset(imgname)
            }
        }
        
        GoToLogin()
    }
    
    
    func GoToLogin() {
        
        SVProgressHUD.dismiss()
        
        if UserDefaults.standard.string(forKey: Constants.UserID) == nil || UserDefaults.standard.string(forKey: Constants.UserID) == ""  {
            
                DispatchQueue.main.async(execute: {
                    
                    let login = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView")
                    UIApplication.shared.keyWindow?.rootViewController = login
                })
        }
        else {
            DispatchQueue.main.async(execute: {

                let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                UIApplication.shared.keyWindow?.rootViewController = viewController
            })
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    
    
    func getSubCategoriesforType(_ typeStr : String){ // Kitchen // Laundry // Living Spaces
        
        if Reachability.isConnectedToNetwork() {
           // Constants.appDelegate.startIndicator()
            
            let urlStr = Constants.BASEURL + "categories"
            let param = "type=\(typeStr)"
            
            Server.postRequestWithURL(urlStr, paramString: param, completionHandler: { (response) in
                
                
                if response[Constants.STATE]?.int32Value  == 1 {
                    
                    let subCatArray = response["data"] as! Array<AnyObject>
                    
                    UserDefaults.standard.set(subCatArray, forKey: typeStr.uppercased())
                    for cat in subCatArray{
                        let dict = cat as! Dictionary<String,Any>
                        let catdID = dict["id"] as! NSInteger
                        let imageStr = dict["image"] as! String
                        self.imagArray.append(imageStr)
                        
                        
                        if self.categoryIDArray.contains(catdID) == false{
                            self.categoryIDArray.append(catdID)
                        }
                    }
                    
                    if typeStr == "kitchen" {
                   //     Constants.appDelegate.stopIndicator()
                        self.getSubCategoriesforType("laundry")
                    }
                    else if typeStr == "laundry" {
                     //   Constants.appDelegate.stopIndicator()
                        self.getSubCategoriesforType("living-spaces")
                    }
                    else{
                       // UserDefaults.standard.set(self.categoryIDArray, forKey: Constants.catID_array)
                        //Constants.appDelegate.stopIndicator()
                        print(self.imagArray.count)
                        print(self.imagArray)
                        if self.imagArray.count > 0 {
                            self.downloadImages()
                        }
                    }
                }
                else {
                    
                    self.getSubCategoriesforType(typeStr)
//                    Constants.appDelegate.stopIndicator()
//                    let mess = response["message"] as! String
//                    CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"", "message":mess])
                }
            })
            
        }
        else {
            CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
        }
    }
*/
        
}
