
//
//  LoginView.swift
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import SafariServices

class LoginView: UIViewController, GIDSignInUIDelegate {
    
    @IBOutlet weak var txtMobileNum: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnNewUserSignIn: UIButton!
    @IBOutlet weak var lbl_timer:  UILabel!
    @IBOutlet weak var scr_pagecontrol: UIScrollView!
    @IBOutlet weak var pageControl:  UIPageControl!
    @IBOutlet weak var lbl_mobileNumberHeight:NSLayoutConstraint!
    @IBOutlet weak var Txt_mobileNumberHeight:NSLayoutConstraint!
    weak var actionToEnable : UIAlertAction?
    var counter = 30
    var timer : Timer!
    var str_mobile = ""
    var str_GmailId = ""
    var str_FacebookId = ""
    var str_SocialId = ""
    var str_UserName = ""
    @IBOutlet weak var logButton:UIView!
    @IBOutlet weak var viewforSocialLogin:UIView!
    @IBOutlet weak var txt_privacyPolicy:UITextView!
    var OTPScreen = false
    
    var dict_Register = [String:String]()
    
    
    
    //MARK:- View Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
        //tap.delegate = self
        txt_privacyPolicy.addGestureRecognizer(tap)
        self.logButton.isHidden = false
        GIDSignIn.sharedInstance().uiDelegate = self
        // Helper.saveDataInNsDefault(object: dict_Register as AnyObject, key: Constants.PROFILE)
        var text = [String]()
        text = ["Share rides are available only on fixed route.","For pickup/drop outside fixed route, Kindly use the solo ride option."]

        var frame: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        for index in 0..<2 {
            frame.origin.x = self.scr_pagecontrol.frame.size.width * CGFloat(index)
            frame.size = self.scr_pagecontrol.frame.size
            let subView = UIView(frame: frame)
            subView.backgroundColor = .clear//colors[index]
            self.scr_pagecontrol.addSubview(subView)
            let lbl_description = UILabel()
            lbl_description.frame = CGRect(x: 0, y: 0, width: subView.frame.width, height: subView.frame.height)
            lbl_description.text = text[index]
            lbl_description.backgroundColor = .clear
            lbl_description.numberOfLines = 0
            lbl_description.textAlignment = .center
            lbl_description.font = UIFont.init(name: FONTS.Roboto_Regular, size: 13.0)
            subView.addSubview(lbl_description)
        }
        self.scr_pagecontrol.isPagingEnabled = true
        self.scr_pagecontrol.contentSize = CGSize(width: self.scr_pagecontrol.frame.size.width * 2, height: self.scr_pagecontrol.frame.size.height)
        Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        self.pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    }
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("view will appear")
        print(OTPScreen)
//        if OTPScreen{
//
//            toDecideWeatherOTPScreenORLoginScreen()
//        }else{
//            lbl_mobileNumberHeight.constant = 50.0
//            Txt_mobileNumberHeight.constant = 20.0
//            lbl_timer.isHidden = true
//            btnResendOTP.isHidden = true
//            logButton.isHidden = false
//            ////btnNewUserSignIn.isHidden = false
//            btnSignIn.setTitle("SIGN IN", for: UIControlState.normal)
//            scrollView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: 155), animated: true)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(OTPScreen)
        self.txt_privacyPolicy.isHidden = false
        self.viewforSocialLogin.isHidden = false
        scrollView.contentSize = CGSize.init(width: SCREEN_WIDTH * 2, height: 155)
        scrollView.isUserInteractionEnabled = true
        Helper.giveShadowToView(btnSignIn)
        if OTPScreen{

            toDecideWeatherOTPScreenORLoginScreen()
        }else{
            lbl_mobileNumberHeight.constant = 50.0
            Txt_mobileNumberHeight.constant = 20.0
            lbl_timer.isHidden = true
            btnResendOTP.isHidden = true
            logButton.isHidden = false
            ////btnNewUserSignIn.isHidden = false
            btnSignIn.setTitle("SIGN IN", for: UIControlState.normal)
            scrollView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: SCREEN_WIDTH, height: 155), animated: true)
        }
    }
    override func viewDidDisappear(_ animated: Bool){
        super.viewDidDisappear(animated)
        if OTPScreen {
            timer.invalidate()
        }
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    @objc func myMethodToHandleTap(_ sender: UITapGestureRecognizer) {

        let myTextView = sender.view as! UITextView
        let layoutManager = myTextView.layoutManager

        // location of tap in myTextView coordinates and taking the inset into account
        var location = sender.location(in: myTextView)
        location.x -= myTextView.textContainerInset.left;
        location.y -= myTextView.textContainerInset.top;

        // character index at tap location
        let characterIndex = layoutManager.characterIndex(for: location, in: myTextView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

        // if index is valid then do something.
        if characterIndex < myTextView.textStorage.length {

            // print the character index
            print("character index: \(characterIndex)")
            if characterIndex >= 41 && characterIndex <= 56{
                print("terms")
                self.openLink(url: "http://getsmarte.in/")
            }

            if characterIndex >= 61 && characterIndex <= 73{
                print("Privacy Policy")
                self.openLink(url: "http://getsmarte.in/")
            }

        }
    }

    func openLink(url: String) {
        if let url = URL(string: url) {
            let svc = SFSafariViewController(url: url)
            UIApplication.shared.statusBarStyle = .default
            setNeedsStatusBarAppearanceUpdate()

            if #available(iOS 10.0, *) {
                svc.preferredControlTintColor = Colors.appThemeColorText
            } else {
                // Fallback on earlier versions
            }
            svc.delegate = self as? SFSafariViewControllerDelegate
            present(svc, animated: true, completion: {
                if #available(iOS 10.0, *) {
                    svc.preferredControlTintColor = Colors.appThemeColorText
                } else {
                    // Fallback on earlier versions
                }
            })
        }
    }
    func callApiSocialRegister()
    {
        let dict_user = UserDefaults.standard.object(forKey:"dict_user") as! [String:Any]
        print(dict_user)
        let urlstring = Constants.BASEURL+"SocialRegisterLoginV2"

        let dict_param = [
        "Name": dict_user["name"] as! String,
        "EmailID": dict_user["email"] as? String ?? "",
        "Socialid": self.str_SocialId,
        "FaceBookID": self.str_FacebookId,
        "GmailID": self.str_GmailId,

        "MobileNumber": self.str_mobile,
        "DeviceID": Constants.Device_UUID! ,
        "DeviceToken":UserDefaults.standard.value(forKey: Constants.DevToken)  as! String,
        "IPAddress": "IP Address",
        "DeviceName" : "IOS"
        ] as [String : Any]

        Server.PostDataInDictionary(urlstring, dict_data: dict_param as [String:AnyObject], completionHandler: {(response) in
            if (response != nil) && (response?["MessageCode"] as! String == "Act13"){
                print(response!)
                self.str_mobile =  response?["MobileNumber"] as! String
                DispatchQueue.main.async {
                    let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                    viewController.dict_Register = ["MobileNumber" : self.str_mobile]
                    viewController.OTPScreen = true
                    //viewController.toDecideWeatherOTPScreenORLoginScreen()
                    UIApplication.shared.keyWindow?.rootViewController = viewController;
                }
            }
            else if response?["MessageCode"] as! String == "Act03"
            {
                DispatchQueue.main.async {
                    let dict_profile = [
                        "Email": response?["Email"] as! String,
                        "Name": response?["Name"] as! String ,
                        "MobileNumber": response?["MobileNumber"] as! String,
                        "ImageURL": response?["ImagePath"] as? String ?? ""
                    ]

                    print("dict_profile ==== \(dict_profile.description)")

                    Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
                    UserDefaults.standard.set(response?["Userid"] as! NSNumber, forKey: Constants.USERID)
                    // self.timer.invalidate()


                    let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")

                    UIApplication.shared.keyWindow?.rootViewController = viewController;
                }
            }
            else if response?["MessageCode"] as! String == "Act21"
            {
                DispatchQueue.main.async {
                    //                let dict_profile = [
                    //                    "Email": response?["Email"] as! String,
                    //                    "Name": response?["Name"] as! String ,
                    //                    "MobileNumber": response?["MobileNumber"] as! String,
                    //                    "ImageURL": response?["ImagePath"] as? String ?? ""
                    //                ]
                    //
                    //                print("dict_profile ==== \(dict_profile.description)")

                    //                Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
                    //                UserDefaults.standard.set(response?["Userid"] as! NSNumber, forKey: Constants.USERID)
                    // self.timer.invalidate()
                    self.AlertWithTextfield(message: "Enter Your Mobile Number")
                    //                let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                    //
                    //                UIApplication.shared.keyWindow?.rootViewController = viewController;
                }
            }
            else{
                print(response!)
                PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
            }

        })

    }

    //MARK:- for Alert with Textfield
    func AlertWithTextfield(message:String){
        let alertController = UIAlertController(title: Constants.appName, message: message, preferredStyle: .alert)
        alertController.addTextField { tf in
            tf.addTarget(self, action: #selector(self.textChange), for: .editingChanged)
            tf.keyboardType = .numberPad
        }

        let confirmAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in

            self.callApiSocialRegister()
        })
        alertController.addAction(confirmAction)
        alertController.actions[0].isEnabled = false
        self.present(alertController, animated: true)

       // present(alertController, animated: true, completion: { _ in })
    }

    @objc func textChange(_ sender: Any) {
        let tf = sender as! UITextField
        var resp : UIResponder! = tf
        while !(resp is UIAlertController) { resp = resp.next }
        let alert = resp as! UIAlertController
        //alert.actions[1].isEnabled = (tf.text != "")
        if Helper.isValidPhoneNumber(phoneNumber: tf.text!) {
            alert.actions[0].isEnabled = true
            str_mobile = tf.text!

        }
        else
        {
            alert.actions[0].isEnabled = false
        }
    }
    //MARK:- for autoscroll scrollview
    @objc func moveToNextPage (){

        let pageWidth:CGFloat = self.scr_pagecontrol.frame.width
        let maxWidth:CGFloat = pageWidth * 2
        let contentOffset:CGFloat = self.scr_pagecontrol.contentOffset.x

        var slideToX = contentOffset + pageWidth

        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
            pageControl.currentPage = 0
        }
        else
        {
            pageControl.currentPage = 1
        }
        self.scr_pagecontrol.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scr_pagecontrol.frame.height), animated: true)
    }
    //MARK:- Button Actions
    //
    //    @IBAction func btnSignInClicked(sender:UIButton){
    //        self.view.endEditing(true)
    //        if btnSignIn.titleLabel?.text == "SIGN IN" {
    //            if Helper.isValidPhoneNumber(phoneNumber: txtMobileNum.text!){
    //                logButton.isHidden = true
    //                toLoginWithMobileNumber()
    //                //btnNewUserSignIn.isHidden = true
    //            }
    //            else{
    //                PKSAlertController.alert(Constants.appName, message: "Please Enter a Valid Mobile Number.")
    //            }
    //        }
    //        else
    //        {
    //            guard txtOTP.text!.count > 0 else{
    //                PKSAlertController.alert(Constants.appName, message: "Please Enter OTP Number.")
    //                return
    //            }
    //            if Helper.isConnectedToNetwork(){
    //            var url_str = Constants.BASEURL +  MethodName.mobileValidateLogin
    //            url_str = url_str + "?MobileNumber=" + "\(dict_Register["MobileNumber"]!)" + "&otp=" + "\(txtOTP.text!)"
    //
    //            print("url_str === \(url_str)")
    //
    //            Helper.ToShowIndicator()
    //            Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
    //                Helper.ToHideIndicator()
    //                if (response?.count)! > 1  && response?["MessageCode"] as! String == "Act11"{
    ////                    self.timer.invalidate()
    //                        DispatchQueue.main.async {
    //                            let dict_profile = [
    //                                "Email": response?["Email"] as! String,
    //                                "Name": response?["Name"] as! String ,
    //                                "MobileNumber": response?["MobileNumber"] as! String,
    //                                "ImageURL": response?["ImagePath"] as? String ?? ""
    //                            ]
    //                            print("resulttt ==== \(response?.description)")
    //                            print("dict_profile ==== \(dict_profile.description)")
    //
    //                            Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
    //                            UserDefaults.standard.set(response?["Userid"] as! NSNumber, forKey: Constants.USERID)
    //                           // self.timer.invalidate()
    //
    //
    //                            let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
    //                            UIApplication.shared.keyWindow?.rootViewController = viewController;
    //                        }
    //                    }else{
    //                        PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
    //                    }
    //            })
    //            }else{
    //                PKSAlertController.alertForNetwok()
    //
    //            }
    //
    //        }
    //
    //    }

    @IBAction func googlePlusButtonTouchUpInside(sender: AnyObject) {

      // UserDefaults.standard.set("Google", forKey: "loginfrom")

        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func btnSignInClicked(sender:UIButton){
        self.view.endEditing(true)
        if btnSignIn.titleLabel?.text == "SIGN IN" {
            if Helper.isValidPhoneNumber(phoneNumber: txtMobileNum.text!){
               // logButton.isHidden = true
                toLoginWithMobileNumber()
                ////btnNewUserSignIn.isHidden = true
            }
            else{
                PKSAlertController.alert(Constants.appName, message: "Please Enter a Valid Mobile Number.")
            }
        }
        else
        {
            guard txtOTP.text!.count > 0 else{
                PKSAlertController.alert(Constants.appName, message: "Please Enter OTP Number.")
                return
            }
            if Helper.isConnectedToNetwork(){
                var url_str = Constants.BASEURL +  MethodName.mobileValidateLogin
                url_str = url_str + "?MobileNumber=" + "\(dict_Register["MobileNumber"]!)" + "&otp=" + "\(txtOTP.text!)"

                print("url_str === \(url_str)")

                Helper.ToShowIndicator()
                Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
                    Helper.ToHideIndicator()
                    if (response?.count)! > 1  && response?["MessageCode"] as! String == "Act11"{
                        //                    self.timer.invalidate()
                        DispatchQueue.main.async {
                            let dict_profile = [
                                "Email": response?["Email"] as! String,
                                "Name": response?["Name"] as! String ,
                                "MobileNumber": response?["MobileNumber"] as! String,
                                "ImageURL": response?["ImagePath"] as? String ?? ""
                            ]
                            print("resulttt ==== \(String(describing: response?.description))")
                            print("dict_profile ==== \(dict_profile.description)")

                            Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
                            UserDefaults.standard.set(response?["Userid"] as! NSNumber, forKey: Constants.USERID)
                            // self.timer.invalidate()


                            let viewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
                            UIApplication.shared.keyWindow?.rootViewController = viewController;
                        }
                    }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                    }
                })
            }else{
                PKSAlertController.alertForNetwok()

            }

        }

    }
    
    
    
    //["MessageCode": Act11, "Email": poll@yopmail.com, "Name": pradeep, "Userid": 47, "haserror": 0, "message": OTP Verified Succssfully, "MobileNumber": 9650854236]
    

    @IBAction func btnRegisterClicked(sender:UIButton){
        let login = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RegisterView") as! RegisterView
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    
    @IBAction func btnResendOTP(sender:UIButton){
        print("resend button")
        toLoginWithMobileNumber()
    }
    //MARK: for Facebook
    @IBAction func btn_facebook(sender:Any)
    {
        if Helper.isConnectedToNetwork() {
            Constants.appDelegate.startIndicator()
            DispatchQueue.main.async()
                {
                    let logManager = FBSDKLoginManager.init()
                    //[FBSDKSettings sdkVersion]
                    print(FBSDKSettings.sdkVersion())
                    logManager.loginBehavior = FBSDKLoginBehavior.web
                    logManager.logOut()
                    logManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
                        if error != nil {
                            Constants.appDelegate.stopIndicator()
                            PKSAlertController.alert(Constants.appName, message: "Some error Occurred !")
                        }
                        else if result?.isCancelled == true {
                            Constants.appDelegate.stopIndicator()
                            //                        print("Is Cancelled")
                        }
                        else {
                            if result?.token.tokenString != "" || result?.token != nil {
                                self.fetchUserInfo()
                            }
                        }
                    }
            }
        }
        else {
            // CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            PKSAlertController.alert(Constants.appName, message: "Please check your internet connection.")
        }
    }


    func fetchUserInfo()
    {

        if Helper.isConnectedToNetwork() {


            let req = FBSDKGraphRequest.init(graphPath: "me" , parameters: ["fields": "id, name, picture.width(500).height(500), email, gender,first_name, last_name"])
            DispatchQueue.main.async()
                {
                    req?.start {(connection, result, error) in
                        if(error == nil) {

                            let resDict = result as! Dictionary<String,AnyObject>
                            self.checkUserAuthonticationWithUserInfo(result: resDict as NSDictionary)

                        }
                        else {
                            Constants.appDelegate.stopIndicator()
                            // CustomAlertController.showAlert(on: self, with: Simple, andAttributes: ["title":"Culineria","message":"Some error occured"])
                            PKSAlertController.alert(Constants.appName, message: "Some error occured")
                        }
                    }
            }
        }
        else {
            Constants.appDelegate.stopIndicator()
            // CustomAlertController.showAlert(on: self, with: Simple, andAttributes:["title": "", "message":"Please check your internet connection."])
            PKSAlertController.alert(Constants.appName, message: "Please check your internet connection.")
        }

    }

    func checkUserAuthonticationWithUserInfo(result : NSDictionary) {

        if Helper.isConnectedToNetwork() {

            let userId = result["id"] as! String
            //    let fullName = result["name"] as! String
            var email = result["email"] as? String

            if email == nil {
                email = ""
            }

            let picDic = result["picture"] as! Dictionary<String,AnyObject>
            let picData = picDic["data"] as! Dictionary<String,AnyObject>
            //      let profileImage = picData["url"] as! String

            print("userName is aaa", result )
            let fbDetails = result
            print("userName is", fbDetails)
            self.str_FacebookId = fbDetails["id"] as! String
            self.str_SocialId = "1"
            UserDefaults.standard.set(fbDetails, forKey: "dict_user")
            //...
           // self.AlertWithTextfield(message: "Please Enter Your Mobile Number")
            self.callApiSocialRegister()
            //            var message = "Please Enter Your Mobile Number"
            //            let alertController = UIAlertController(title: Constants.appName, message: message, preferredStyle: .alert)
            //            alertController.addTextField { tf in
            //                tf.addTarget(self, action: #selector(self.textChange), for: .editingChanged)
            //            }
            //
            //            let confirmAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //            let dict_user = UserDefaults.standard.object(forKey:"dict_user")
            //
            //            })
            //            alertController.addAction(confirmAction)
            //            alertController.actions[0].isEnabled = false
            //            present(alertController, animated: true, completion: { _ in })



        }
        else {

            Constants.appDelegate.stopIndicator()
            PKSAlertController.alert(Constants.appName, message: "Please check your internet connection.")
        }
    }

    //    func textChange(_ sender: Any) {
    //        let tf = sender as! UITextField
    //        var resp : UIResponder! = tf
    //        while !(resp is UIAlertController) { resp = resp.next }
    //        let alert = resp as! UIAlertController
    //        //alert.actions[1].isEnabled = (tf.text != "")
    //        if Helper.isValidPhoneNumber(phoneNumber: tf.text!) {
    //            alert.actions[0].isEnabled = true
    //        }
    //        else
    //        {
    //            alert.actions[0].isEnabled = false
    //        }
    //    }




    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    //MARK:- extraa functions

    //    func toLoginWithMobileNumber(){
    //    if Helper.isConnectedToNetwork(){
    //        Helper.ToShowIndicator()
    //        let dict_param = [
    //            "MobileNumber": txtMobileNum.text!,
    //            "DeviceID": Constants.Device_UUID ,
    //            "DeviceToken":UserDefaults.standard.value(forKey: Constants.DevToken),
    //            "UserName": "",
    //            "DeviceName" : "IOS"
    //        ]
    //
    //        let url_str = Constants.BASEURL + MethodName.Login
    //        print(dict_param)
    //        Server.PostDataInDictionary(url_str, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
    //        Helper.ToHideIndicator()
    //        if (response != nil) && (response?["MessageCode"] as! String == "Act07" || response?["MessageCode"] as! String == "Act13"){
    //                    print(response!)
    //                    DispatchQueue.main.async {
    //                        self.dict_Register = ["MobileNumber" : self.txtMobileNum.text!]
    //                        Helper.ToHideIndicator()
    //                        self.toDecideWeatherOTPScreenORLoginScreen()
    //                    }
    //                }else{
    //            PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
    //                }
    //        })
    //        }else{
    //            PKSAlertController.alertForNetwok()
    //
    //        }
    //
    //    }
    func toLoginWithMobileNumber()
    {
        self.str_UserName = self.txtUserName.text!
        if Helper.isConnectedToNetwork(){
            Helper.ToShowIndicator()
            let dict_param = [
                "MobileNumber": txtMobileNum.text!,
                "DeviceID": Constants.Device_UUID ,
                "DeviceToken":UserDefaults.standard.value(forKey: Constants.DevToken),
                "UserName": str_UserName,
                "DeviceName" : "IOS"
            ]

            let url_str = Constants.BASEURL + MethodName.Login
            print(dict_param)
            Server.PostDataInDictionary(url_str, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
                Helper.ToHideIndicator()
                if (response != nil) && (response?["MessageCode"] as! String == "Act07" || response?["MessageCode"] as! String == "Act13"){
                    print(response!)
                    DispatchQueue.main.async {
                        self.logButton.isHidden = true
                        self.dict_Register = ["MobileNumber" : self.txtMobileNum.text!]
                        Helper.ToHideIndicator()
                        self.toDecideWeatherOTPScreenORLoginScreen()
                    }
                }
                else if  (response?["MessageCode"] as! String == "Act25")
                {
                    DispatchQueue.main.async {
                        self.logButton.isHidden = false
                    self.lbl_mobileNumberHeight.constant = 20.0
                    self.Txt_mobileNumberHeight.constant = 12.0
                    self.txtUserName.isHidden = false
                    self.txtUserName.becomeFirstResponder()
                    }
                }
                else{
                    PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
                }
            })
        }else{
            PKSAlertController.alertForNetwok()


        }

    }
    func toDecideWeatherOTPScreenORLoginScreen(){
        scrollView.scrollRectToVisible(CGRect.init(x: SCREEN_WIDTH, y: 0, width: SCREEN_WIDTH, height: 155), animated: true)
        btnResendOTP.isHidden = true
        logButton.isHidden = true
        self.txt_privacyPolicy.isHidden = true
        self.viewforSocialLogin.isHidden = true
        btnSignIn.setTitle("VALIDATE", for: UIControlState.normal)
        ////btnNewUserSignIn.isHidden = true
        counter = 30
        lbl_timer.text = "Otp Resends In: \(counter) seconds"
        lbl_timer.isHidden = false
        btnResendOTP.isHidden = true
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)

    }
    
    // called every time interval from the timer
    func timerAction() {
        counter -= 1
        lbl_timer.text = "Otp Resends In: \(counter) seconds"
        //counter == 0 ? timer.invalidate():

        if counter == 0 {
            timer.invalidate()
            timer = nil
            lbl_timer.isHidden = true
            btnResendOTP.isHidden = false
        }
        print(counter)
    }

    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scr_pagecontrol.frame.size.width
        scr_pagecontrol.setContentOffset(CGPoint(x: x,y :0), animated: true)
    }
}



extension LoginView : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 10
    }
}
extension LoginView : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
}
