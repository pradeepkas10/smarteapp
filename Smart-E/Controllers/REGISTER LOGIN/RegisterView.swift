//
//  RegisterView.swift
//  NikonApp
//
//  Created by Surbhi on 15/12/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import SafariServices


class RegisterView: UIViewController {

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var txtView: UITextView!


    var myDictionary: [AnyHashable: Any]? = UserDefaults.standard.dictionary(forKey: "dict_user")
    var datePicker = UIDatePicker()
    var dict_Response = [String:String]()
    
    

    //MARK: -  view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(myMethodToHandleTap(_:)))
        //tap.delegate = self
        txtView.addGestureRecognizer(tap)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
    }
    
    
    //MARK: -  button actions
    @IBAction func btnDoneClicked(sender:UIButton){
        if !ValidationData(){
            return
        }
        if Helper.isConnectedToNetwork(){
        Helper.ToShowIndicator()
        let dict_param = [
            "Name": txtName.text!,
            "EmailID": txtEmail.text!,
            "MobileNumber": txtMobile.text!,
            "Device_ID": Constants.Device_UUID! ,
            "DeviceToken": UserDefaults.standard.value(forKey: Constants.DevToken)!,
            "IPAddress": "IP Address",
            "Device_Name" : "IOS"
        ]
        
        let url_str = Constants.BASEURL + MethodName.Register
        print(dict_param)
        Server.PostDataInDictionary(url_str, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
            Helper.ToHideIndicator()
            if (response != nil) && response?["MessageCode"] as! String == "Act13"{
                    print(response!)
                    self.dict_Response = ["MobileNumber" : response?["MobileNumber"] as! String]
                    self.toSendUserToOTPScreen(withOTP: true)
            }else{
                PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
            }
            
        })
        }else{
            PKSAlertController.alertForNetwok()
        }
        
        
    }
    
    @IBAction func btnAlreadyHaveAccount(sender:UIButton){
        toSendUserToOTPScreen(withOTP: false)
       
    }

    //MARK: -  extraa functions

    override var preferredStatusBarStyle: UIStatusBarStyle {
        print("change bar style")
        return .default
    }
    
    
    func myMethodToHandleTap(_ sender: UITapGestureRecognizer) {
        
        let myTextView = sender.view as! UITextView
        let layoutManager = myTextView.layoutManager
        
        // location of tap in myTextView coordinates and taking the inset into account
        var location = sender.location(in: myTextView)
        location.x -= myTextView.textContainerInset.left;
        location.y -= myTextView.textContainerInset.top;
        
        // character index at tap location
        let characterIndex = layoutManager.characterIndex(for: location, in: myTextView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        // if index is valid then do something.
        if characterIndex < myTextView.textStorage.length {
            
            // print the character index
           print("character index: \(characterIndex)")
            if characterIndex >= 41 && characterIndex <= 56{
                print("terms")
                self.openLink(url: "http://getsmarte.in/")
            }
            
            if characterIndex >= 61 && characterIndex <= 73{
                print("Privacy Policy")
                self.openLink(url: "http://getsmarte.in/")
            }
            
        }
    }
    
     func openLink(url: String) {
        if let url = URL(string: url) {
            let svc = SFSafariViewController(url: url)
            UIApplication.shared.statusBarStyle = .default
            setNeedsStatusBarAppearanceUpdate()
            
            if #available(iOS 10.0, *) {
                svc.preferredControlTintColor = Colors.appThemeColorText
            } else {
                // Fallback on earlier versions
            }
            svc.delegate = self
            present(svc, animated: true, completion: {
                if #available(iOS 10.0, *) {
                    svc.preferredControlTintColor = Colors.appThemeColorText
                } else {
                    // Fallback on earlier versions
                }
            })
        }
    }
    
    
    func toSendUserToOTPScreen(withOTP:Bool){
        for vc in (self.navigationController?.viewControllers)!{
            if vc is LoginView{
                let LoginVC = vc as! LoginView
                LoginVC.OTPScreen = withOTP
                LoginVC.dict_Register = dict_Response
                DispatchQueue.main.async {
                   _ = self.navigationController?.popToViewController(LoginVC, animated: true)
                }
            }
        }
    }
   
    func ValidationData()->Bool{
        if txtName.text?.count == 0{
            PKSAlertController.alert(Constants.appName , message: "Please enter your name.")
            return false
        }
        if txtMobile.text?.count == 0{
             PKSAlertController.alert(Constants.appName, message: "Please enter your mobile number.")
            return false
        }
        if !Helper.isValidPhoneNumber(phoneNumber: txtMobile.text!){
            PKSAlertController.alert(Constants.appName, message: "Please enter valid mobile number.")
            return false
        }
        if  txtEmail.text?.count != 0 && !Helper.isValidEmail(txtEmail.text!){
            PKSAlertController.alert(Constants.appName, message: "Please enter valid email.")
            return false
        }
        
        return true
    }
    
    
}

//MARK:- Safari View Controller
extension RegisterView: SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


extension RegisterView : UITextFieldDelegate{
  
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtMobile{
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 10
        }
        
        if textField == txtName {
            let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ").inverted
            return string.rangeOfCharacter(from: set) == nil
        }
        
        
        return true
    }
    
    
}
