    //
    //  RideStartViewController.swift
    //  Smart-E
    //
    //  Created by Hitesh Dhawan on 18/01/18.
    //  Copyright © 2018 Hitesh Dhawan. All rights reserved.
    //

    import UIKit
    import FloatRatingView
    import Ligero

    protocol toUpdateBookingCompletionData:class {
        func updateResultAfterBookingCompletion()

    }

    weak var bookingCompletionDelegate:toUpdateBookingCompletionData!

    class RideStartViewController: UIViewController ,toUpdatedataViaNotification,UITextViewDelegate {
        func bookingcancelledByDriver() {

        }




        @IBOutlet weak var btnDone:UIButton!
        @IBOutlet weak var viewAddress:UIView!
        @IBOutlet weak var heightView:NSLayoutConstraint!
        @IBOutlet weak var lblStatus:UILabel!
        @IBOutlet weak var lblFare:UILabel!
        @IBOutlet weak var viewPrice:UIView!
        @IBOutlet weak var viewComment:UIView!
        @IBOutlet weak var sourceAddress:UILabel!
        @IBOutlet weak var destintionAddress:UILabel!
        @IBOutlet weak var driverName:UILabel!
        @IBOutlet weak var vehicalNo:UILabel!
        @IBOutlet weak var imageView:UIImageView!
        @IBOutlet weak var ratingView:FloatRatingView!
        @IBOutlet weak var txtView:UITextView!
        @IBOutlet weak var heightFortxtView:NSLayoutConstraint!
        @IBOutlet weak var heightFortxtView2:NSLayoutConstraint!
        var bookingRideDetails:BookingRideDetails?

        var transactionID = ""
        var comingFromPayment = false
        var amountToPay = ""
        var BookingID = ""
        var DriverID = ""
        var RideType = ""
        var TravelDistance = "'"
        var dict_profile = [String:String]()
        var transLogId = ""
        var StrUserID = ""
        var sourceAddresssStr = ""
        var destinationAddressStr = ""
        var snapShotImage:UIImage?
        var sourceLatLong = ""
        var DestinationLatLong = ""
        var currentRating = ""
        var dict_dataFromNotification = [String:Any]()
        var Status = ""


        //MARK : to update data after notification received
        func updatedatawithnotifcation(dict: NSDictionary) {
            print("ankit sharma")
            print(dict)
            self.dict_dataFromNotification = dict as! [String : Any]
            self.toUpdate()
        }
        //MARK:- view controller life cycle
        override func viewDidLoad() {
            super.viewDidLoad()
            self.StrUserID = String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
            dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as! [String : String]

            viewPrice.isHidden = true
            viewComment.isHidden = true
            //self.viewPrice
            heightFortxtView.constant = 70
            heightFortxtView2.constant = 0
            ratingView.delegate = self
            //Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.toUpdate), userInfo: nil, repeats: false)

            sourceAddress.text = bookingRideDetails?.SourceAdd
            destintionAddress.text = bookingRideDetails?.DestinationAdd
            driverName.text = "Driver Name:- \(bookingRideDetails!.DriverName)"
            vehicalNo.text = "Vehicle No:- \(bookingRideDetails!.VechicleNumber)"
            sourceLatLong = "\(String(describing: bookingRideDetails!.Source.coordinate.latitude)),\(String(describing: bookingRideDetails!.Source.coordinate.longitude))"
            DestinationLatLong = "\(String(describing: bookingRideDetails!.Destination.coordinate.latitude)),\(String(describing: bookingRideDetails!.Destination.coordinate.longitude))"

            print("source addres ==== \(sourceLatLong)  \(DestinationLatLong)")
            Helper.saveDataInNsDefault(object: bookingRideDetails!, key: Constants.bookingRideDetails)

//            if bookingRideDetails?.PaymentStatus == 1 {
//                viewAddress.isHidden = true
//                heightView.constant  = 0
//                heightFortxtView2.constant = 106
//                viewPrice.isHidden = false
//                UIView.animate(withDuration: 0.5){
//                    self.view.layoutIfNeeded()
//                }
//            }
            toGetImage()
        }
        override func viewDidAppear(_ animated: Bool) {
             delegateUpdateData = self
        }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }

        //MARK:- extraa functions

    //    func updatedatawithnotifcation(dict: [String : Any])
    //{
    //    let BookingID = dict["BookingID"] as! String
    //    let DriverID = dict["DriverID"] as! Int
    //    let FinalFareamount = dict["FinalFareamount"] as! String
    //    let RideType = dict["RideType"] as! String
    //    let TransID = dict["TransID"] as! String
    //    let TravelDistance = dict["TravelDistance"] as! String
    //
    //    }
        func toUpdate(){

            if self.dict_dataFromNotification.keys.contains("message"){
                heightFortxtView2.constant = 0
                viewPrice.isHidden = true
                viewComment.isHidden = false

                // lblStatus.text = "YOUR RIDE IS COMPLETED"
                UIView.animate(withDuration: 0.5){
                    self.view.layoutIfNeeded()
                }
            }
            else
            {
                viewAddress.isHidden = true
                heightView.constant  = 0
                heightFortxtView2.constant = 150
                viewPrice.isHidden = false
                //String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
                self.amountToPay =   String(describing:dict_dataFromNotification["FinalFareamount"]!)
                self.transactionID = String(describing:dict_dataFromNotification["TransID"]!)
                self.DriverID =  String(describing:dict_dataFromNotification["DriverID"]  as! NSNumber)
                self.RideType =  String(describing:dict_dataFromNotification["RideType"]  as! NSNumber)
                self.TravelDistance =  String(describing:dict_dataFromNotification["TravelDistance"]  as! NSNumber)
                self.BookingID =  String(describing:dict_dataFromNotification["BookingID"]  as! NSNumber)
                self.bookingRideDetails?.DriverID = Int(DriverID)!
                self.bookingRideDetails?.bookingID = Int(BookingID)!
               // self.bookingRideDetails?.Fare = self.amountToPay
                lblFare.text = "₹ "+self.amountToPay
                lblStatus.text = "YOUR RIDE IS COMPLETED"

                UIView.animate(withDuration: 0.5){
                    self.view.layoutIfNeeded()
                }
            }
        }

        func toUpdateUIAfterGotNotification(){
            UIView.animate(withDuration: 1.0) {
                self.btnDone.isHidden = false
            }
        }

        func toGetGiveFeedBack(){
            let url = Constants.BASEURL + "DriverFeedback"
            Helper.ToShowIndicator()
            var str_comment = txtView.text
            if str_comment?.trimmingCharacters(in: .whitespacesAndNewlines) == "Write your Comment." {
                str_comment = ""

            }
            else
            {
                str_comment = txtView.text
            }


            let dict_param = [
                "UserID":StrUserID,
                "DriverId": bookingRideDetails?.DriverID as Any,
                "StarsCount": currentRating,
                "FeedbackDiscription": str_comment!,
                "Device_ID": Constants.Device_UUID!,
                "DeviceToken": "string",
                "IPAddress": "IP Address",
                "Device_Name" : "IOS"
            ]

            Server.PostDataInDictionary(url, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
                print(response as Any)
                print(response!)
                Helper.ToHideIndicator()
                if (response != nil) {
                    if response?["MessageCode"] as! String == "FD01"{
                        print(response!)
                        DispatchQueue.main.async {
                            self.togetbacktoDashboard()
                        }
                    }else{
                        PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                    }
                }else{
                    print("errror")
                }
            })
        }

        func togetbacktoDashboard()
        {
            for vc in (self.navigationController?.viewControllers)!{
                if vc is HomeDashBoardVC{
                    DispatchQueue.main.async {
                        bookingCompletionDelegate.updateResultAfterBookingCompletion()
                        Helper.saveDataInNsDefault(object: [:] as AnyObject, key: Constants.bookingRideDetails)
                        UserDefaults.standard.set("2", forKey: Constants.RideStatus)
                        _ = self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
        func toGetImage(){
            let width = Int(UIScreen.main.bounds.width)-20
            let staticMapUrl: String = "https://maps.googleapis.com/maps/api/staticmap?format=png32&size=\(width)x180&scale=2&markers=color:red%7Clabel:S%7C\(sourceLatLong)&markers=color:green%7Clabel:D%7C\(DestinationLatLong)"
            Server.toGetImageFromURL(url: staticMapUrl) { (image) in
                if image != nil{
                    DispatchQueue.main.async {
                        self.imageView.contentMode = .scaleAspectFill
                        self.imageView.backgroundColor = .clear
                        self.imageView.image = image!
                    }
                }
            }
        }

        //MARK:- button actions

        @IBAction func btnCallDriverClicked(sender:AnyObject){
            let number = bookingRideDetails?.DriverMobileNumber
            if let url = URL(string: "tel://\(number!)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
        @IBAction func btnPayByCash(sender:UIButton){
            var dic_customdata = NSDictionary()
            dic_customdata = [
                "message": "Pay By Cash",
                "MessageCode": "ACT",
                ]
            delegateUpdateData?.updatedatawithnotifcation(dict: dic_customdata)
        }
        @IBAction func btnRideClicked(sender:UIButton){
            self.toGetGiveFeedBack()
        }
        @IBAction func btnSkipClicked(sender:UIButton)
        {
            DispatchQueue.main.async {
                self.togetbacktoDashboard()
            }
        }
        @IBAction func btnPaymentClicked(sender:UIButton)
        {
            Helper.ToShowIndicator()
            self.paymentWithPhonePE()
        }

        func paymentWithPhonePE(){

            //        var dict = [String:Any]()
            //        dict["merchantId"] = "M2306160483220675579140"
            //        dict["transactionId"] = String(arc4random_uniform(100000))
            //        dict["amount"] = 100
            //        dict["merchantOrderId"] = "1"
            //        dict["merchantUserId"] = "1dfer2"
            //        dict["message"] = "new ORder"
            //        dict["mobileNumber"] = "8130514507"
            //        dict["email"] = "nandani2719mishra@gmail.com"
            //        dict["shortName"] = "NandaniMishra"
            //
            //        print(dict)
            //
            //
            //        let dictBase64Data: Data = try! JSONSerialization.data(withJSONObject:dict,options: JSONSerialization.WritingOptions.prettyPrinted)
            //        let dictBase64 = dictBase64Data.base64EncodedString()
            //
            //        print(dictBase64)
            //
            //        let saltProcee = (dictBase64 + "/v3/debit" + "8289e078-be0b-484d-ae60-052f117f8deb").sha256()
            //        print(saltProcee)
            //
            //        let checkSum = saltProcee + "###1"
            //
            //        print(checkSum)

            let headers = ["Content-type": "application/json"]

            toGetSaltCheckSumValue { (SaltCheckSum, base64,apiEndPoints,TranslogID) in
                if SaltCheckSum != nil && base64 != nil{
                    let req = PPSTransactionRequest.init(base64EncodedBody: base64!, apiEndPoint: apiEndPoints!, checksum: SaltCheckSum!, headers: headers)

                    PhonePeSDK.shared().startPhonePeTransactionRequest(req, on: self, animated: true) { (request, result) in
                        print(result.context)
                        self.Status = result.context["statusCode"] as! String
                        print(result.description)
                        print(result.successful)
                        self.toGetTransactionStatus(completionHandler: { (response) in
                            print("respse")
                        })
                    }
                }
            }
        }

        func toGetTransactionStatus(completionHandler:@escaping (_ result:[String:AnyObject]?)->Void){

    let url_str = Constants.BASEURL+"PassengerPaymentRecieptance"
            var dict = [String:Any]()
                    dict["UserID"] = StrUserID
                    dict["BookingID"] = BookingID
                    dict["TransID"] = transactionID
                    dict["TransLogID"] = transLogId

            //Helper.ToShowIndicator()
            Server.PostDataInDictionary(url_str, dict_data: dict as Dictionary<String, AnyObject>, completionHandler: { (response) in

                print(response!)
                if (response != nil) && (response?["payResponseCode"] as! String == "PAYMENT_SUCCESS") {
                    Helper.ToHideIndicator()
                    print(response!)
                    DispatchQueue.main.async {
                        PKSAlertController.alert(Constants.appName, message:response?["paymentState"] as! String)
                        self.heightFortxtView2.constant = 0
                        self.viewPrice.isHidden = true
                        self.viewComment.isHidden = false

                        // lblStatus.text = "YOUR RIDE IS COMPLETED"
                        UIView.animate(withDuration: 0.5){
                            self.view.layoutIfNeeded()
                        }
                    }
                }else{
                    Helper.ToHideIndicator()
                    PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : "Your transaction is failed, Please try again later.")
                }
            })
        }
    //       // let url_str = "https://mercury-uat.phonepe.com/v3/transaction/M2306160483220675579140/\(transactionID)/status"
    //
    //        let url_str = "http://13.127.170.151?UserID=\(StrUserID)&BookingID=\(BookingID)&TransID=\(transactionID)&TransLogID=\(transLogId)&amount=\(self.amountToPay)&success=\(Status)"
    //    //" http://13.127.170.151{"UserID":"63","BookingID":"651","TransID":"304","TransLogID":"1178","amount":1000,"success":"{\"statusCode\":\"SUCCESS\"
    //       let saltValue1 = ("/v3/transaction/M2306160483220675579140/\(transactionID)/status" + "8289e078-be0b-484d-ae60-052f117f8deb").sha256() + "###" + "1"
    //
    //        print(url_str)
    //
    //        print(saltValue)
    //
    //        let defaultConfigObject = URLSessionConfiguration.default
    //        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
    //
    //        let urlRequest = NSMutableURLRequest(url: NSURL(string: url_str)! as URL)
    //        urlRequest.httpMethod = "GET"
    //
    //        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
    //        //  urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
    //
    //        urlRequest.addValue(saltValue, forHTTPHeaderField: "x-verify")
    //
    //        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
    //
    //            if error != nil {
    //                print("Error occurred: "+(error?.localizedDescription)!)
    //                return;
    //            }
    //            do {
    //                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
    //                completionHandler(responseObjc)
    //                print("responseObjc ==== \(responseObjc)")
    //                PKSAlertController.alert(Constants.appName, message: "payment is successful.")
    //
    //            }
    //            catch {
    //                print("Error occurred parsing data: \(error)")
    //            }
    //        }
    //        task.resume()



        func toGetSaltCheckSumValue(completionHandler:@escaping (_ saltValue:String?,_ base64Str:String?,_ apiEndPoints:String?, _ TranslogID:String?)->Void){
            if Helper.isConnectedToNetwork(){

              //  transactionID = String(arc4random_uniform(100000))
                let amount =   Double(self.amountToPay)!*100
                print(amount)
                var dict = [String:Any]()
              //  dict["merchantId"] = "M2306160483220675579140"
                dict["transactionId"] = self.transactionID
                dict["amount"] = "\(forTrailingZero(temp: amount))"
                dict["merchantOrderId"] = self.BookingID
                dict["merchantUserId"] = StrUserID
                dict["message"] = "iOS"
                dict["mobileNumber"] = dict_profile["MobileNumber"]
                dict["email"] = dict_profile["Email"]
                dict["shortName"] = dict_profile["Name"]
                print(dict)

                let url_str = Constants.BASEURL + "GetCheckSum"
                print(dict)
                Server.PostDataInDictionary(url_str, dict_data: dict as Dictionary<String, AnyObject>, completionHandler: { (response) in
//                    Helper.ToHideIndicator()
                    if (response != nil) && (response?["MessageCode"] as! String == "CHECK01") {
                        print(response!)
                        DispatchQueue.main.async {
                            completionHandler(response?["Checksum"] as? String, response?["Base64"] as? String,response?["apiEndPoints"] as? String,response?["TranslogID"] as? String)
                            self.transLogId = String(describing:(response?["TranslogID"] as! NSNumber))
                        }
                    }else{
                        completionHandler(nil, nil,nil,nil)

                        PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
                    }
                })
            }else{
                PKSAlertController.alertForNetwok()
            }
        }

        func forTrailingZero(temp: Double) -> String {
            let tempVar = String(format: "%g", temp)
        return tempVar
        }
        func textViewDidBeginEditing(_ textView: UITextView) {
            textView.text = ""
        }
//        func textViewDidEndEditing(_ textView: UITextView) {
//            let str = textView.text
//            if ((str?.trimmingCharacters(in: .whitespaces)) == "") {
//                PKSAlertController.alert("Smarte", message: "")
//            }
//
//        }
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
    }
    extension String {

        func sha256() -> String{
            if let stringData = self.data(using: String.Encoding.utf8) {
                return hexStringFromData(input: digest(input: stringData as NSData))
            }
            return ""
        }

        private func digest(input : NSData) -> NSData {
            let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
            var hash = [UInt8](repeating: 0, count: digestLength)
            CC_SHA256(input.bytes, UInt32(input.length), &hash)
            return NSData(bytes: hash, length: digestLength)
        }

        private  func hexStringFromData(input: NSData) -> String {
            var bytes = [UInt8](repeating: 0, count: input.length)
            input.getBytes(&bytes, length: input.length)

            var hexString = ""
            for byte in bytes {
                hexString += String(format:"%02x", UInt8(byte))
            }

            return hexString
        }

    }

    extension RideStartViewController:FloatRatingViewDelegate{

        func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
            currentRating = String(rating)
            if rating <= 3{
                heightFortxtView.constant = 70
                //heightFortxtView2.constant = 0
            }else{
                heightFortxtView.constant = 0
                //heightFortxtView2.constant = 106
            }
        }
    }
