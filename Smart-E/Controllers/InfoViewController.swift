//
//  InfoViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 01/03/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var carousel: ScalingCarouselView!
    var index = 0
    var arr_ride = [String]()
    var arr_Price = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        arr_ride = ["SHARED RIDE","SOLO RIDE"]
        arr_Price = ["Rs. 10 \n (For first 2 km)","Rs. 30 \n (For first 2 km)"]
        //view.isOpaque = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
        
    }
    var gradientLayer: CAGradientLayer!
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()

        gradientLayer.frame = self.view.bounds

        gradientLayer.colors = [UIColor.init(red: 224.0, green: 243.0, blue: 197.0, alpha: 1.0), UIColor.white.cgColor]

        self.view.layer.addSublayer(gradientLayer)
    }


}

class Cell: ScalingCarouselCell {
    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var lblRide:UILabel!
    @IBOutlet weak var lblRidePrice:UILabel!
    
}


typealias CarouselDatasource = InfoViewController
extension CarouselDatasource: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        if let scalingCell = cell as? Cell {
            //scalingCell.mainView.backgroundColor = UIColor.white
          let  gradientLayer = CAGradientLayer()

            gradientLayer.frame =  scalingCell.mainView.bounds

            gradientLayer.colors = [UIColor.init(red: 224.0, green: 243.0, blue: 197.0, alpha: 1.0), UIColor.white.cgColor]

            scalingCell.mainView.layer.addSublayer(gradientLayer)
           // index = indexPath.row
            if index == 0
            {

                scalingCell.lblRide.text = arr_ride[indexPath.row]//"SHARED RIDE"
                scalingCell.lblRidePrice.text = arr_Price[indexPath.row]
            }
            else
            {
                if indexPath.row == 0
                {
                    scalingCell.lblRide.text = arr_ride[indexPath.row+1]//"SHARED RIDE"
                    scalingCell.lblRidePrice.text = arr_Price[indexPath.row+1]
                }
                else
                {
                    scalingCell.lblRide.text = arr_ride[indexPath.row-1]//"SHARED RIDE"
                    scalingCell.lblRidePrice.text = arr_Price[indexPath.row-1]
                }
                //let rotation = CGAffineTransform(rotationAngle: CGFloat(Double.pi / -2.0))
                // scalingCell.transform = cell.transform.concatenating(rotation);
            }
        }
        return cell
    }
}

typealias CarouselDelegate = InfoViewController
extension InfoViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        carousel.didScroll()

        // guard let currentCenterIndex = carousel.currentCenterCellIndex?.row else { return }
        //index = (carousel.currentCenterCellIndex?.row)!
        // output.text = String(describing: currentCenterIndex)
    }
}

