//
//  RatingViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 29/12/17.
//  Copyright © 2017 Hitesh Dhawan. All rights reserved.
//

import UIKit
import  FloatRatingView

class RatingViewController: UIViewController {

    @IBOutlet weak var ratingView:FloatRatingView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
       //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }

    
}


extension RatingViewController : FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        print(rating)
    }
    
    
    
}
