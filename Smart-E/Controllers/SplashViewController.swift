//
//  SplashViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 13/02/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var imageView : UIImageView!
    
    
    var timer:Timer!
    var navCtrl: UINavigationController? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(toSetImageViewWithAnimation), userInfo: nil, repeats: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            self.makeDecisionForFisrtScreen()
        })
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("disapper")
        timer.invalidate()
        timer = nil
    }
    
    
    var point:CGFloat = 75.0
    
    @objc func toSetImageViewWithAnimation(){
        imageView.frame.origin.x = (point)
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
        point = point + 10
        if point >= SCREEN_WIDTH{
            point = 0
        }
        
    }
    
    func makeDecisionForFisrtScreen(){
        let loggedin:Bool = (UserDefaults.standard.object(forKey: Constants.PROFILE) != nil)
        if loggedin{
            let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "SideMenuNavigation")
            Constants.appDelegate.window?.rootViewController = root
            UIApplication.shared.keyWindow?.rootViewController = root
        }
        else{
            let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            navCtrl = UINavigationController.init(rootViewController: root)
            navCtrl?.navigationBar.isHidden = true
            Constants.appDelegate.window?.rootViewController = navCtrl
        }
         Constants.appDelegate.window?.makeKeyAndVisible()
    }
    
    
    
}
