//
//  PaymentVC.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 18/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreData

protocol toGetAddress:class {
    func updateAddress(dict:[String:Any])
}

weak var delegateAddress:toGetAddress?

class PaymentVC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var resultText: UITableView!
    @IBOutlet weak var tbl_favourites: UITableView!
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var tbl_favouriteHeight: NSLayoutConstraint!
    var fetcher: GMSAutocompleteFetcher?
    var arrAddressListingPrimary = [String]()
    var arrAddressListingSecondary = [String]()
    var listingPlace = [[String:String]]()
     var favouritePlace = [[String:String]]()
    var str_header = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        textField.becomeFirstResponder()
       // self.tbl_favourites.register(CustomFavouriteCell.self, forCellReuseIdentifier: "CustomFavouriteCell")

        lbl_header.text = str_header
        view.backgroundColor = .white
        edgesForExtendedLayout = []
        let latLong = CLLocationCoordinate2D(latitude: 28.370917, longitude: 76.803156)
        let latLong1 = CLLocationCoordinate2D(latitude: 28.882014, longitude: 77.432123)
        let bounds = GMSCoordinateBounds(coordinate: latLong, coordinate: latLong1)
        self.tbl_favouriteHeight.constant = 0
        resultText.tableFooterView = UIView()
        tbl_favourites.tableFooterView = UIView()
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter
        filter.country = "IN"

        // Create the fetcher.
        fetcher = GMSAutocompleteFetcher(bounds: bounds, filter: filter)
        fetcher?.autocompleteBounds = bounds
        fetcher?.autocompleteBoundsMode = .bias
        fetcher?.delegate = self
        textField?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                             for: .editingChanged)
        textField.autocorrectionType = .no

    }
    override func viewDidAppear(_ animated: Bool) {
        tbl_favourites.delegate = self
        tbl_favourites.dataSource = self
        self.fetchdata()
//        for prediction in favouritePlace {
//            var dict = [String:String]()
////            dict["First"] = prediction.attributedPrimaryText.string
////            dict["Second"] = prediction.attributedSecondaryText?.string
////            dict["placeId"] = prediction.placeID
//            favouritePlace.append(dict)
//        }
//        tbl_favourites?.reloadData()
    }
    func fetchdata()
    {

        let request = NSFetchRequest<Favourite>(entityName: "Favourite")
        request.returnsObjectsAsFaults = false
        //let predicate = [NSPredicate predicateWithFormat:"%K = %@", "planID", @(30)] as! NSPredicate
        do {
            let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
            print(searchResults.count)
            print(searchResults)
            var userId = UserDefaults.standard.value(forKey: Constants.USERID) as! Int64
            for task in searchResults {
                print(task)
                if task.id == userId
                {
                    var dict = [String:String]()
                    dict["First"] = task.location_address
                    dict["Second"] = task.location_address2
                    dict["placeId"] = task.location_id
                    dict["locationTitle"] = task.location_title
                    favouritePlace.append(dict)
                }

                //  let predicate = NSPredicate(format: "%K == %@", "planID", task.planID)
            }
            tbl_favourites.reloadData()
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }


    @IBAction func btnBackPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }


 @objc func textFieldDidChange(textField: UITextField) {
       // self.tbl_favouriteHeight.constant = 300
        DispatchQueue.main.async{
            self.fetcher?.sourceTextHasChanged(textField.text!)
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {

    }

}

extension PaymentVC: GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        listingPlace.removeAll()
        for prediction in predictions {
            var dict = [String:String]()
            dict["First"] = prediction.attributedPrimaryText.string
            dict["Second"] = prediction.attributedSecondaryText?.string
            dict["placeId"] = prediction.placeID
            listingPlace.append(dict)
        }
        resultText?.reloadData()

        self.tbl_favouriteHeight.constant = CGFloat(listingPlace.count*60)

        // print(resultText?.text)
    }

    func didFailAutocompleteWithError(_ error: Error) {
        //  resultText?.text = error.localizedDescription
        self.tbl_favouriteHeight.constant = 0

        PKSAlertController.alert(Constants.appName, message: error.localizedDescription)
    }


}

extension PaymentVC : UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0

        if tableView == resultText {
            count = listingPlace.count
        }
        else
        {
            if favouritePlace.count == 0{
               // Helper.SetBalnkViewForEmptyTableView(tblView: tableView, message: "There is no favourites.")
                count = 0
            }
            else
            {
                count = favouritePlace.count
            }
        }
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        var tblCell: UITableViewCell!
        if tableView == resultText {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomAutocompleteCell", for: indexPath) as! CustomAutocompleteCell

            let dict = listingPlace[indexPath.item]
            cell.lbl_addressPrimary?.text = dict["First"]
            cell.lbl_addressSecondary?.text = dict["Second"]
            cell.lbl_addressSecondary?.textColor = UIColor.darkGray
            cell.lbl_addressPrimary?.textColor = UIColor.darkText
            cell.selectionStyle = .none
           // tblCell = cell
            return cell
        }
        else if tableView == tbl_favourites
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "CustomFavouriteCell", for: indexPath) as! CustomFavouriteCell
            let dict = favouritePlace[indexPath.item]
            print(favouritePlace)
            print(dict)

            cell1.lbl_addressPrimary?.text = dict["First"]
            cell1.lbl_addressSecondary?.text = dict["Second"]
            cell1.lbl_addressSecondary?.textColor = UIColor.darkGray
            cell1.lbl_addressPrimary?.textColor = UIColor.darkText
            cell1.lbl_addressType.text = dict["locationTitle"]
            cell1.selectionStyle = .none
            if dict["locationTitle"] == "Work"
            {
                cell1.img?.image = UIImage.init(named: "work")
            }
            else if dict["locationTitle"] == "Home"
            {
                cell1.img?.image = UIImage.init(named: "0")
            }
            else
            {
                cell1.img?.image = UIImage.init(named: "heart_Filled")
            }
            //tblCell = cell1
            //cell1.backgroundColor = .red
            return cell1
        }
   
        return tblCell
    }

     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var count = 0
        if tableView == resultText
        {
            count = 0
        }
        else
        {
            count = 40
        }
        return CGFloat(count)
    }

     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header_view = UIView()
        if tableView == resultText
        {

        }
        else
        {
        header_view.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 40)
        header_view.backgroundColor = .white

        let lbl_favourite = UILabel.init(frame: CGRect(x: 10, y: 5, width: SCREEN_WIDTH, height: 30))
        lbl_favourite.font = UIFont.init(name: FONTS.Roboto_Bold, size: 15.0)
        lbl_favourite.textAlignment = .left
        lbl_favourite.text = "Favourites"
        header_view.addSubview(lbl_favourite)
        }
        return header_view
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var count = 0
        if tableView == resultText
        {
            count = 60
        }
        else
        {
            count = 95
        }
        return CGFloat(count)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict = [String:String]()
        var dict_adddress = [String:Any]()
        if tableView == resultText {
             dict = listingPlace[indexPath.item]
                print(dict)
            UserDefaults.standard.set(false, forKey: "Favourite")
             dict_adddress = listingPlace[indexPath.item]
        }
        else if tableView == tbl_favourites
        {
            dict = favouritePlace[indexPath.item]
            UserDefaults.standard.set(true, forKey: "Favourite")
            dict_adddress = favouritePlace[indexPath.item]
        }
        UserDefaults.standard.synchronize()
        let address = dict["First"]! + " " + dict["Second"]!
        let placeid  = dict["placeId"]

        print(placeid!,address)

        Helper.ToShowIndicator()  //AIzaSyDkUAh-itHv5pd4qFflg3twgFkcn9XF-HA
        // handle no location found
       // https://maps.googleapis.com/maps/api/geocode/json?latlng=\(source.latitude),\(source.longitude)&key=\(KEYS.GoogleKey)")!

        let str = "https://maps.googleapis.com/maps/api/geocode/json?place_id=\(placeid!)&language=en&key=\(KEYS.GoogleKey)".addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed)
       // let str = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(source.latitude),\(source.longitude)&key=\(KEYS.GoogleKey)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        let url = URL(string: str!)!

        // self.viewHeader.isHidden = false
        print("url === \n \(url.absoluteString)")
        if Helper.isConnectedToNetwork(){
            Server.getRequestWithURL(urlString: url.absoluteString, completionHandler: { (response) in
                Helper.ToHideIndicator()

                if response != nil {
                    let results = response?["results"] as? [Any]
                    if (results?.count)! > 0{
                        let results1 = results?[0] as? [String:Any]

                        DispatchQueue.main.async {
                            // print(results!)
                            //print(results1!)
                            let placeId = results1!["place_id"] as! String
                            let loc  = results1!["geometry"] as! [String:Any]
                            let cordinatess = loc["location"] as! [String:Any]
                            var coordinate1 = CLLocationCoordinate2D()
                            coordinate1 = CLLocationCoordinate2D(latitude: cordinatess["lat"] as! CLLocationDegrees, longitude: cordinatess["lng"] as! CLLocationDegrees)
                           // dict_adddress["coordinate"] = coordinate1
                            dict_adddress["lat"] = String(coordinate1.latitude)
                            dict_adddress["lng"] = String( coordinate1.longitude)
                            dict_adddress["placeId"] = placeId
                            UserDefaults.standard.set(dict_adddress, forKey: Constants.SAVEADDRESS)
                            UserDefaults.standard.synchronize()
                            delegateAddress?.updateAddress(dict: dict_adddress)
                        }
                    }else{
                        PKSAlertController.alert(Constants.appName, message:"Location not found.")
                    }
                }else{
                    print("error")
                }

            })

        }else{
            Helper.ToHideIndicator()
            PKSAlertController.alert(Constants.appName, message:"Network Configuration")
        }
        self.dismiss(animated: true, completion: nil)
    }
}

class CustomAutocompleteCell:UITableViewCell{
    @IBOutlet weak var lbl_addressPrimary:UILabel!
    @IBOutlet weak var lbl_addressSecondary:UILabel!
}

class CustomFavouriteCell:UITableViewCell{
    @IBOutlet weak var lbl_addressPrimary:UILabel!
    @IBOutlet weak var lbl_addressSecondary:UILabel!
    @IBOutlet weak var lbl_addressType:UILabel!

    @IBOutlet weak var img:UIImageView!

}





