//
//  HelpViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 17/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import MessageUI

class HelpViewController: UIViewController {

    @IBOutlet weak var webView:UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Helper.ToShowIndicator()
        webView.scrollView.bounces = false
        let url = URL.init(string: "http://13.127.170.151/AppHtmlPage/privacy-policy.html")
        webView.loadRequest(URLRequest.init(url: url!))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func BackButtonPressed(sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnHelpClicked(sender:UIButton){
        sendEmail()
    }
    
    @IBAction func btnCallDriverClicked(sender:AnyObject){
        let number = "8010400600"
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }

    }
    
    
}

extension HelpViewController:UIWebViewDelegate{
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Helper.ToHideIndicator()
    }
    
}

//MARK:- Mail Composer
extension HelpViewController: MFMailComposeViewControllerDelegate {
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            // Configure the fields of the interface.
            let dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as! [String : String]
            let name = dict_profile["Name"] ?? ""
            let mobile = dict_profile["MobileNumber"] ?? ""
            
            mailComposer.setSubject("Smarte App Feedback")
            mailComposer.setMessageBody("Name: \(name) and mobileNumber : \(mobile)", isHTML: false)
            mailComposer.setToRecipients(["support@getsmarte.in"])
            //            self.show(mailComposer, sender: self)
            
            // Present the view controller modally.
            present(mailComposer, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
}

