//
//  BookingRideDetail.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 07/02/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit


class BookingRideDetails: NSObject, NSCoding {
    
    var bookingID:Int
    var bookingLogID:Int
    var RouteID:Int
    var Source:CLLocation
    var Destination:CLLocation
    var SourceAdd:String
    var DestinationAdd:String
    var RideStatus:Int = 0
    var DriverName:String
    var VechicleNumber:String
    var DriverID:Int
    var DriverMobileNumber:String
    var routeArr:[CLLocation]
    var PaymentStatus = 0
    var BookingType = "0"


    
    init(bookingID:Int,bookingLogID:Int,RouteID:Int,Source:CLLocation,Destination:CLLocation,SourceAdd:String,DestinationAdd : String,DriverName:String ,PaymentStatus:Int, RideStatus:Int,DriverID:Int,VechicleNumber:String,routeArr:[CLLocation],DriverMobileNumber:String,BookingType:String){
        self.bookingLogID = bookingLogID
        self.RouteID = RouteID
        self.Source = Source
        self.Destination = Destination
        self.SourceAdd = SourceAdd
        self.DestinationAdd = DestinationAdd
        self.VechicleNumber = VechicleNumber
        self.DriverName = DriverName
        self.RideStatus  = RideStatus
        self.DriverID = DriverID
        self.bookingID = bookingID
        self.routeArr = routeArr
        self.DriverMobileNumber = DriverMobileNumber
        self.PaymentStatus = PaymentStatus
        self.BookingType = BookingType
    }
    
    
    required convenience init(coder decoder: NSCoder){
        let bookingID = decoder.decodeInteger(forKey: "bookingID")
        let bookingLogID = decoder.decodeInteger(forKey: "bookingLogID")
        let RouteID = decoder.decodeInteger(forKey: "RouteID")
        let Source = decoder.decodeObject(forKey: "Source") as! CLLocation
        let Destination = decoder.decodeObject(forKey: "Destination") as! CLLocation
        let SourceAdd = decoder.decodeObject(forKey: "SourceAdd") as! String
        let DestinationAdd = decoder.decodeObject(forKey: "DestinationAdd") as! String
        let DriverName = decoder.decodeObject(forKey: "DriverName") as! String
        let RideStatus = decoder.decodeInteger(forKey: "RideStatus")
        let DriverID = decoder.decodeInteger(forKey: "DriverID")
        let VechicleNumber = decoder.decodeObject(forKey: "VechicleNumber") as! String
        let routeArr = decoder.decodeObject(forKey: "routeArr") as! [CLLocation]
        let DriverMobileNumber = decoder.decodeObject(forKey: "DriverMobileNumber") as! String
        let PaymentStatus = decoder.decodeInteger(forKey: "PaymentStatus")
        let BookingType = decoder.decodeObject(forKey: "BookingType")
        self.init(bookingID:bookingID,bookingLogID:bookingLogID,RouteID:RouteID,Source:Source,Destination:Destination,SourceAdd:SourceAdd,DestinationAdd : DestinationAdd,DriverName:DriverName , PaymentStatus:PaymentStatus, RideStatus:RideStatus,DriverID:DriverID,VechicleNumber:VechicleNumber, routeArr : routeArr,DriverMobileNumber:DriverMobileNumber,BookingType: BookingType as! String)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.bookingLogID, forKey: "bookingLogID")
        aCoder.encode(self.RouteID, forKey: "RouteID")
        aCoder.encode(self.RideStatus, forKey: "RideStatus")
        aCoder.encode(self.DriverID, forKey: "DriverID")
        aCoder.encode(self.bookingID, forKey: "bookingID")
        aCoder.encode(self.PaymentStatus, forKey: "PaymentStatus")
        aCoder.encode(self.Source , forKey: "Source")
        aCoder.encode(self.Destination, forKey: "Destination")
        aCoder.encode(self.SourceAdd, forKey: "SourceAdd")
        aCoder.encode(self.DestinationAdd, forKey: "DestinationAdd")
        aCoder.encode(self.DriverName, forKey: "DriverName")
        aCoder.encode(self.VechicleNumber, forKey: "VechicleNumber")
        aCoder.encode(self.routeArr, forKey: "routeArr")
        aCoder.encode(self.DriverMobileNumber, forKey: "DriverMobileNumber")
        aCoder.encode(self.BookingType, forKey: "BookingType")
    }
    
}

