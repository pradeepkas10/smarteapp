//
//  HomeDashboradExtnsn.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 06/02/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import Foundation
import  UIKit

extension HomeDashBoardVC {
    
    
    func toSetTopViewsForAddresses(){
        view_address.frame = CGRect(x: 10, y: 65, width: SCREEN_WIDTH-20, height:100)
        view_address.backgroundColor = UIColor.white
        
        btn_pickupaddressHeight = NSLayoutConstraint(item: view_address, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.greaterThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 110)
        view_address.addConstraint(btn_pickupaddressHeight)
        
        btn_pickupaddress = UIButton(frame: CGRect( x:20 , y:5, width:view_address.frame.width-50.0 ,height : 35))
        btn_pickupaddress.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 0)
        //btn_pickupaddress.setImage(#imageLiteral(resourceName: "circle (1).png"), for:UIControlState.normal)
        
        btn_pickupaddress.setTitleColor(Colors.appThemeColorTextgray, for: UIControlState.normal)
        btn_pickupaddress.tag = 1
        btn_pickupaddress.titleLabel?.font = UIFont(name: FONTS.Roboto_Regular, size: 17.0)
        btn_pickupaddress.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btn_pickupaddress.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btn_pickupaddress.setTitle("Pin Location", for: .normal)
        btn_pickupaddress.backgroundColor = UIColor.white
        
        btn_pickupaddress.addTarget(self, action: #selector(autocompleteClickeddemo(_:)), for: .touchUpInside)
        btn_pickupaddress.titleEdgeInsets.left = 10.0
        self.btn_pickupaddress.titleLabel?.lineBreakMode = .byTruncatingTail
        view_address.addSubview(btn_pickupaddress)
        

        
        btn_destaddress =  UIButton.init(type: UIButtonType.custom)
        btn_destaddress.frame = CGRect( x:20 , y:50, width:view_address.frame.width-50.0 ,height : 40.0)
        //        btn_destaddress = UIButton(frame: CGRect( x:20 , y:61, width:view_address.frame.width-35.0 ,height : 35.0))
        //btn_destaddress.buttonType = UIButtonType.custom
        btn_destaddress.titleEdgeInsets.left = 10.0
        btn_destaddress.setTitleColor(UIColor.gray, for: UIControlState.normal)
        btn_destaddress.titleLabel?.font = UIFont(name: FONTS.Roboto_Regular, size: 17.0)
        btn_destaddress.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btn_destaddress.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btn_destaddress.backgroundColor = UIColor.white
        self.btn_destaddress.titleLabel?.lineBreakMode = .byTruncatingTail
        btn_destaddress.setTitle("Drop Location", for: .normal)
        btn_destaddress.tag = 2
//        btn_destaddress.backgroundColor = .red
        btn_destaddress.addTarget(self, action: #selector(autocompleteClickeddemo(_:)), for: .touchUpInside)
        view_address.addSubview(btn_destaddress)

        btn_switchAddress =  UIButton.init(type: UIButtonType.custom)

       // btn_switchAddress.frame = CGRect(x: Int(view_address.frame.width-25), y: Int(btn_pickupaddress.frame.height+btn_pickupaddress.frame.origin.y+3), width: 20, height: 20)
        btn_switchAddress.frame = CGRect(x: Int(view_address.frame.width-30), y: Int(btn_destaddress.frame.origin.y+8), width: 25, height: 25)
        btn_switchAddress.isHidden = true
        //btn_switchAddress.backgroundColor = UIColor.yellow
       // btn_switchAddress.setImage(#imageLiteral(resourceName: "switch"), for: .normal)
        btn_switchAddress.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        btn_switchAddress.addTarget(self, action: #selector(btn_switchAddress(_:)), for: .touchUpInside)
        view_address.addSubview(btn_switchAddress)

        let img = UIImageView()
        img.frame = CGRect(x: 15, y: btn_pickupaddress.frame.origin.y+btn_pickupaddress.frame.height/2-4, width: 9, height: 9)
        //img.image = UIImage.init(named: "circle (1).png")
        img.backgroundColor = Colors.appThemeColorText
        img.layer.cornerRadius = 4.5
        
        view_address.addSubview(img)
        
        let img1 = UIImageView()
        img1.frame = CGRect(x: 15, y: btn_destaddress.frame.origin.y+btn_destaddress.frame.height/2-4, width: 9, height: 9)
        img1.layer.borderColor = Colors.appThemeColorText.cgColor
        img1.layer.borderWidth = 2.0
        img1.layer.cornerRadius = 4.5
        //img1.backgroundColor = .white
        
        view_address.addSubview(img1)
        let lbl = UILabel()
        let height = img.frame.origin.y+img.frame.height
        
        lbl.frame = CGRect(x: img.frame.origin.x+4, y: height, width: 1, height: img1.frame.origin.y-3-height)
        lbl.backgroundColor = UIColor.green
        view_address.addSubview(lbl)
        Helper.giveShadowToView(view_address)
        viewHeadermain.addSubview(view_address)
    }
    
    
    
    func toSetBottomView(){
        stackView.axis  = UILayoutConstraintAxis.horizontal
        stackView.distribution  = UIStackViewDistribution.equalSpacing
        stackView.alignment = UIStackViewAlignment.center
        stackView.backgroundColor = UIColor.green
        stackView.spacing   = 16.0
        //
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.viewpassangersCount.addSubview(stackView)
        stackView.centerXAnchor.constraint(equalTo: self.viewpassangersCount.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.viewpassangersCount.centerYAnchor).isActive = true
        for arrayIndex in 1 ..< 5 {
            let Btn_passenger = UIButton()
            Btn_passenger.backgroundColor = UIColor.clear
            Btn_passenger.setTitle("\(arrayIndex)", for: .normal)
            Btn_passenger.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
            Btn_passenger.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
            
            Btn_passenger.layer.borderWidth = 2.0
            Btn_passenger.layer.borderColor = UIColor.white.cgColor
            Btn_passenger.layer.cornerRadius = 30
            stackView.addArrangedSubview(Btn_passenger)
            
        }
        stackView.centerXAnchor.constraint(equalTo: self.viewpassangersCount.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.viewpassangersCount.centerYAnchor).isActive = true
        
    }

    
}



// Autocomplete class extension
//extension HomeDashBoardVC:GMSAutocompleteViewControllerDelegate {
//
//    // Handle the user's selection.
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        //MARK: for Pickup
//        if tag == 1 {
//            print(" End in autocompltePlace address: \(String(describing: place.formattedAddress))")
//
//            let str_newAddress =  "\(place.name) \(place.formattedAddress!)"
//            self.btn_pickupaddress.setTitle(str_newAddress, for: .normal)
//            self.source = place.coordinate
//            viewMap.camera = GMSCameraPosition.camera(withTarget: place.coordinate, zoom: zoomLevel)
//        }
//            //MARK: for destination
//        else if tag == 2{
//            destaddressId = place.placeID
//
//            let str_newAddress =  "\(place.name) \(place.formattedAddress!)"
//            print(" start in autocomplte  Place address: \(str_newAddress))")
//
//            self.btn_destaddress.setNeedsUpdateConstraints()
//            self.btn_destaddress.setTitle(str_newAddress, for:.normal)
//            self.destinationLocation1 = place.coordinate
//            ///viewMap.camera = GMSCameraPosition.camera(withTarget: place.coordinate, zoom: zoomLevel)
//        }
//        //Close the autocomplete widget.
//        dismiss(animated: true, completion: nil)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print("Error: ", error.localizedDescription)
//    }
//
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//
//    // Show the network activity indicator.
//    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    }
//
//    // Hide the network activity indicator.
//    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//    }
//}

//    @IBAction func autocompleteClicked(_ sender: UIButton) {
//
//        tag = sender.tag
//        print(tag)
//        let autocompleteController = GMSAutocompleteViewController()
//        let filter = GMSAutocompleteFilter()
//        let visibleRegion = viewMap.projection.visibleRegion()
//
//        let latLong = CLLocationCoordinate2D(latitude: 28.370917, longitude: 76.803156)
//        let latLong1 = CLLocationCoordinate2D(latitude: 28.882014, longitude: 77.432123)
//        //let bounds = GMSCoordinateBounds(coordinate: visibleRegion.farLeft, coordinate: visibleRegion.nearRight)
//
//        let bounds = GMSCoordinateBounds(coordinate: latLong, coordinate: latLong1)
//        filter.type = .geocode
//        //filter.type = GMSPlacesAutocompleteTypeFilter.region
//        filter.country = "IN"
//
//        autocompleteController.delegate = self
//        autocompleteController.autocompleteBounds = bounds
//        autocompleteController.autocompleteFilter = filter
//        // autocompleteController.autocompleteBounds = getCoordinateBounds(latitude: 28.7041, longitude: 77.1025,distance:5)
//
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = .red
//
//
//        autocompleteController.tintColor = .red
//        UINavigationBar.appearance().barTintColor = UIColor(red: 44.0/255, green: 44.0/255, blue: 49.0/255, alpha: 1.0)
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UISearchBar.appearance().backgroundColor  = Colors.appThemeColorText
//
//
//
//        present(autocompleteController, animated: true, completion: nil)
//    }

