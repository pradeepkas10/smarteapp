 //
 //  HomeDashBoardVC.swift

 //
 //  Created by Surbhi on 13/10/17.
 //  Copyright © 2017 Hitesh Dhawan. All rights reserved.
 //

 import UIKit
 import GooglePlacePicker
 import GoogleMaps
 import CoreData


 class HomeDashBoardVC: ReachabilityViewController,ENSideMenuDelegate,toGetAddress,toUpdateBookingCompletionData{
    
    @IBOutlet weak var btnSideMenu:UIButton!
    @IBOutlet weak var viewpassangersCount:UIView!
    @IBOutlet weak var viewbotom:UIView!
    @IBOutlet weak var viewbookRide:UIView!
    @IBOutlet weak var viewHeader:UIView!
    @IBOutlet weak var viewHeadermain:UIView!
    @IBOutlet weak var viewSaveAddress:UIView!
    @IBOutlet weak var viewMap:GMSMapView!
    @IBOutlet weak var btn_pickupaddressHeight: NSLayoutConstraint!
    @IBOutlet weak var view_HeaderHeight: NSLayoutConstraint!
    @IBOutlet weak var view_bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var txt_otheraddressHeight: NSLayoutConstraint!
    @IBOutlet weak var view_saveaddressHeight: NSLayoutConstraint!
    @IBOutlet weak var lblSoloRide:UIButton!
    @IBOutlet weak var lblSharedRide:UIButton!
    @IBOutlet weak var lblChooseRide:UILabel!
    @IBOutlet weak var btn_Home:UIButton!
    @IBOutlet weak var btn_work:UIButton!
    @IBOutlet weak var btn_other:UIButton!
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var txt_Otheraddress:UITextField!
    @IBOutlet weak var txt_saveaddress:UITextField!
    @IBOutlet weak var lbl_saveaddress:UILabel!
    @IBOutlet weak var btnSoloRide:UIButton!
    @IBOutlet weak var btnSharedRide:UIButton!

    var tag = 1 /// tag 1 = pickup address button and 2 = destinnation address
    var destinationLocation1 = CLLocationCoordinate2D()
    var source = CLLocationCoordinate2D()
    var destinationMarker = GMSMarker()
    //var destinationMarker = GMSMarker()
    var btn_pickupaddress :UIButton!
    var btn_destaddress :UIButton!
    var btn_switchAddress:UIButton!//for save Address
    var stackView   = UIStackView()
    var view_address = UIView()
    var placesClient: GMSPlacesClient!
    var locationManager = CLLocationManager()
    var str_latitude = ""
    var str_longitude = ""
    var originId = ""
    var destaddressId = ""
    var originAddress = ""
    var destAddress = ""
    var str_rideType = ""
    var str_addressType = ""
    var str_addressTypeId = ""

    var zoomLevel:Float = 16.37
    var arrAllroute = [[String:AnyObject]]()
    var userId = UserDefaults.standard.value(forKey: Constants.USERID) as! Int64
    var viewForPin = UIView()
    lazy var pinView: UIImageView = { [unowned self] in
        let v = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        v.image = #imageLiteral(resourceName: "marker")//UIImage(named: "marker", in: Bundle(for: HomeDashBoardVC.self), compatibleWith: nil)
        v.backgroundColor = .clear
        v.clipsToBounds = true
        v.contentMode = .scaleAspectFit
        v.isUserInteractionEnabled = false
        return v
        }()
    var ComingFromAutoComplete = false
    var curretnLocation :CLLocationCoordinate2D?
    var currentLocationBtnClicked = false
    var firstTimeEntry = true
    var bookingRideDetails:BookingRideDetails?
    
    //MARK:- View Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSaveAddress.isHidden = true
        UIApplication.shared.isStatusBarHidden = false
        locationManager.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 50
        self.viewMap.isMyLocationEnabled=true
        viewMap.settings.myLocationButton = true
        viewMap.setMinZoom(8.73368263, maxZoom: 18.1021137)
        //viewMap.maxZoom = 18.1021137
        //8.73368263
        viewForPin.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        
        pinView.center = CGPoint(x: viewForPin.center.x, y: viewForPin.center.y/2)
        
        //viewForPin.backgroundColor = .lightGray
        viewForPin.addSubview(pinView)
        view.addSubview(viewForPin)
        viewForPin.center = self.view.center
        updateBtnSoloRideStatus(selected: false)
        //self.viewHeader.isHidden = false
        viewMap.delegate = self
        viewMap.isIndoorEnabled = true
        viewMap.accessibilityElementsHidden = true
        bookingCompletionDelegate = self
        self.locationManager.startUpdatingLocation()
        toSetTopViewsForAddresses()

    }



    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        delegateAddress = self
        self.hideSideMenuView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        userId = UserDefaults.standard.value(forKey: Constants.USERID) as! Int64
        self.view.bringSubview(toFront: btnSideMenu)
        Helper.ToHideIndicator()
        var toview = Bool()
        toview = UserDefaults.standard.bool(forKey: "FromNotification")
        if toview
        {
            UserDefaults.standard.set(false, forKey: "FromNotification")
            UserDefaults.standard.synchronize()
            let viewcontroller = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
        else
        {
            toMakeDecisionForRideIsInProcess()
        }
        self.sideMenuController()?.sideMenu?.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        //        Helper.setImageOntextField(self.txt_Otheraddress, imagename: "heart", mode: true, frame: CGRect(x: -10, y: 5, width: 30, height: 20))
        //        Helper.setImageOntextField(self.txt_saveaddress, imagename: "0", mode: true, frame: CGRect(x: -10, y: 5, width: 30, height: 20))
        let insetForLogog = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 149, right: 0.0)
        viewMap.padding = insetForLogog
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //viewMap.removeObserver(self, forKeyPath: "myLocation")
        
    }
    
    override var prefersStatusBarHidden: Bool{
        return false
    }

    //MARK : to update data after notification received
    func updatedatawithnotifcation(dict: NSDictionary) {

    }
    // MARK:- update ride status
    func updateBtnSoloRideStatus(selected:Bool){
        let image = selected ? #imageLiteral(resourceName: "Share_selected") : #imageLiteral(resourceName: "Share_Unselected")
        if str_rideType == "1" {
            DispatchQueue.main.async {
                self.btnSoloRide.setImage(image, for: .normal)
                self.lblSoloRide.titleLabel?.textColor = selected ? Colors.appThemeColorText : .gray
            }
        }
        else
        {
            DispatchQueue.main.async {
                self.btnSharedRide.setImage(image, for: .normal)
                self.lblSharedRide.titleLabel?.textColor = selected ? Colors.appThemeColorText : .gray
            }
        }
    }

    // MARK:-  draw polyline with static lat long for shared smartE
    func DrawPolylineWithLatLong(arr: [[String : Any]]){
        DispatchQueue.main.async {

            let gmsPath = GMSMutablePath()
            for point in arr {

                let latitude:Double = Double(point["Latitude"] as! String)!
                let Longitude:CLLocationDegrees = Double(point["Longitude"] as! String)!
                //gmsPath.addLatitude(point["Latitude"] as! Double, longitude: (point["Longitude"] as? Double)!)
                gmsPath.addLatitude(latitude, longitude: Longitude)

            }

            let polyline1 = GMSPolyline(path: gmsPath)
            polyline1.strokeWidth = 5.0
            polyline1.strokeColor = Colors.appThemeColorText
            polyline1.geodesic = true
            let color = UIColor(red: 255.0/255, green: 105.0/255, blue: 180.0/255, alpha: 1.0)

            let styles = [GMSStrokeStyle.solidColor(color),
                          GMSStrokeStyle.solidColor(.white)]
            let lengths: [NSNumber] = [15, 15]
            polyline1.spans = GMSStyleSpans(polyline1.path!, styles, lengths, GMSLengthKind.rhumb)
            // Create two styles: one that is solid blue, and one that is a gradient from red to yellow
            //        let solidBlue = GMSStrokeStyle.solidColor(.blue)
            //        let solidBlueSpan = GMSStyleSpan(style: solidBlue)
            //        let redYellow =
            //            GMSStrokeStyle.gradient(from: .red, to: .yellow)
            //        let redYellowSpan = GMSStyleSpan(style: redYellow)

            // for gradient color in polyline
            //        let solidRed = GMSStrokeStyle.solidColor(.red)
            //        let redYellow =
            //            GMSStrokeStyle.gradient(from: .red, to: .yellow)
            //        polyline1.spans = [GMSStyleSpan(style: solidRed),
            //                          GMSStyleSpan(style: solidRed),
            //                          GMSStyleSpan(style: redYellow)]

            UIView.animate(withDuration: 1.0) {
                polyline1.map = self.viewMap
            }
        }
    }


    // MARK:- show nearby smarte on map
    func showAllNerestSmartEOnMapView(location:CLLocationCoordinate2D?){
        if location == nil {
            return
        }
        let urlString  =  Constants.BASEURL + "FindVehicleAlongWithLocation"
        let paramdict  = [
            "Latitude": "\(location!.latitude)",
            "Longitude": "\(location!.longitude)"
        ]


        if Helper.isConnectedToNetwork(){
            Server.PostDataInDictionary(urlString, dict_data: paramdict as Dictionary<String, AnyObject>, completionHandler: { (response) in


                if response != nil && response?["MessageCode"] as! String == "DRTF04"{

                    let dic_data = response?["Data"] as? [String:AnyObject]
                    let arrLatLong = dic_data?["VehicleResponses"] as? [[String:AnyObject]]
                    let dic_routes = dic_data?["Routes"] as? [String:AnyObject]
                    self.arrAllroute = (dic_routes?["Locations"] as? [[String:AnyObject]])!
                    DispatchQueue.main.async {
                        if self.arrAllroute.count > 0
                        {
                            Helper.ToShowIndicator()
                            for i in 0..<self.arrAllroute.count
                            {
                                let arroute = self.arrAllroute[i]
                                let arrlocations = arroute["Location"] as? [[String:AnyObject]]
                                self.DrawPolylineWithLatLong(arr: arrlocations!)

                            }

                        }

                        //self.viewMap.clear()
                        for pin in arrLatLong!{
                            let state_marker = GMSMarker()
                            state_marker.position = CLLocationCoordinate2D(latitude: Double(pin["CurrentLatitude"] as! String)!, longitude: Double(pin["CurrentLongitude"] as! String)!)
                            state_marker.icon = #imageLiteral(resourceName: "DriverMarker")
                            state_marker.map = self.viewMap
                        }
                        Helper.ToHideIndicator()
                    }
                }
            })
        }
    }
    // MARK:- to check if ride is in progress
    func toMakeDecisionForRideIsInProcess(){
        // let status =  UserDefaults.standard.value(forKey: Constants.RideStatus) as? String
        toGetStatusOfRide(completionHandler: { (Result) in
            if Result{
                let when = DispatchTime.now() + 0.5
                if self.bookingRideDetails?.RideStatus == 3{
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RideStartViewController") as! RideStartViewController
                        destViewController.bookingRideDetails = self.bookingRideDetails
                        self.navigationController?.pushViewController(destViewController, animated: true)
                    }
                }else{
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RouteMapVC") as! RouteMapVC
                        destViewController.bookingRideDetails = self.bookingRideDetails
                        self.navigationController?.pushViewController(destViewController, animated: true)
                    }
                }
            }
        })

        
    }
    
    
    func toGetStatusOfRide(completionHandler:@escaping (_ response: Bool) -> Void) {
        let userID:String = String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
        let url = Constants.BASEURL + "OnlineCheckPassengerStatus" + "?UserID=" + "\(userID)"
        Helper.ToShowIndicator()

        if Helper.isConnectedToNetwork(){
            Server.getRequestWithURL(urlString: url, completionHandler: { (response) in
                Helper.ToHideIndicator()

                if response != nil && response?["MessageCode"] as! String == "Act12"{
                    let resultData = response?["Data"] as? [String:AnyObject]
                    let result = resultData?["PassengerData"] as? [String:AnyObject]
                    let Sourcelatlong = CLLocation.init(latitude: Double(result!["SourceLatitude"] as! String)!, longitude: Double(result!["SourceLongitude"] as! String)!)
                    let DestinationLatLong = CLLocation.init(latitude: Double(result!["DestinationLatitude"] as! String)!, longitude: Double(result!["DestinationLongitude"] as! String)!)
                    
                    var RouteArr = [CLLocation]()
                    let arrLocation = resultData!["Location"] as! [[String:AnyObject]]
                    for point in arrLocation{
                        let lat = Double(point["Latitude"] as! String)!
                        let long = Double(point["Longitude"] as! String)!
                        let location = CLLocation(latitude: lat, longitude: long)
                        RouteArr.append(location)
                    }

                    self.bookingRideDetails = BookingRideDetails.init(bookingID: result!["BookingID"] as! Int, bookingLogID: 0, RouteID: result!["RouteID"] as! Int, Source: Sourcelatlong, Destination: DestinationLatLong, SourceAdd: result!["PickUpPoint"] as! String, DestinationAdd: result!["DroffPoint"] as! String, DriverName: result!["DriverName"] as! String, PaymentStatus: result!["PaymentStatus"] as! Int, RideStatus: result!["RideStatus"] as! Int, DriverID: result!["DriverID"] as! Int, VechicleNumber: result!["VehicleRegistrationNo"] as! String,routeArr:RouteArr,DriverMobileNumber:result!["DriverMobileNumber"] as! String, BookingType: self.str_rideType)
                    
                    DispatchQueue.main.async {
                        completionHandler(true)
                    }
                }else{
                    completionHandler(false)
                }
            })
            
        }
    }
    // MARK:- for update address from autocomplete
    func updateAddress(dict: [String : Any]) {

        Helper.ToShowIndicator()
        print(dict)
        //
        let newString =  "\(dict["First"]!) \(dict["Second"]!)" 

        if tag == 1 {
            self.originId = dict["placeId"] as! String
            print("formatterrr update destination auto === \(newString)")
            self.btn_pickupaddress.setTitle(newString, for: .normal)
            self.btn_pickupaddress.setNeedsUpdateConstraints()
            self.btn_pickupaddress.setTitleColor(Colors.appThemeColorTextgray, for: .normal)
            // source = dict["coordinate"] as! CLLocationCoordinate2D
            source = CLLocationCoordinate2D(latitude: CLLocationDegrees(dict["lat"] as! String)!, longitude: CLLocationDegrees(dict["lng"] as! String)!)
            viewMap.camera = GMSCameraPosition.camera(withTarget: source, zoom: zoomLevel)
        }
        else
        {
            self.destaddressId = dict["placeId"] as! String
            self.btn_destaddress.setTitle(newString, for: .normal)
            self.btn_destaddress.setNeedsUpdateConstraints()
            self.btn_destaddress.setTitleColor(UIColor.gray,  for: .normal)
            // self.destinationLocation1 = dict["coordinate"] as! CLLocationCoordinate2D
            self.destinationLocation1 = CLLocationCoordinate2D(latitude: CLLocationDegrees(dict["lat"] as! String)!, longitude: CLLocationDegrees(dict["lng"] as! String)!)

            if UserDefaults.standard.bool(forKey: "Favourite")
            {
                btn_switchAddress.setImage(#imageLiteral(resourceName: "heart_Filled"), for: .normal)
            }
            else
            {
                btn_switchAddress.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
            }

            self.btn_switchAddress?.isHidden = false
            //viewMap.camera = GMSCameraPosition.camera(withTarget: destinationLocation1, zoom: zoomLevel)
            
        }
        Helper.ToHideIndicator()
    }
    // MARK:- for update status after booking completion
    func updateResultAfterBookingCompletion() {
        DispatchQueue.main.async {
            self.updateBtnSoloRideStatus(selected: false)
            self.bookingRideDetails = nil
            self.btn_destaddress.setTitle("Drop Location", for: .normal)
            self.btn_switchAddress?.isHidden = true

        }

    }
    //
    func getDistanceRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        if Helper.isConnectedToNetwork(){

            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)

            let str = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=\(source.latitude),\(source.longitude)&destinations=\(destination.latitude),\(destination.longitude)&mode=driving&key=\(KEYS.GoogleKey)"
            // let str = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&mode=driving&key=\(KEYS.GoogleKey)"

            let url = URL.init(string: str)

            let task = session.dataTask(with: url!, completionHandler: {
                (data, response, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    PKSAlertController.alert(Constants.appName, message:(error?.localizedDescription)!)

                }else{
                    do {
                        if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{

                            if json["status"] as? String == "OK"
                            {
                                let rows = json["rows"] as? [Any]

                                if (rows?.count)! > 0
                                {
                                    let rows1 = rows?[0] as!  Dictionary<String , Any>
                                    let elements = rows1["elements"] as? [Any]
                                    //  let personDictionary = (elements as! NSArray).mutableCopy() as! NSMutableArray
                                    let legsarray = elements![0] as? Dictionary<String,Any>
                                    let distance = legsarray!["distance"] as? Dictionary<String,Any>
                                    let distanceValue  = distance!["value"] as! NSInteger

                                    if distanceValue <= 6099
                                    {
                                        self.callApiInitiateBooking()
                                    }
                                    else
                                    {        Helper.ToHideIndicator()
                                        PKSAlertController.alert(Constants.appName, message: "Your Travel distance is more than 6km. Please select your destination location between 0 to 6km")
                                        self.updateBtnSoloRideStatus(selected: false)

                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        print("error in JSONSerialization")
                    }
                }
            })
            task.resume()
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }

    func callApiInitiateBooking(){

        let urlString  =  Constants.BASEURL+"InitiateRequestForRide"
        let paramdict : [String : Any] = ["BookingType": self.str_rideType,
                                          "SourceLatitude": "\(source.latitude)",
            "SourceLongitude": "\(source.longitude)",
            "pickuppoint": originAddress,
            "Dropoffpoint": destAddress,
            "DestinationLatitude": "\(destinationLocation1.latitude)",
            "DestinationLongitude": "\(destinationLocation1.longitude)"]
        print(paramdict)
        Server.PostDataInDictionary(urlString, dict_data: paramdict as Dictionary<String, AnyObject>, completionHandler: { (response) in

            if (response != nil) {
                if response?["MessageCode"] as! String == "R02"{
                    var dictRespone = response?["Data"] as! Dictionary<String,Any>
                    let dict = dictRespone["PassengerData"] as! [String:AnyObject]
                    let arrLocation = dictRespone["Location"] as! [[String:AnyObject]]

                    DispatchQueue.main.async {

                        let route = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RouteMapVC") as! RouteMapVC
                        self.originAddress = dict["pickupPoint"] as! String
                        self.destAddress = dict["dropoffPoint"] as! String

                        let destinationLatLong = CLLocationCoordinate2D(latitude: Double(dict["DestinationLatitude"] as! String)!, longitude:  Double(dict["DestinationLongitude"] as! String)!)

                        let SourceLatLong = CLLocationCoordinate2D(latitude: Double(dict["SourceLatitude"] as! String)!, longitude: Double(dict["SourceLongitude"] as! String)!)

                        let DestinationClLocation = CLLocation(latitude: destinationLatLong.latitude, longitude: destinationLatLong.longitude)
                        let SourceClLocation = CLLocation(latitude: SourceLatLong.latitude, longitude: SourceLatLong.longitude)
                        var RouteArr = [CLLocation]()

                        for point in arrLocation{
                            let lat = Double(point["Latitude"] as! String)!
                            let long = Double(point["Longitude"] as! String)!
                            let location = CLLocation(latitude: lat, longitude: long)
                            RouteArr.append(location)
                        }

                        
                        self.updateBtnSoloRideStatus(selected: false)
                        Helper.ToHideIndicator()
                        self.bookingRideDetails = BookingRideDetails.init(bookingID: 0, bookingLogID: (dict["BookingLogID"] as? Int)!, RouteID: (dict["RouteID"] as? Int)!, Source: SourceClLocation, Destination: DestinationClLocation, SourceAdd: self.originAddress, DestinationAdd: self.destAddress, DriverName: "", PaymentStatus: 0, RideStatus: 0, DriverID: 0, VechicleNumber: "",routeArr:RouteArr,DriverMobileNumber:"", BookingType:self.str_rideType)
                        route.bookingRideDetails = self.bookingRideDetails
                        route.str_rideType = self.str_rideType
                        self.navigationController?.pushViewController(route, animated: true)

                    }
                }
                else if response?["MessageCode"] as! String == "Act20" || response?["MessageCode"] as! String == "R01"
                {
                    self.updateBtnSoloRideStatus(selected: false)
                    Helper.ToHideIndicator()
                    PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)

                }
                else{
                    self.updateBtnSoloRideStatus(selected: false)
                    print("errror")
                    Helper.ToHideIndicator()

                }
            }
        })
    }
    
    //MARK:- Button Actions
    @IBAction func btnSoloRideClicke(_ sender: UIButton){
        if btn_destaddress.titleLabel?.text == "Drop Location"{
            PKSAlertController.alert(Constants.appName, message:"Please select a drop point")
        }
        else
        {
            str_rideType = "1"

            updateBtnSoloRideStatus(selected: true)
            originAddress = (self.btn_pickupaddress.titleLabel?.text)!
            destAddress = (self.btn_destaddress.titleLabel?.text)!
            Helper.ToShowIndicator(msg: "Looking for SmartE near you")
            self.getDistanceRoute(from: source, to: destinationLocation1)
            // self.callApiInitiateBooking()
        }
    }
    @IBAction func btnHomeClicke(_ sender: UIButton){
        str_addressType = "Home"
        str_addressTypeId = "0"
        txt_otheraddressHeight.constant = 0.0
        view_saveaddressHeight.constant = 165
        btn_Home.setImage(#imageLiteral(resourceName: "radioselected"), for: .normal)
        btn_work.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
        btn_other.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
        img.isHidden = true
    }
    @IBAction func btnWorkClicke(_ sender: UIButton){
        str_addressType = "Work"
        str_addressTypeId = "1"
        btn_Home.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
        btn_work.setImage(#imageLiteral(resourceName: "radioselected"), for: .normal)
        btn_other.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
        txt_otheraddressHeight.constant = 0.0
        view_saveaddressHeight.constant = 165//view_saveaddressHeight.constant-35
        img.isHidden = true

    }
    @IBAction func btnOtherClicke(_ sender: UIButton){
        str_addressType = "Other"
        str_addressTypeId = "2"
        img.isHidden = false
        btn_Home.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
        btn_work.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
        btn_other.setImage(#imageLiteral(resourceName: "radioselected"), for: .normal)
        txt_otheraddressHeight.constant = 35.0
        view_saveaddressHeight.constant = 200 //view_saveaddressHeight.constant+35

    }
    @IBAction func btnSaveClicke(_ sender: UIButton){
        let dict = UserDefaults.standard.value(forKey: Constants.SAVEADDRESS) as! [String:Any]
        print(dict)
        let newString =  "\(dict["First"]!) \(dict["Second"]!)"
        self.destaddressId = dict["placeId"] as! String

        let addresslat = dict["lat"]
        let addresslng = dict["lng"]

        if str_addressType != "Home" && str_addressType != "Work"{
            str_addressType = (txt_Otheraddress?.text)!
        }
        self.deleteTask(withUUID: str_addressType)
        self.saveFavouriteDetails(id:userId, location_address: "\(dict["First"]!)",location_address2: "\(dict["Second"]!)", location_id: dict["placeId"] as! String, locationLatitude:addresslat as! String, location_longitude: addresslng as! String, location_type: str_addressTypeId, location_title: str_addressType)
        self.viewSaveAddress.isHidden = true

    }
    @IBAction func btncancelClicke(_ sender: UIButton){
        self.viewSaveAddress.isHidden = true
    }

    @IBAction func btnInfoClicked(_ sender: UIButton){
        //        self.fetchdata()
        let infoViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        infoViewController.index = 0
        infoViewController.modalPresentationStyle = .overFullScreen
        present(infoViewController, animated: true, completion: nil)
    }

    @IBAction func btnInfoClickedsolo(_ sender: UIButton){
        let infoViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
        infoViewController.index = 1
        infoViewController.modalPresentationStyle = .overFullScreen
        present(infoViewController, animated: true, completion: nil)
    }
    
    @IBAction func drawRoute(_ sender: UIButton){
        if btn_destaddress.titleLabel?.text == "Drop Location"{
            PKSAlertController.alert(Constants.appName, message:"Please select a drop point")
        }
        else
        {
            str_rideType = "2"

            updateBtnSoloRideStatus(selected: true)
            originAddress = (self.btn_pickupaddress.titleLabel?.text)!
            destAddress = (self.btn_destaddress.titleLabel?.text)!
            Helper.ToShowIndicator(msg: "Looking for SmartE near you")
            self.callApiInitiateBooking()
        }
    }

    @IBAction func drawRoute2(_ sender: UIButton){

    }
    
    @IBAction func autocompleteClickeddemo(_ sender: UIButton){
        tag = sender.tag
        currentLocationBtnClicked = true
        ComingFromAutoComplete = tag == 1 ? true:false
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        destViewController.str_header = tag == 1 ? "Enter pickup location" : "Enter drop location"
        self.navigationController?.present(destViewController, animated: true, completion: nil)
        
        //        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RideStartViewController") as! RideStartViewController
        //        self.navigationController?.present(destViewController, animated: true, completion: nil)
    }
    
    
    //    @IBAction func btn_switchAddress(_ sender: Any) {
    //        if btn_destaddress.titleLabel?.text == "Drop Location"{
    //            return
    //        }
    //        let sourceAdd = self.btn_pickupaddress.titleLabel?.text
    //        let sourcelatlong = source
    //
    //        self.btn_pickupaddress.setTitle(self.btn_destaddress.titleLabel?.text, for: .normal)
    //        self.btn_destaddress.setTitle(sourceAdd, for: .normal)
    //
    //        self.source = destinationLocation1
    //        self.destinationLocation1 = sourcelatlong
    //        currentLocationBtnClicked = true
    //        viewMap.camera = GMSCameraPosition.camera(withTarget: source, zoom: zoomLevel)
    //    }
    @IBAction func btn_switchAddress(_ sender: Any) {
        DispatchQueue.main.async {


                    let dict = UserDefaults.standard.value(forKey: Constants.SAVEADDRESS) as! [String:Any]
                    print(dict)
                    self.destaddressId = dict["placeId"] as! String

                    let request = NSFetchRequest<Favourite>(entityName: "Favourite")
                    request.returnsObjectsAsFaults = false
                    //let predicate = [NSPredicate predicateWithFormat:"%K = %@", "planID", @(30)] as! NSPredicate
                    do {
                        let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
                        print(searchResults.count)
                        print(searchResults)
                        var str_bool = true

                        for task in searchResults {
                            print(task)
                            if task.id == self.userId
                            {
                            if task.location_id  == self.destaddressId
                            {
                                str_bool = false
                                PKSAlertController.alert("Already a favourite!",message:"This location is already saved as your favourite.")
                                break
                            }
                            else
                            {
                                str_bool = true
                            }
                            }
                        }
                        if str_bool
                        {
                            self.viewSaveAddress.isHidden = false
                            self.pinView.isHidden = true
                            self.lbl_saveaddress.text = self.btn_destaddress.titleLabel?.text

                            self.str_addressType = "Home"
                            self.str_addressTypeId = "0"
                            self.txt_Otheraddress.text = ""
                            self.txt_otheraddressHeight.constant = 0.0
                            self.view_saveaddressHeight.constant = 165
                            self.btn_Home.setImage(#imageLiteral(resourceName: "radioselected"), for: .normal)
                            self.btn_work.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
                            self.btn_other.setImage(#imageLiteral(resourceName: "RadioUnselected"), for: .normal)
                            self.img.isHidden = true

                        }
                    }
                    catch let error as NSError {
                        print("Could not save. \(error), \(error.userInfo)")
                    }

        }
    }

    @IBAction func SideMenuPressed(_ sender: Any) {
        UIApplication.shared.isStatusBarHidden = true
        toggleSideMenuView()
    }
    
    
    //MARK:- Extraa Functions
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(x:0,y: 0,width: newSize.width,height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    
    // for animate view
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })

        //        alertController.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction) -> Void in

    }
    
    func toUpdateAddressInViews(results1:[String:Any]){
        let newString = results1["formatted_address"]!
        self.originId = results1["place_id"]! as! String
        
        print("formatterrr update destination === \(newString)")
        DispatchQueue.main.async {
            // if tag == 1 {
            self.btn_pickupaddress.setTitle(newString as? String, for: .normal)
            self.btn_pickupaddress.setNeedsUpdateConstraints()
            self.btn_pickupaddress.setTitleColor(Colors.appThemeColorTextgray, for: .normal)
            // }
        }
    }

    //MARK: CoreData Methods

    func deleteTask(withUUID: String) {

        let request = NSFetchRequest<Favourite>(entityName: "Favourite")
        do {
            let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
            print(searchResults)
            for task in searchResults {

                if userId == task.id{

                    if task.location_title == str_addressType {
                        // delete task
                        Constants.appDelegate.persistentContainer.viewContext.delete(task)

                        // self.fetchdata()
                    }
                }

            }
        } catch {
            print("Error with request: \(error)")
        }
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }

    func saveFavouriteDetails(id:Int64,location_address:String,location_address2:String,location_id:String,locationLatitude:String,location_longitude:String,
                              location_type:String,location_title:String)
    {
        let request = NSFetchRequest<Favourite>(entityName: "Favourite")
        request.returnsObjectsAsFaults = false
        do {

            let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
            print(searchResults)
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }

            // 1
            let managedContext = appDelegate.persistentContainer.viewContext


            // 2
            let entity = NSEntityDescription.entity(forEntityName: "Favourite", in: managedContext)!
            let category = NSManagedObject(entity: entity, insertInto: managedContext)
            category.setValue(id, forKeyPath: "id")
            category.setValue(location_address, forKeyPath: "location_address")
            category.setValue(location_address2, forKeyPath: "location_address2")
            category.setValue(location_id, forKeyPath: "location_id")
            category.setValue(location_longitude, forKeyPath: "location_longitude")
            category.setValue(locationLatitude, forKeyPath: "locationLatitude")
            category.setValue(location_type, forKeyPath: "location_type")
            category.setValue(location_title, forKeyPath: "location_title")
            do {
                try managedContext.save()
                // arr_PlanDetails.append(category)
                self.btn_switchAddress.setImage(#imageLiteral(resourceName: "heart_Filled"), for: .normal)
            }
            catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }

    func fetchdata()
    {

        let request = NSFetchRequest<Favourite>(entityName: "Favourite")
        request.returnsObjectsAsFaults = false
        //let predicate = [NSPredicate predicateWithFormat:"%K = %@", "planID", @(30)] as! NSPredicate
        do {
            let searchResults = try Constants.appDelegate.persistentContainer.viewContext.fetch(request)
            print(searchResults.count)
            print(searchResults)
            for task in searchResults {
                print(task)
                //  let predicate = NSPredicate(format: "%K == %@", "planID", task.planID)
            }
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }

 }

 extension HomeDashBoardVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("did update location")
        if let location = locations.first {
            let value = 0.0//0001
            curretnLocation = location.coordinate
            self.viewMap.moveCamera(.setCamera(GMSCameraPosition(target: self.curretnLocation!, zoom: self.zoomLevel, bearing: 0, viewingAngle: 0)))
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                let latlong = CLLocationCoordinate2D.init(latitude: ((self.curretnLocation?.latitude)!+value) , longitude: ((self.curretnLocation?.longitude)!+value) )
                let point = self.viewMap.projection.point(for: latlong)
                self.viewForPin.center = CGPoint(x: point.x , y: point.y)
            }
            
            locationManager.stopUpdatingLocation()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted{
            //Show Alert
        }
        
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    
 }

 extension HomeDashBoardVC : GMSMapViewDelegate {
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                PKSAlertController.alertForNetwok(title: "No current location access", message: "Please provide access for current location to get current location")
            case .authorizedAlways, .authorizedWhenInUse:
                viewMap.animate(with: GMSCameraUpdate.setTarget(curretnLocation!, zoom: zoomLevel))//animate(toLocation: curretnLocation!)
                currentLocationBtnClicked = true
            }
        } else {
            PKSAlertController.alertForNetwok(title: "No current location access", message: "Please provide access for current location to get current location")
            
        }
        
        return true
    }


    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if !currentLocationBtnClicked{
            self.view_bottomHeight.constant = 0
            self.view_HeaderHeight.constant = 0
            setView(view: view_address, hidden: true)
            setView(view: viewHeader, hidden: true)
            setView(view: viewbookRide, hidden: true)
            UIView.animate(withDuration: 0.7) {
                // self.view_address.isHidden = true
                self.stackView.isHidden = true
                //self.viewMap.settings.myLocationButton = false
                self.view.layoutIfNeeded()
            }
        }else{
            currentLocationBtnClicked = false
        }
        //let insetForLogog = UIEdgeInsets.init(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        //viewMap.padding = insetForLogog
    }

    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {

        if !currentLocationBtnClicked{
            self.view_bottomHeight.constant = 147
            self.view_HeaderHeight.constant = 200
            //self.btn_pickupaddressHeight.constant = 110
            setView(view: view_address, hidden: false)
            setView(view: viewHeader, hidden: false)
            setView(view: viewbookRide, hidden: false)
            viewMap.isMyLocationEnabled = true
        }else{
            currentLocationBtnClicked = false
        }
        
        var destinationLocation = CLLocation()
        if tag != 2  && !ComingFromAutoComplete{
            let point = viewForPin.center//CGPoint.init(x : pinView.center.x, y: pinView.center.y)
            var currentCoordinate:CLLocationCoordinate2D!
            if curretnLocation != nil{
                currentCoordinate = firstTimeEntry ? curretnLocation! : viewMap.projection.coordinate(for: point)
                firstTimeEntry = false
            }else{
                currentCoordinate = viewMap.projection.coordinate(for: point)
            }
            self.showAllNerestSmartEOnMapView(location: currentCoordinate!)
            destinationLocation = CLLocation.init(latitude: currentCoordinate.latitude, longitude: currentCoordinate.longitude)
            source = destinationLocation.coordinate



            let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(source.latitude),\(source.longitude)&key=\(KEYS.GoogleKey)")!
            
            print("url === \n \(url.absoluteString)")
            if Helper.isConnectedToNetwork(){
                DispatchQueue.main.async {
                    Server.getRequestWithURL(urlString: url.absoluteString, completionHandler: { (response) in
                        if response != nil {
                            let results = response?["results"] as? [Any]
                            if (results?.count)! > 0{
                                let results1 = results?[0] as? [String:Any]
                                DispatchQueue.main.async {
                                    self.toUpdateAddressInViews(results1: results1!)
                                }
                            }else{

                                // PKSAlertController.alert(Constants.appName, message:"json error")
                            }
                        }else{
                            print("errror")
                        }
                    })
                }
            }else{
                PKSAlertController.alertForNetwok()
            }
        }else{
            tag = 0
            ComingFromAutoComplete = false
        }
    }
 }

