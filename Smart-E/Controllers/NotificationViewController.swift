//
//  NotificationViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 17/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController{
    
    @IBOutlet weak var tblView:UITableView!

    var arrNotificationListing = [[String:AnyObject]]()

    
    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.tableFooterView = UIView()
        tblView.estimatedRowHeight = 100

        toGetListingOfNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- button action
    @IBAction func BackButtonPressed(sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- get data

    func toGetListingOfNotification(){
        let userID:String = String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
        let url = Constants.BASEURL + "NotificationList" + "?UserID=" + "\(userID))"
        Helper.ToShowIndicator()
        if Helper.isConnectedToNetwork(){
            Server.getRequestWithURL(urlString: url, completionHandler: { (response) in
                Helper.ToHideIndicator()
                // print(response)
                if response != nil && (response?["MessageCode"] as! String) == "NF01" {
                    let results = response?["Data"] as? [[String:AnyObject]]
                    self.arrNotificationListing = results!
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
                else{
                    let str_message = response == nil  ? "Server Error" : response?["message"] as! String
                    PKSAlertController.alert(Constants.appName, message: str_message, buttons: ["OK"]) { (alertAction, index) in
                        print("index === \(index)")
                        if index == 0 {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            })
        }
        else
        {
            PKSAlertController.alertForNetwok()
        }
    }
}





extension NotificationViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrNotificationListing.count == 0{
            Helper.SetBalnkViewForEmptyTableView(tblView: tableView, message: "There is no Notification.")
            return 0
        }
        tableView.backgroundView = nil
        return arrNotificationListing.count

    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
        cell.selectionStyle = .none
        
        let dict = arrNotificationListing[indexPath.item]
        
        let lblHeader = cell.viewWithTag(100) as! UILabel
        let lblDesc = cell.viewWithTag(101) as! UILabel
        
        lblHeader.text = dict["Content"] as? String
        lblDesc.text = dict["Discription"] as? String

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


