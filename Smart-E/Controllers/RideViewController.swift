//
//  RideViewController.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 10/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit

enum DecideRideStaus:String{
    case Cancelled
    case Free
    case Normal
}


class RideViewController: UIViewController {
    
    @IBOutlet weak var tblView:UITableView!
    
    
    var arrRideListing = [[String:AnyObject]]()
    
    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Helper.ToShowIndicator()
        toGetListingOfRide()
        Helper.ToHideIndicator()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- button action
    
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- get data
    
    func toGetListingOfRide(){
        let userID:String = String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
        let url = Constants.BASEURL + MethodName.passengerBookingReport + "?UserID=" + "\(userID)"

        print(url)
        Helper.ToShowIndicator()
        if Helper.isConnectedToNetwork(){
            Server.getRequestWithURL(urlString: url, completionHandler: { (response) in
                Helper.ToHideIndicator()
                print(response!)
                if response != nil && response?["MessageCode"] as! String == "Act12"{
                    let results = response?["Data"] as? [[String:AnyObject]]
                    self.arrRideListing = results!
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }else{
                    //PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
                    DispatchQueue.main.async {
                        self.tblView.reloadData()
                    }
                }
            })
        }else{
            PKSAlertController.alertForNetwok()
        }
        Helper.ToHideIndicator()
    }
  }


extension RideViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrRideListing.count == 0{
            Helper.SetBalnkViewForEmptyTableView(tblView: tableView, message: "There is no Ride History.")
            return 0
        }
        tableView.backgroundView = nil
        return arrRideListing.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideTableViewCell", for: indexPath) as! RideTableViewCell
        cell.selectionStyle = .none
        let dict = arrRideListing[indexPath.item]
        print(dict)
        cell.setData(dataDict:dict )
        if dict["RideStatus"] as! String == "Completed"{
            cell.toDecideRideStatus(status: .Normal)

        }
        if dict["RideStatus"] as! String == "Cancelled"{
            cell.toDecideRideStatus(status: .Cancelled)
        }
     

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
}


class RideTableViewCell:UITableViewCell{
    @IBOutlet weak var lblDateAndTime:UILabel!
    @IBOutlet weak var lblRideType:UILabel!

    @IBOutlet weak var lblNumber:UILabel!
    @IBOutlet weak var lblBookingID:UILabel!
    @IBOutlet weak var lblSourceAdd:UILabel!
    @IBOutlet weak var lblDestinationAdd:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var imgViewFree:UIImageView!
    @IBOutlet weak var imgViewCancelled:UIImageView!
    @IBOutlet weak var viewBlur: UIVisualEffectView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = 25
        viewBlur.layer.cornerRadius = 25 
    }
    
    
    func setData(dataDict:[String:AnyObject]?){
        if let dateStr = dataDict!["BookingTime"] as? String{
            self.lblDateAndTime.text = Helper.ToConvertDateFromOneFormatToAnotherFormat(dateStr: dateStr, sourceFormat: "yyyy-MM-dd HH:mm:ss", destinationFormat: "EEE, dd MMMM yyyy | hh:mm a")
        }
        if let address = dataDict!["pickupPoint"] as? String{
            self.lblSourceAdd.text = address
        }
        
        if let address = dataDict!["dropoffPoint"] as? String{
            self.lblDestinationAdd.text = address
        }
        
        if let VehicleNumber = dataDict!["VehicleNumber"] as? String{
            self.lblNumber.text = VehicleNumber
        }
        if let bookType = dataDict!["BookingType"] as? String{
            self.lblRideType.text = String(bookType)+", NO:"
        }
        if let TotalFare = dataDict!["TotalFare"] as? Int{
            self.lblPrice.text = "Rs " + String(TotalFare)//Constants.RUPEE + String(TotalFare)
        }
        if let BookingId = dataDict!["BookingID"] as? Int{
            self.lblBookingID.text = "Booking Id: \(BookingId), Payment is: \(String(describing: dataDict!["PaymentMethod"] as! String))"

        }
        if let DriverImage = dataDict!["DriverImage"] as? String{
            Server.toGetImageFromURL(url: Constants.BASEURLImage + DriverImage , completionHandler: { (image) in
                if image != nil{
                    self.imgView.image = image!
                }
            })
        }
    }
    
    
    func toDecideRideStatus(status:DecideRideStaus){
        switch status {
        case .Cancelled:
            imgViewFree.isHidden = true
            imgViewCancelled.isHidden = false
            imgView.isHidden = true
            viewBlur.isHidden = true
            lblPrice.isHidden = true
        case .Free:
            imgViewFree.isHidden = false
            imgViewCancelled.isHidden = true
            viewBlur.isHidden = true
            lblPrice.isHidden = true
            imgView.isHidden = false
        case .Normal:
            imgViewFree.isHidden = true
            imgView.isHidden = false
            imgViewCancelled.isHidden = true
            viewBlur.isHidden = true
            lblPrice.isHidden = false
        }
    }
    
}
