
//
//  RouteMapVC.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 30/11/17.
//  Copyright © 2017 Hitesh Dhawan. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GoogleMaps



class RouteMapVC: UIViewController,CLLocationManagerDelegate, GMSMapViewDelegate {

    func bookingcancelledByDriver() {
        self.navigationController?.popViewController(animated: true)
    }

  
    @IBOutlet weak var mapView:GMSMapView!
    @IBOutlet weak var viewMap:UIView!
    @IBOutlet var lbl_DriverName :UILabel!
    @IBOutlet var lbl_registrationNo :UILabel!
    @IBOutlet var viewForDriverDetail :UIView!
    @IBOutlet var viewBottomForMain :UIView!
    @IBOutlet var viewForSeatSelection :UIView!
    @IBOutlet var viewForAnimtionSmarte :UIView!
    @IBOutlet var viewForseats :UIView!
    @IBOutlet var scrollView :UIScrollView!
    @IBOutlet weak var btnSeat1:BorderButton!
    @IBOutlet weak var btnSeat2:BorderButton!
    @IBOutlet weak var btnSeat3:BorderButton!
   // @IBOutlet weak var btnSeat4:BorderButton!
    @IBOutlet var lblForDashLine:UILabel!
    @IBOutlet weak var btnBack :UIButton!
     @IBOutlet weak var btnconfirmBooking :UIButton!
    @IBOutlet weak var viewHeader:UIView!
    @IBOutlet weak var viewHeadermain:UIView!
    @IBOutlet weak var btn_pickupaddressHeight: NSLayoutConstraint!
    @IBOutlet weak var view_HeaderHeight: NSLayoutConstraint!
    @IBOutlet weak var view_bottomHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView_Height: NSLayoutConstraint!
    @IBOutlet weak var viewSeats_Height: NSLayoutConstraint!
    @IBOutlet weak var viewSeatselection_Height: NSLayoutConstraint!
    @IBOutlet weak var viewcontainer_Height: NSLayoutConstraint!
    var btn_pickupaddress :UIButton!
    var btn_destaddress :UIButton!
    var str_message = ""
    var view_address = UIView()
    var str_distance = ""
    var str_duration = ""
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var origin = ""
    var destaddress = ""
    var originId = ""
    var destaddressId = ""
    var marker_ImageType = ""
    var screenShotImage:UIImage?
    var dict_initializeRide = [String:Any]()
    var str_rideType = ""
    var drivername = ""

    var destinationLocation1 = CLLocationCoordinate2D()
    var source = CLLocationCoordinate2D()
    var destinationMarker = GMSMarker()
    var DriverMarker = GMSMarker()
    var Driverlocation = CLLocationCoordinate2D()
    var str_DriverMarkerStatus = ""
    var numberOfSeat = 1
    var arrBtn : [BorderButton] = [BorderButton]()
    var StartCordinates = CLLocationCoordinate2D()
    var EndCordinates = CLLocationCoordinate2D()

    var booking_Ride_Dict = [String:AnyObject]()
    
    weak var delegateUpdateData:toUpdatedataViaNotification?
    var bookingRideDetails:BookingRideDetails?

    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gmsCoordinateBounds = GMSCoordinateBounds(coordinate: (bookingRideDetails?.Source.coordinate)!, coordinate: (bookingRideDetails?.Destination.coordinate)!)
        mapView.animate(with: .fit(gmsCoordinateBounds, with: UIEdgeInsets.init(top: 205, left: 40, bottom: 150, right: 40)))
        mapView.delegate = self
       // mapView.isMyLocationEnabled = true
      //  mapView.settings.myLocationButton = true
       // mapView.settings.compassButton = true
      //  mapView.isIndoorEnabled = true
        toSetIntialDataWithPin()
        toSetTopViewsForAddresses()
        btnSeat1.backgroundColor = UIColor.init(red: 29/255, green: 165/255, blue: 28/255, alpha: 1)
        arrBtn = [btnSeat1,btnSeat2,btnSeat3]

    }
 
    override func viewWillAppear(_ animated: Bool) {

        if str_rideType == "2" {
            scrollView.contentSize = CGSize.init(width: SCREEN_WIDTH, height: 340)
            self.viewForseats.isHidden = false
            self.scrollView_Height.constant = 170.0
            self.viewSeatselection_Height.constant = 170
            self.viewSeats_Height.constant = 120.0
            //self.viewcontainer_Height.constant = 340.0
            DrawPolylineWithLatLong()


        }
        else
        {
            scrollView.contentSize = CGSize.init(width: SCREEN_WIDTH, height: 220)
            self.scrollView_Height.constant = 50.0
            self.viewSeatselection_Height.constant = 50.0
            self.viewSeats_Height.constant = 0.0
            //self.viewForseats.removeFromSuperview()
            //self.viewcontainer_Height.constant = 220.0
            self.btnconfirmBooking .setTitle("Confirm Booking", for: .normal)
            self.viewForseats.isHidden = true
            getPolylineRoute(from: (bookingRideDetails?.Source.coordinate)!, to: (bookingRideDetails?.Destination.coordinate)!)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.bringSubview(toFront: viewHeadermain)
        Helper.ToHideIndicator()

        toTakeDecisionWheatherAleradyBookedOrNot()
        //delegateUpdateData = self

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func bookingCancelledByDriver()
    {
        PKSAlertController.alert(Constants.appName, message: str_message, buttons: ["OK"]) { (alertAction, index) in
            print("index === \(index)")
            if index == 0 {
                self.navigationController?.popViewController(animated: true)
            }
        }

    }
    
    //MARK:- button actions
    
    
    @IBAction func btnCallDriverClicked(sender:AnyObject){
        let number = bookingRideDetails?.DriverMobileNumber
        if let url = URL(string: "tel://\(number!)"), UIApplication.shared.canOpenURL(url) {
            self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 170), animated: true)
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }

    
    @IBAction func btnSeatSelectionClicked(sender:AnyObject){
        let button = sender as! UIButton
        numberOfSeat = button.tag - 99
        for index in stride(from: button.tag, to: 99, by: -1){
            let btn = arrBtn[index-100]
            btn.backgroundColor = UIColor.init(red: 29/255, green: 165/255, blue: 28/255, alpha: 1)
        }
        if button.tag == 102{
            return
        }
        for tag in (button.tag+1)...102{
                let btn = arrBtn[tag-100]
                btn.backgroundColor = UIColor.init(red: 226/255, green: 0, blue: 55/255, alpha: 1)
            }
    }
    
    @IBAction func btnBookRideClicked(sender:UIButton){
        if numberOfSeat == 0 {
            return
        }
       // self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 170), animated: true)
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if str_rideType == "1" {
            numberOfSeat = 4
        }
        let datetime = dateFormatter.string(from: date)
        let urlString  =  Constants.BASEURL+"NewBookingRequestforRide"
        let userID = UserDefaults.standard.value(forKey: Constants.USERID)
        print(userID!)
        let paramdict : [String : Any] = [ "BookingLogID": self.bookingRideDetails!.bookingLogID as Any,
                                           //"BookingType":2,
                                           "noofSeat": numberOfSeat,
                                           "UserID": userID! as Any,
                                           "RouteID": self.bookingRideDetails!.RouteID as Any,
                                           "CouponCode": "",
                                           "CouponAmt": 0,
                                           "PaymentMethod": 0,
                                           "BookingTime": datetime,
                                           "TravelDistance": "0.0"/*str_distance*/]

        print(paramdict)
         if Helper.isConnectedToNetwork(){
            Helper.ToShowIndicator()
        Server.PostDataInDictionary(urlString, dict_data: paramdict as Dictionary<String, AnyObject>, completionHandler: { (response) in
            if (response != nil) {
                print(response!)
                self.str_DriverMarkerStatus = "1"
                if response?["MessageCode"] as! String == "BKR01"{

                    let dict = response?["Data"] as! [String:AnyObject]
                    print(dict)
                    self.booking_Ride_Dict = dict
//                  
                    DispatchQueue.main.async {
                        UserDefaults.standard.set("1", forKey: Constants.RideStatus)
                        UserDefaults.standard.synchronize()
                        self.btnBack.isHidden = false
                        UIView.animate(withDuration: 0.5, animations: {  //
                            self.bookingRideDetails?.bookingID = (dict["BookingID"] as! Int)
                            self.bookingRideDetails?.DriverName = dict["DriverName"] as! String //+  " - SMARTE".uppercased()
                            self.bookingRideDetails?.VechicleNumber = (dict["VehicleRegistrationNo"] as! String)
                            self.bookingRideDetails?.DriverID = (dict["DriverID"] as! Int)
                            self.bookingRideDetails?.DriverMobileNumber = (dict["DriverMobileNumber"] as! String)
                            self.scrollView_Height.constant = 170.0
                            self.viewSeats_Height.constant = 120.0
                            self.viewSeatselection_Height.constant = 170

                            //self.viewcontainer_Height.constant = 340.0
                            self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 170), animated: true)
                            self.drivername = (dict["DriverName"] as? String)!
                            self.lbl_DriverName.text = self.drivername+" on his way ETA : "+(dict["EstimateTime"]as? String)!+" min" //+  " - SMARTE".uppercased()
                            self.lbl_registrationNo.text = (dict["VehicleRegistrationNo"] as! String)
                            self.btnBack.isHidden = true
                            var location = CLLocationCoordinate2D()

                            location.latitude = Double(dict["Latitude"] as! String)!
                            location.longitude = Double(dict["Longitude"] as! String)!
                            self.showAllNerestSmartEOnMapView(location:location)

                        })
                        Helper.ToHideIndicator()
                    }
                }
                else if(response?["MessageCode"] as! String == "BKR12")
                {
//                    PKSAlertController.alert(Constants.appName, message: "Are you sure you want to cancel this ride?", buttons: ["Cancel","OK"]) { (alertAction, index) in
//                        print("index === \(index)")
//                        if index == 1  {
                      Helper.ToHideIndicator()
                    DispatchQueue.main.async {
                            let dict = response?["Data"] as! [String:AnyObject]
                            print(dict)
                    Helper.ToHideIndicator()
                         let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                            destViewController.amountToPay = String(describing: dict["amt"]!)
                            destViewController.BookingID = String(describing: dict["BookingID"]!)
                            destViewController.transactionID = String(describing: dict["TransactionID"]!)
                            self.bookingRideDetails?.bookingID = (dict["BookingID"] as! Int)

                          //  self.bookingRideDetails?.Fare = (dict["amt"] as! String)
                        destViewController.comingFromPayment = true
                        self.present(destViewController, animated: true, completion: nil)
                    }
//                        }
//                    }
}
                else
                {
                     Helper.ToHideIndicator()
                    DispatchQueue.main.async {

                       // PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                        PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "ServerError":(response?["message"] as! String), buttons: ["OK"]) { (alertAction, index) in
                            print("index === \(index)")
                            if index == 0 {
                                // self.dismiss(animated: true, completion: nil)
                                self.togetbacktoDashboard()
                            }
                        }


                        UIView.animate(withDuration: 0.5, animations: {
                            self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
                        })

                    }
                }
            }else{
                print("errror",response as Any)
                
                Helper.ToHideIndicator()
//                PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)

                PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "ServerError":(response?["paymentState"] as! String), buttons: ["OK"]) { (alertAction, index) in
                    print("index === \(index)")
                    if index == 0 {
                       // self.dismiss(animated: true, completion: nil)
                        self.togetbacktoDashboard()
                    }
                }
            }
        })
        }
        else{
            PKSAlertController.alertForNetwok()

        }

}

    func togetbacktoDashboard()
    {
        for vc in (self.navigationController?.viewControllers)!{
            if vc is HomeDashBoardVC{
                DispatchQueue.main.async {
                    bookingCompletionDelegate.updateResultAfterBookingCompletion()
                    Helper.saveDataInNsDefault(object: [:] as AnyObject, key: Constants.bookingRideDetails)
                    UserDefaults.standard.set("2", forKey: Constants.RideStatus)
                    _ = self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
    @IBAction func BackButtonPressed(sender:UIButton){
        if scrollView.contentOffset.y > 330{
            CancelRide()
        }else{
            bookingCompletionDelegate.updateResultAfterBookingCompletion()
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    @IBAction func btnStartRide(sender:UIButton){
        startRide()
    }

    @IBAction func btnCancelRide(sender:UIButton){
        CancelRide()
    }
    
    
    //MARK:- extraa functions
    
    func toCheckStatusForPayment(){
        let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        destViewController.amountToPay = "You have to pay \(Constants.RUPEE)10 to book new ride."
        destViewController.comingFromPayment = true
        self.present(destViewController, animated: true, completion: nil)
       // self.navigationController?.pushViewController(destViewController, animated: true)
    }
   // MARK:-  draw polyline with static lat long for shared smartE
    func DrawPolylineWithLatLong(){
        
        let gmsPath = GMSMutablePath()
        for point in bookingRideDetails!.routeArr {
            gmsPath.addLatitude(point.coordinate.latitude, longitude: point.coordinate.longitude)
        }
        
        let polyline1 = GMSPolyline(path: gmsPath)
        polyline1.strokeWidth = 5.0
        polyline1.strokeColor = Colors.appThemeColorText
        polyline1.geodesic = true
        UIView.animate(withDuration: 1.0) {
            polyline1.map = self.mapView
        }
        
        let marker = GMSMarker.init(position: (bookingRideDetails?.Source.coordinate)!)
        marker.icon = #imageLiteral(resourceName: "marker")
        marker.map = self.mapView
        
        
        DispatchQueue.main.async {
            let gmsCoordinateBounds = GMSCoordinateBounds(coordinate:  (self.bookingRideDetails!.routeArr.first?.coordinate)!, coordinate: (self.bookingRideDetails!.routeArr.last?.coordinate)!)
            self.mapView.animate(with: .fit(gmsCoordinateBounds, with: UIEdgeInsets.init(top: 160, left: 50, bottom: 90, right: 50)))
        }
        
    }
    
    func toTakeDecisionWheatherAleradyBookedOrNot(){

        if bookingRideDetails?.RideStatus == 1{
            DispatchQueue.main.async {

                self.str_DriverMarkerStatus = "1"

                let lat = UserDefaults.standard.value(forKey: "lat") as! String
                let lng = UserDefaults.standard.value(forKey: "lng") as! String
                print(lat )
                print(lng )
                self.Driverlocation.latitude = Double(lat)!
                self.Driverlocation.longitude = Double(lng)!
                self.drivername = (self.bookingRideDetails?.DriverName)!
//                self.Driverlocation.latitude = UserDefaults.value(forKey: "lat") as! Double
//                self.Driverlocation.longitude = UserDefaults.value(forKey: "lng") as! Double
            self.showAllNerestSmartEOnMapView(location: self.Driverlocation)
            self.scrollView_Height.constant = 170.0
            self.viewSeats_Height.constant = 120.0
            self.viewSeatselection_Height.constant = 170
            //self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 170), animated: true)

            //self.lbl_DriverName.text = (self.bookingRideDetails?.DriverName)!+" on his way ETA : "+"1"+" min"

            self.lbl_registrationNo.text = self.bookingRideDetails?.VechicleNumber
            self.scrollView.setContentOffset(CGPoint.init(x: 0, y: 170), animated: true)
            self.btnBack.isHidden = true
            }
        }
    }
    
    func CancelRide(){
        PKSAlertController.alert(Constants.appName, message: "Are you sure you want to cancel your ride?", buttons: ["No","Yes"]) { (alertAction, index) in
            print("index === \(index)")
            if index == 1 {
                Helper.ToShowIndicator()
                let dict_param = [
                    "UserID": UserDefaults.standard.value(forKey: Constants.USERID),
                    "BookingID": self.bookingRideDetails?.bookingID
                ]
                let url_str = Constants.BASEURL + "CancelBookingRequestForRide"
                Server.PostDataInDictionary(url_str, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
                    print(response as Any)
                    Helper.ToHideIndicator()
                    if (response != nil) {
                        print(response!)
                        if response?["MessageCode"] as! String == "BKR02"{
                            print(response!)
                            bookingCompletionDelegate.updateResultAfterBookingCompletion()
                            DispatchQueue.main.async {
                                self.btnBack.isHidden = false
                                _ = self.navigationController?.popViewController(animated: true)
                            }
                        }else{
                            PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                        }
                    }else{
                        print("errror")
                    }
                })
            }
        }
    }
    
    func startRide(){
        PKSAlertController.alert(Constants.appName, message: "Are you sure you want to Start your ride?", buttons: ["No","Yes"]) { (alertAction, index) in
            print("index === \(index)")
            if index == 1 {
                Helper.ToShowIndicator()
                let dict_param = [
                    "UserID": UserDefaults.standard.value(forKey: Constants.USERID),
                    "BookingID": self.bookingRideDetails?.bookingID,
                    "DeviceID": Constants.Device_UUID ,
                    "DeviceToken":UserDefaults.standard.value(forKey: Constants.DevToken),
                    "DeviceName" : "IOS"
                ]

                let url_str = Constants.BASEURL + "PassengerBookingConfirmRequest"

                Server.PostDataInDictionary(url_str, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
                    print(response as Any)
                    Helper.ToHideIndicator()
                    if (response != nil && (response?.keys.contains("MessageCode"))!) {
                        if response?["MessageCode"] as! String == "BKR06"{
                            print(response!)
                            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "RideStartViewController") as! RideStartViewController
                            DispatchQueue.main.async {
                                destViewController.bookingRideDetails  = self.bookingRideDetails
                                UserDefaults.standard.set("3", forKey: Constants.RideStatus)
                                self.navigationController?.pushViewController(destViewController, animated: true)
                            }
                        }else{
                            PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
                            //PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                        }
                    }else{
                        print("errror")
                    }

                })

            }
        }
    }

    func toSetIntialDataWithPin(){
        do {
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                //self.getPolylineRoute(from: (bookingRideDetails?.Source.coordinate)!, to: (bookingRideDetails?.Destination.coordinate)!)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("One or more of the map styles failed to load. \(error)")
        }

    }

    // MARK:- show nearby smarte on map
    func showAllNerestSmartEOnMapView(location:CLLocationCoordinate2D?){
        if location == nil {
            return
        }
        let urlString  =  Constants.BASEURL + "FindVehicleAlongWithLocation"
        let paramdict  = [
            "Latitude": "\(location!.latitude)",
            "Longitude": "\(location!.longitude)",
            "DriverID": self.bookingRideDetails?.DriverID,
            "UserID": UserDefaults.standard.value(forKey: Constants.USERID)
        ]

//        let ridestatuss = UserDefaults.value(forKey: Constants.RideStatus) as! String
//        if ridestatuss == "1"
//        {
        if Helper.isConnectedToNetwork(){
            Server.PostDataInDictionary(urlString, dict_data: paramdict as Dictionary<String, AnyObject>, completionHandler: {(response) in
                print(response as Any)
                if response != nil && response?["MessageCode"] as! String == "DRTF04"{
                    let dic_data = response?["Data"] as? [String:AnyObject]
                    let arrLatLong = dic_data?["VehicleResponses"] as? [[String:AnyObject]]
                    if (arrLatLong?.count)! > 0
                    {
                    let dic_routes = arrLatLong?[0] as! Dictionary<String,AnyObject>
                    print(dic_routes)
                    DispatchQueue.main.async {
                        let lat =  dic_routes["CurrentLatitude"] as! String
                        let lng =  dic_routes["CurrentLongitude"] as! String

                        let lat1 =  dic_routes["PreviousLatitude"] as! String
                        let lng1 =  dic_routes["PreviousLongitude"] as! String

                        self.Driverlocation.latitude = Double(lat)!
                        self.Driverlocation.longitude = Double(lng)!
                        UserDefaults.standard.set(lat, forKey: "lat")
                        UserDefaults.standard.set(lng, forKey: "lng")
                        self.lbl_DriverName.text = self.drivername+" on his way ETA : "+(dic_routes["EstimateTime"]as? String)!+" min"
                        //UserDefaults.standard.set(self.Driverlocation, forKey: "Driverlocation")
                        //UserDefaults.standard.synchronize()
                        if self.str_DriverMarkerStatus == "1"
                        {
                            self.DriverMarker.position = CLLocationCoordinate2D(latitude: (location?.latitude)!, longitude: (location?.longitude)!)
                            self.DriverMarker.title = "pickup point"
                            self.DriverMarker.icon = #imageLiteral(resourceName: "DriverMarker")
                            self.DriverMarker.map = self.mapView
                            self.str_DriverMarkerStatus = "0"

                            print(self.DriverMarker.position)
                        }
                        else
                        {

                            self.DriverMarker.position = CLLocationCoordinate2D(latitude: (location?.latitude)!, longitude: (location?.longitude)!)

                            let oldCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(CDouble(lat1)!, CDouble(lng1)!)

                            let newCoodinate: CLLocationCoordinate2D? = CLLocationCoordinate2DMake(CDouble(lat)!, CDouble(lng)!)
                            self.DriverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
                            self.DriverMarker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: oldCoodinate!, toCoordinate: newCoodinate!))
                            //found bearing value by calculation when marker add
                            self.DriverMarker.position = oldCoodinate!
                            //this can be old position to make car movement to new position
                            self.DriverMarker.map = self.mapView
                            //marker movement animation
                            CATransaction.begin()
                            CATransaction.setValue(Int(2.0), forKey: kCATransactionAnimationDuration)
                            CATransaction.setCompletionBlock({() -> Void in
                            self.DriverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
                            //self.DriverMarker.rotation = CDouble(data.value(forKey: "bearing"))
                            //New bearing value from backend after car movement is done
                            })
                            self.DriverMarker.position = newCoodinate!
                            //this can be new position after car moved from old position to new position with animation
                            self.DriverMarker.map = self.mapView
                            self.DriverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
                            self.DriverMarker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: oldCoodinate!, toCoordinate: newCoodinate!))
                            self.mapView.camera = GMSCameraPosition(target: newCoodinate!, zoom: 15.0, bearing: self.DriverMarker.rotation, viewingAngle: 0.0)
                            //found bearing value by calculation
                            
                            CATransaction.commit()

                        }
                        if UserDefaults.standard.value(forKey: Constants.RideStatus) as! String == "1"
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                self.showAllNerestSmartEOnMapView(location: self.Driverlocation)
                            })

                        }

                        }
                    }
                    else
                    {
                        if UserDefaults.standard.value(forKey: Constants.RideStatus) as! String == "1"
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                                self.showAllNerestSmartEOnMapView(location: self.Driverlocation)
                            })

                        }

                    }
                    }
                else{
                    print("errror",response as Any)
                    if UserDefaults.standard.value(forKey: Constants.RideStatus) as! String == "1"
                    {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                            self.showAllNerestSmartEOnMapView(location: self.Driverlocation)
                        })

                    }
                }
            })
        }
        //}
    }

    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {

        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    /*Direction Api & Create polyline Route*/
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        if Helper.isConnectedToNetwork(){
         
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            
          // let str = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&mode=driving&key=\(KEYS.GoogleKey)"
            let str = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&mode=driving&key=\(KEYS.GoogleKey)"
            
            let url = URL.init(string: str)
            print(url!)
            let task = session.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                PKSAlertController.alert(Constants.appName, message:(error?.localizedDescription)!)

            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        let routes = json["routes"] as? [Any]
                       // print(routes!)
                        if (routes?.count)! > 0
                        {
                        let routes1 = routes?[0] as? Dictionary<String , Any>
                        let legs = routes1?["legs"]

                        let personDictionary = (legs as! NSArray).mutableCopy() as! NSMutableArray
                        let legsarray = personDictionary[0] as? Dictionary<String,Any>
                            
                            let cordinates = legsarray?["end_location"] as? Dictionary<String,Any>
                            let cordinates1 = legsarray?["start_location"] as? Dictionary<String,Any>
                            DispatchQueue.main.async {
                                for i in 0..<2
                                {

                                    self.destinationMarker = GMSMarker()
                                    self.self.StartCordinates.latitude = cordinates1!["lat"] as! Double
                                    self.StartCordinates.longitude = cordinates1!["lng"] as! Double

                                    self.EndCordinates.latitude = cordinates!["lat"] as! Double
                                    self.EndCordinates.longitude = cordinates!["lng"] as! Double
                                    var image = UIImage()
                                    if i == 0{
                                        self.destinationMarker.position = CLLocationCoordinate2D(latitude: cordinates1!["lat"] as! Double, longitude: cordinates1!["lng"] as! Double)
                                        self.destinationMarker.title = "pickup point"
                                        // state_marker.snippet = "Hey, this is \(state.name)"
                                        image = UIImage(named:"pin.png")!

                                        //state_marker.icon = image //homevx.imageWithImage(image: image, scaledToSize: CGSize(width: 20.0, height: 20.0))
                                        self.destinationMarker.icon = GMSMarker.markerImage(with: UIColor.green)
                                        self.destinationMarker.map = self.mapView
                                        print(self.destinationMarker.position)
                                    }
                                    else{
                                        self.destinationMarker.position = CLLocationCoordinate2D(latitude: cordinates!["lat"] as! Double, longitude: cordinates!["lng"] as! Double)
                                        self.destinationMarker.title = "Drop point"
                                        print(self.destinationMarker.position )
                                        self.destinationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
                                        self.destinationMarker.map = self.mapView
                                        // state_marker.snippet = "Hey, this is \(state.name)"
                                    }
                                }
                            }
                            let distance = legsarray?["distance"] as! Dictionary<String,Any>
                            let duration = legsarray?["duration"] as! Dictionary<String,Any>
                            let distance1 = (distance["text"] as? String)!
                            self.str_duration = (duration["text"] as? String)!
                            let arr = distance1.components(separatedBy: " ")
                            self.str_distance = arr[0]
                            print(arr,self.str_distance)
                            
                        let overview_polyline = routes1?["overview_polyline"] as? Dictionary<String,Any>
                        let polyString = overview_polyline?["points"] as? String
                        
                        //draw path on map
                            DispatchQueue.main.async {
                                self.showPath(polyStr: polyString!)
                                let gmsCoordinateBounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                                //mapView.animate(with: .fit(gmsCoordinateBounds, withPadding: 30))
                                self.mapView.animate(with: .fit(gmsCoordinateBounds, with: UIEdgeInsets.init(top: 160, left: 50, bottom: 90, right: 50)))
                            }
                        }
                        else{
                            PKSAlertController.alert(Constants.appName, message:"Please Select A valid Destination")
                        }
                    }
                }catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
        }
        else{
            PKSAlertController.alertForNetwok()
        }
    }
    
    func toAddDashLine(){
        let layer = CALayer()
        lblForDashLine.text = ""
        let frame = lblForDashLine.frame
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.init(red: 24/255, green: 176/255, blue: 37/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 5
        shapeLayer.lineDashPattern = [2,3]
        
        let path = CGMutablePath()
        let y = CGFloat(0)
        path.addLines(between: [CGPoint(x: 0, y: y),CGPoint(x: frame.size.width, y: y)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
        lblForDashLine.layer.addSublayer(layer)
    }
    
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    
    func showPath(polyStr :String){ //draw polyline with lat long for solo smartE
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        self.path = path!
        self.polyline = polyline

        polyline.strokeWidth = 5.0
        polyline.strokeColor = Colors.appThemeColorText
        polyline.map = self.mapView  // Your map view
        
       // self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)

    }
    
    func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 5.0
            self.animationPolyline.map = self.mapView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    
    
    func toTakeScreenShot(){
        UIGraphicsBeginImageContextWithOptions(mapView.bounds.size, true, 0)
        mapView.drawHierarchy(in:mapView.bounds, afterScreenUpdates: true)
        let mapSnapShot: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        // Put snapshot image into an UIImageView and overlay on top of map.
        print(mapSnapShot!.description)
        screenShotImage = mapSnapShot
    }
    
    
    func toSetTopViewsForAddresses(){

        view_address.frame = CGRect(x: 10, y: 65, width: SCREEN_WIDTH-20, height:110)
        view_address.backgroundColor = UIColor.white

        btn_pickupaddressHeight = NSLayoutConstraint(item: view_address, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.greaterThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 110)
        view_address.addConstraint(btn_pickupaddressHeight)

        btn_pickupaddress = UIButton(frame: CGRect( x:20 , y:5, width:view_address.frame.width-35 ,height : 35))
        btn_pickupaddress.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 15, bottom: 0, right: 0)
        //btn_pickupaddress.setImage(#imageLiteral(resourceName: "circle (1).png"), for:UIControlState.normal)

        btn_pickupaddress.setTitleColor(Colors.appThemeColorTextgray, for: UIControlState.normal)
        btn_pickupaddress.tag = 1
        btn_pickupaddress.titleLabel?.font = UIFont(name: FONTS.Roboto_Regular, size: 17.0)
        btn_pickupaddress.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btn_pickupaddress.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btn_pickupaddress.setTitle(bookingRideDetails?.SourceAdd, for: .normal)
        btn_pickupaddress.backgroundColor = UIColor.white

       // btn_pickupaddress.addTarget(self, action: #selector(autocompleteClicked(_:)), for: .touchUpInside)
        btn_pickupaddress.titleEdgeInsets.left = 10.0
        self.btn_pickupaddress.titleLabel?.lineBreakMode = .byTruncatingTail
        view_address.addSubview(btn_pickupaddress)



        btn_destaddress =  UIButton.init(type: UIButtonType.custom)
        btn_destaddress.frame = CGRect( x:20 , y:61, width:view_address.frame.width-35 ,height : 40.0)
        //        btn_destaddress = UIButton(frame: CGRect( x:20 , y:61, width:view_address.frame.width-35.0 ,height : 35.0))
        //btn_destaddress.buttonType = UIButtonType.custom
        btn_destaddress.titleEdgeInsets.left = 10.0
        btn_destaddress.setTitleColor(Colors.appThemeColorTextgray, for: UIControlState.normal)
        btn_destaddress.titleLabel?.font = UIFont(name: FONTS.Roboto_Regular, size: 17.0)
        btn_destaddress.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        btn_destaddress.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btn_destaddress.backgroundColor = UIColor.white
        self.btn_destaddress.titleLabel?.lineBreakMode = .byTruncatingTail
        btn_destaddress.setTitle(bookingRideDetails?.DestinationAdd, for: .normal)
        btn_destaddress.tag = 2
        //btn_destaddress.addTarget(self, action: #selector(autocompleteClicked(_:)), for: .touchUpInside)
        view_address.addSubview(btn_destaddress)

        let img = UIImageView()
        img.frame = CGRect(x: 15, y: btn_pickupaddress.frame.origin.y+btn_pickupaddress.frame.height/2-4, width: 9, height: 9)
        //img.image = UIImage.init(named: "circle (1).png")
        img.backgroundColor = Colors.appThemeColorText
        img.layer.cornerRadius = 4.5
        view_address.addSubview(img)

        let img1 = UIImageView()
        img1.frame = CGRect(x: 15, y: btn_destaddress.frame.origin.y+btn_destaddress.frame.height/2-4, width: 9, height: 9)
        img1.layer.borderColor = Colors.appThemeColorText.cgColor
        img1.layer.borderWidth = 2.0
        img1.layer.cornerRadius = 4.5
        img1.backgroundColor = .white
        
        view_address.addSubview(img1)
        let lbl = UILabel()
        let height = img.frame.origin.y+img.frame.height

        lbl.frame = CGRect(x: img.frame.origin.x+4, y: height, width: 1, height: img1.frame.origin.y-3-height)
        lbl.backgroundColor = UIColor.green
        view_address.addSubview(lbl)
        Helper.giveShadowToView(view_address)
        viewHeadermain.addSubview(view_address)
    }


}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
