//
//  MyProfileVCVC.swift
//  Smart-E
//
//  Created by Hitesh Dhawan on 10/01/18.
//  Copyright © 2018 Hitesh Dhawan. All rights reserved.
//

import UIKit
import AVFoundation
import SDWebImage


protocol toUpdateProfile:class {
    func SetData()
}

weak var delegateProfile:toUpdateProfile?

class MyProfileVC: UIViewController {
    @IBOutlet  var txtName: UITextField!
    @IBOutlet  var txtMobile: UITextField!
    @IBOutlet  var txtEmail: UITextField!
    @IBOutlet  var btn_update: UIButton!
    @IBOutlet  var btnProfilePic: UIButton!
    @IBOutlet  var activityView: UIActivityIndicatorView!
    
    
    var str_btn_txt = ""
    var dict_Response = [String:String]()
    var dict_profile = [String:String]()
    var picker:UIImagePickerController?
    var imageBase64Str = ""
    var userProfileImage:UIImage?
    
    
    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtMobile.isUserInteractionEnabled = false
        txtMobile.isEnabled = false
        toMakeDecisionEnalbeDisable(value: false)
        setDataForProfile()
        toGetProfileData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK:- button action
    
    @IBAction func btnPictureClicked(sender:UIButton){
        self.view.endEditing(true)
        if(picker == nil){
            picker = UIImagePickerController()
        }
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.toCheckAuthrization()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        picker!.delegate = self
        picker!.allowsEditing = true
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone{
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    @IBAction func BackButtonPressed(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnDoneClicked(sender:UIButton){
        if Helper.isConnectedToNetwork() {
        str_btn_txt = (btn_update.titleLabel?.text)!
        if str_btn_txt == "EDIT PROFILE"
        {
            toMakeDecisionEnalbeDisable(value: true)
            txtName.becomeFirstResponder()
            str_btn_txt = "UPDATE PROFILE"
            btn_update.setTitle(str_btn_txt, for: .normal)
            
        }
        else{
            if !ValidationData() {
                return
            }
            Helper.ToShowIndicator()
            var dict_param:[String:Any] = [
                "UserID": UserDefaults.standard.value(forKey: Constants.USERID)!,
                "Name": txtName.text!,
                "Email": txtEmail.text!,
                "mobileNumber": txtMobile.text!,
                "Device_ID": Constants.Device_UUID! ,
                "DeviceToken": "string",
                "IPAddress": "IP Address",
                "Device_Name" : "IOS"
            ]
            if userProfileImage != nil{
                dict_param["ImageStr"] = self.convertImageToBase64(image: userProfileImage!)
            }else{
                dict_param["ImageStr"] = ""
                
            }
            print(dict_param)
            
            self.str_btn_txt = "EDIT PROFILE"
            // self.btn_update.setTitle(self.str_btn_txt, for: .normal)
            
            let url_str = Constants.BASEURL + "UpdatePassengerProfile"
            Server.PostDataInDictionary(url_str, dict_data: dict_param as Dictionary<String, AnyObject>, completionHandler: { (response) in
                Helper.ToHideIndicator()
                // print(response!)
                if (response != nil) && response?["MessageCode"] as! String == "Act22"{
                    print(response!)
                    print(response!)
                    let dictData = response?["Data"] as! [String:AnyObject]
                    self.dict_Response = ["MobileNumber" : dictData["MobileNumber"] as! String]
                    self.str_btn_txt = "EDIT PROFILE"
                    let dict_profile = [
                        "Email": dictData["EmailID"] as! String,
                        "Name": dictData["Name"] as! String ,
                        "MobileNumber": dictData["MobileNumber"] as! String,
                        "ImageURL": dictData["ImagePath"] as? String ?? ""
                    ]
                    Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)
                    UserDefaults.standard.synchronize()
                    DispatchQueue.main.async {
                        self.toMakeDecisionEnalbeDisable(value: false)
                        delegateProfile?.SetData()
                        self.btn_update.setTitle(self.str_btn_txt, for: .normal)
                    }
                    self.toShowAlert(message: response?["message"] as! String)
                    
                }else{
                    self.toShowAlert(message: "Server Error")
                }
                
            })
        }
        }else{
            PKSAlertController.alertForNetwok()
        }
        
    }
    
    func setDataForProfile(){
        dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as! [String : String]
        txtName.text = dict_profile["Name"] ?? ""
        txtEmail.text = dict_profile["Email"] ?? ""
        txtMobile.text = dict_profile["MobileNumber"]
        if dict_profile["ImageURL"] != ""  && (dict_profile["ImageURL"]) != nil{
            Server.toGetImageFromURL(url: Constants.BASEURLImage + dict_profile["ImageURL"]!, completionHandler: { (image) in
                if image != nil{
                    DispatchQueue.main.async {
                            self.btnProfilePic.setImage(image, for: .normal)
                       }
                    }
                })
        }
    }
    
    func toGetProfileData(){
        if Helper.isConnectedToNetwork() {
        let url_str = Constants.BASEURL + "GetProfileData?Userid=" + "\(UserDefaults.standard.value(forKey: Constants.USERID)!)"
        print("url_str === \(url_str)")
        Helper.ToShowIndicator()
        Server.getRequestWithURL(urlString: url_str, completionHandler: { (response) in
             print(response!)
            Helper.ToHideIndicator()
            if (response?.count)! > 1 && response?["MessageCode"] as! String == "Act12"{
                    let data = response?["data"] as! [String:AnyObject]
                    DispatchQueue.main.async {
                        let dict_profile = [
                            "Email": data["EmailID"] as! String,
                            "Name": data["Name"] as! String ,
                            "MobileNumber": data["MobileNumber"] as! String,
                            "ImageURL": data["ImagePath"] as? String ?? ""
                        ]
                        print(dict_profile)
                        Helper.saveDataInNsDefault(object: dict_profile as AnyObject, key: Constants.PROFILE)

                        UserDefaults.standard.set(data["UserID"] as! NSNumber, forKey: Constants.USERID)
                        UserDefaults.standard.synchronize()
                        self.setDataForProfile()
                    }
                }else{
                    PKSAlertController.alert(Constants.appName, message: response?["message"] as! String)
                }
        })
        }else{
            PKSAlertController.alertForNetwok()
        }
        
        
    }
    
    
    //MARK:- extraa action
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            if UIDevice.current.userInterfaceIdiom == .phone{
                self.present(picker!, animated: true, completion: nil)
            }
        }
        else{
            openGallary()
        }
    }
    
    func toCheckAuthrization(){
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
        case .authorized:
            openCamera()
        case .notDetermined,.restricted,.denied:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    self.openCamera()
                    print("Granted access to \(cameraMediaType)")
                } else {
                    print("Denied access to \(cameraMediaType)")
                    PKSAlertController.alert(Constants.appName, message: "There is no access for photo Go to your setting and provide access .")
                }
            }
        }
    }
    
    func openGallary(){
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        if UIDevice.current.userInterfaceIdiom == .phone{
            self.present(picker!, animated: true, completion: nil)
        }
    }
    
    func toShowAlert(message:String){
        DispatchQueue.main.async {
            PKSAlertController.alert(Constants.appName, message: message)
        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = UIImagePNGRepresentation(image)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    func ValidationData()->Bool{
        
        if txtName.text?.count == 0{
            PKSAlertController.alert(Constants.appName, message: "Please enter your name.")
            return false
        }
        
        if txtMobile.text?.count == 0{
            PKSAlertController.alert(Constants.appName, message: "Please enter your mobile number.")
            return false
        }
        
        if !Helper.isValidPhoneNumber(phoneNumber: txtMobile.text!){
            PKSAlertController.alert(Constants.appName, message: "Please enter valid mobile number.")
            return false
        }
        
        if  txtEmail.text?.count != 0 && !Helper.isValidEmail(txtEmail.text!){
            PKSAlertController.alert(Constants.appName, message: "Please enter valid email.")
            return false
        }
        
        return true
    }
    
    
    
    
    func toMakeDecisionEnalbeDisable(value:Bool)//Textfield nabled or disbled
    {
        txtEmail.isEnabled = value
        txtName.isEnabled = value
        btnProfilePic.isUserInteractionEnabled = value
    }
    
    
    
}



extension MyProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage]{
            btnProfilePic.setImage(pickedImage as? UIImage, for: .normal)
            userProfileImage = UIImage.init(data: UIImageJPEGRepresentation((pickedImage as? UIImage)!, 0.20)!)
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
