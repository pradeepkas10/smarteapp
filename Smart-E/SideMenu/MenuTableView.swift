//
//  MenuTableView.swift
//  TestSwiftApp
//
//  Created by Surbhi on 24/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
import CoreData
import SafariServices
import SDWebImage
import Ligero
import GoogleSignIn


class MenuTableView: TableViewControllerSideMenu,SFSafariViewControllerDelegate,toUpdateProfile  {
    
    var menuArray = [String]()
    var selectedMenuItem : Int = 0
    var lastSelectedMenuItem : Int = 0
    var username = String()
    var containImage = Bool()
    var profileImageStr = String()
    var profileDictionary = Dictionary<String,Any>()
    var headerView:HeaderViewSide?
    
    
    //MARK:- View controller life cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReloadMenuView), name: NSNotification.Name(rawValue: Constants.pushNotificationToUpdate), object: nil)
        let loggedin:Bool = (UserDefaults.standard.object(forKey: Constants.isLoggedIn) != nil)
        print(loggedin)
        tableView.frame = UIScreen.main.bounds

        menuArray = ["HOME","PHONEPE PROFILE","YOUR RIDES","NOTIFICATION","HELP",
                    "LOGOUT"]
        self.navigationController?.isNavigationBarHidden = true
        //tableView.scrollsToTop = false
        self.view.backgroundColor = UIColor.init(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
        tableView.contentInset = UIEdgeInsets.zero
        tableView.tableHeaderView = UIView.init(frame: CGRect.zero)
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }

        // Preserve selection between presentations
        //self.clearsSelectionOnViewWillAppear = false
        tableView.selectRow(at: IndexPath(row: selectedMenuItem, section: 0), animated: false, scrollPosition: .middle)
        tableView.separatorStyle = .none
        
        //tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MenuTableViewCell")
        
        tableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        
       // tableView.style = .plain
        var width = CGFloat()
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P {
            width  = 275 //300.0
        }
        else
        {
            width = 225
        }
        headerView = HeaderViewSide(frame: CGRect.init(x: 0, y: 0, width: width+5, height: 150))
        headerView?.btnView.addTarget(self, action: #selector(viewProfileButton), for: .touchUpInside)
        delegateProfile = self

        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject

        //Then just cast the object as a String, but be careful, you may want to double check for nil
        let version = nsObject as! String
        print("Version===",version)
        let footerview = UIView()
        footerview.frame = CGRect(x: 0, y: SCREEN_HEIGHT-85, width: width, height: 85)
        footerview.backgroundColor = .clear
        //tableView.tableFooterView = footerview

        let btn_fb = UIButton()
        btn_fb.frame = CGRect(x: 20, y: 10, width: 40, height: 40)
        btn_fb.addTarget(self, action: #selector(btn_fbpressed), for: .touchUpInside)
        btn_fb.layer.cornerRadius = 20.0
        btn_fb.setImage(#imageLiteral(resourceName: "FacebookIcon"), for: .normal)
        footerview.addSubview(btn_fb)

        let btn_linkedin = UIButton()
        btn_linkedin.frame = CGRect(x: 70, y: 10, width: 40, height: 40)
        btn_linkedin.addTarget(self, action: #selector(btn_linkedinPressed), for: .touchUpInside)
        btn_linkedin.layer.cornerRadius = 15.0
        btn_linkedin.setImage(#imageLiteral(resourceName: "linkedin"), for: .normal)
        footerview.addSubview(btn_linkedin)


        let lblVersion = UILabel()
        lblVersion.frame = CGRect(x: 20, y: 50, width: width-40, height: 30)
        lblVersion.text = "Version "+version
        lblVersion.textColor = Colors.Apptextcolor
        lblVersion.font = UIFont.init(name: FONTS.Roboto_Light, size: 15.0)
        lblVersion.textAlignment = .left
        footerview.addSubview(lblVersion)
        self.view.addSubview(footerview)
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       // self.automaticallyAdjustsScrollViewInsets = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func btn_fbpressed(){
      //  UIApplication.shared.openURL(NSURL(string: "https://www.facebook.com/getSmartE/")! as URL)
        let url =  "https://www.facebook.com/getSmartE/"
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
    }

    @objc func btn_linkedinPressed(){
    //    UIApplication.shared.openURL(NSURL(string: "https://www.linkedin.com/company/treasure-vase-ventures-pvt-ltd/")! as URL)
        let url =  "https://www.linkedin.com/company/treasure-vase-ventures-pvt-ltd/"
        UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)

    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        SetDataProfile()
        return headerView
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       return 30
    }

//override func tableView
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if DeviceType.IS_IPHONE_6 || DeviceType.IS_IPHONE_6P
        {
            return 50;
        }
        return 45;
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.lblText.text = menuArray[indexPath.item]
        cell.imgView?.image = UIImage.init(named: "\(indexPath.row)")
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(selectedMenuItem , lastSelectedMenuItem)
        
        //  Payment view controller
        switch (indexPath.row) {
        case 0:
            toggleSideMenuView()
//            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
//            lastSelectedMenuItem = selectedMenuItem
//            selectedMenuItem = indexPath.row
//
//            if Constants.appDelegate.returnTopViewController().isKind(of: HomeDashBoardVC.classForCoder()){
//                let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
//                home.hideSideMenuView()
//                home.navigationController?.pushViewController(destViewController, animated: true)
//            }
//            else{
//                sideMenuController()?.setContentViewController(destViewController)
//            }

        case 2: // MY Rides
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "RideViewController") as! RideViewController
            
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row
            
            if Constants.appDelegate.returnTopViewController().isKind(of: HomeDashBoardVC.classForCoder()){
                let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
                //
                home.navigationController?.pushViewController(destViewController, animated: true)
                home.hideSideMenuView()
            }
            
            
       
        case 3: // Notification ViewController
            let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row

            if Constants.appDelegate.returnTopViewController().isKind(of: HomeDashBoardVC.classForCoder()){
                let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
                home.hideSideMenuView()
                home.navigationController?.pushViewController(destViewController, animated: true)
            }
            else{
                sideMenuController()?.setContentViewController(destViewController)
            }
            break
            //
            case 4: // Help
          let destViewController = Constants.mainStoryboard2.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            lastSelectedMenuItem = selectedMenuItem
            selectedMenuItem = indexPath.row

            if Constants.appDelegate.returnTopViewController().isKind(of: HomeDashBoardVC.classForCoder()){
               let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
               home.hideSideMenuView()
                home.navigationController?.pushViewController(destViewController, animated: true)
                        }
            else{
               sideMenuController()?.setContentViewController(destViewController)
            }
//
//
        case 1: // Settings
            let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
            let headers = ["Content-type": "application/json"]
            toGetSaltCheckSumValue { (SaltCheckSum, base64, apiendPoint) in
                if SaltCheckSum != nil && base64 != nil && apiendPoint != nil{
                    self.hideSideMenuView()
                    let profileRequest = PPSTransactionRequest.init(base64EncodedBody: base64!, apiEndPoint: apiendPoint!, checksum: SaltCheckSum!, headers: headers)

                    PhonePeSDK.shared().startPhonePeTransactionRequest(profileRequest,
                                                                       on: home, animated: true) {
                                                                        (request, result) in
                                                                        //Do something with the request/result
                    }

                }
            }
//            let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
//            lastSelectedMenuItem = selectedMenuItem
//            selectedMenuItem = indexPath.row
//
//            if Constants.appDelegate.returnTopViewController().isKind(of: HomeDashBoardVC.classForCoder()){
//                let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
//                home.hideSideMenuView()
//                home.navigationController?.pushViewController(destViewController, animated: true)
//            }
//            else{
//                sideMenuController()?.setContentViewController(destViewController)
//            }
            break
        case 5:
            btnlogoutPressed()
            
        default:
            break
            
        }
       
    }

//    @IBAction func didTapSignOut(_ sender: AnyObject) {
//        GIDSignIn.sharedInstance().signOut()
//    }

    //MARK:- Extraa funtions
    func  UpdateMenuView(){
    }


    func toGetSaltCheckSumValue(completionHandler:@escaping (_ saltValue:String?,_ base64Str:String?, _ apiendPoint:String?)->Void){
        if Helper.isConnectedToNetwork(){
            Helper.ToShowIndicator()
            let dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as! [String : String]
            var dict = [String:Any]()
//            dict["merchantId"] = "M2306160483220675579156"
//            dict["transactionId"] = "1234567892580"
//            dict["merchantUserId"] = "60"
//            dict["mobileNumber"] = "8802189030"
//            dict["email"] = "ankit.sharma776@gmail.com"
//            dict["shortName"] = "ankit"
            let StrUserID = String(describing: UserDefaults.standard.value(forKey: Constants.USERID) as! NSNumber)
            dict["merchantUserId"] = StrUserID
            dict["mobileNumber"] = dict_profile["MobileNumber"]
            dict["email"] = dict_profile["Email"]
            dict["shortName"] = dict_profile["Name"]

            let url_str = Constants.BASEURL + "PhonePeProfile"
            print(dict)
            Server.PostDataInDictionary(url_str, dict_data: dict as Dictionary<String, AnyObject>, completionHandler: { (response) in
                print(response!)
                Helper.ToHideIndicator()
                if (response != nil) && (response?["MessageCode"] as! String == "CHECK01") {
                    DispatchQueue.main.async {
                        completionHandler(response?["Checksum"] as? String, response?["Base64"] as? String,response?["apiEndPoints"] as? String)
                    }
                }else{
                    completionHandler(nil, nil, nil)

                    PKSAlertController.alert(Constants.appName, message: (response == nil)  ? "Server Error" : response?["message"] as! String)
                }
            })
        }else{
            PKSAlertController.alertForNetwok()

        }

    }

    @objc func  ReloadMenuView(){
        tableView.reloadData()
    }
    
    func SetDataProfile(){
        let dict_profile = Helper.getDataFromNsDefault(key: Constants.PROFILE) as! [String : String]
        headerView?.lblName.text = dict_profile["Name"] ?? ""
        headerView?.lblEmail.text = dict_profile["MobileNumber"] ?? ""
        if (dict_profile["ImageURL"]) != nil && (dict_profile["ImageURL"]) != ""{
            Server.toGetImageFromURL(url:  Constants.BASEURLImage + dict_profile["ImageURL"]!, completionHandler: { (image) in
                if image != nil{
                    DispatchQueue.main.async {
                        self.headerView?.imgViewProfile.image = image
                    }
                }
            })
        }
    }
    
    
    //Delegate method for update profile data in header
    func SetData(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    
    func btnlogoutPressed(){
        let alertController = UIAlertController.init(title: Constants.appName, message: "Are you sure, you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        let  yesButton = UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action:UIAlertAction) in
            UserDefaults.standard.removeObject(forKey: Constants.USERID)
            UserDefaults.standard.removeObject(forKey: Constants.PROFILE)
            UserDefaults.standard.removeObject(forKey: Constants.PASSWORD)
            self.hideSideMenuView()
            let root = Constants.mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            let navCtrl = UINavigationController.init(rootViewController: root)
            navCtrl.navigationBar.isHidden = true
            Constants.appDelegate.window?.rootViewController = navCtrl
            Constants.appDelegate.window?.makeKeyAndVisible()
            GIDSignIn.sharedInstance().signOut()
        })
        let noButton = UIAlertAction.init(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: { (action:UIAlertAction) in
            self.hideSideMenuView()
        })
        alertController.addAction(noButton)
        alertController.addAction(yesButton)
        self.present(alertController, animated: true, completion: {
        })
        
    }
    
    func logoutpressed(){
        UserDefaults.standard.set(false, forKey: Constants.isLoggedIn)
        UserDefaults.standard.synchronize()
        self.ResetDefaults()
        self.UpdateMenuView()
        hideSideMenuView()
        let destinationController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "Login")
        self.sideMenuController()?.setContentViewController(destinationController)
    }
    
    //view Profile Button Action
    
    @objc func viewProfileButton()  {
        let destViewController = Constants.mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        UserDefaults.standard.setValue("Edit1", forKey: "edit")
        
        if Constants.appDelegate.returnTopViewController().isKind(of: HomeDashBoardVC.classForCoder()){
            let home = Constants.appDelegate.returnTopViewController() as! HomeDashBoardVC
            home.hideSideMenuView()
            home.navigationController?.pushViewController(destViewController, animated: true)
        }
        else{
            sideMenuController()?.setContentViewController(destViewController)
        }
    }
    
    func ResetDefaults(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
}

class TableViewControllerSideMenu:UITableViewController{
    
    override init(style: UITableViewStyle) {
        super.init(style: .plain)
        print("called")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

