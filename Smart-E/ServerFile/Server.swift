//
//  Server.swift
//  TestSwiftApp
//
//  Created by Surbhi on 21/10/16.
//  Copyright © 2016 Neuronimbus. All rights reserved.
//

import UIKit
//import AFNetworking
//import Alamofire


class Server: NSObject {
    
    static func getRequestWithURL(urlString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>?) -> Void) {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "GET"
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
            
            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                //                Constants.appDelegate.stopIndicator()
                return;
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler(nil)
            }
        }
        task.resume()
    }
  
    static  func PostDataInDictionary(_ urlString: String, dict_data: Dictionary <String, AnyObject>, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>?) -> Void) {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.timeoutInterval = 90.0
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        print("=====",authValue)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict_data, options: [])
            //
            //all fine with jsonData here
            urlRequest.httpBody = jsonData
            print(jsonData)
            
        } catch {
            //handle error
            print(error)
        }
        
        //urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        print(urlRequest)
        
        
            
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            guard let data = data else {
                completionHandler(nil)
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    //  print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                completionHandler(nil)
            }
        })
        
        task.resume()
        
    }
    
    static func postRequestWithURL(_ urlString: String, paramString: String, completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void) {
        
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
       
        print(urlRequest)
        
       /* let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)

        // Handling Basic HTTPS Authorization
   /*     let authString = "wsbrandshop:Neuro@123"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")*/
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")*/
        
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
//        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in
//            
//            if error != nil {
//                print("Error occurred: "+(error?.localizedDescription)!)
//                //                Constants.appDelegate.stopIndicator()
//                return;
//            }
//            do {
//                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
//                completionHandler(responseObjc)
//            }
//            catch {
//                print("Error occurred parsing data: \(error)")
//                completionHandler([:])
//            }
//        }
        task.resume()
    }
    
    
    static func toGetImageFromURL(url:String,completionHandler:@escaping (_ response: UIImage?) -> Void){
        print(url)
        let session = URLSession(configuration: .default)
        let imageURL = session.dataTask(with: URL.init(string: url)!) { (data, response, error) in
            if let e = error {
                print("Error Occurred: \(e)")
            } else {
                if (response as? HTTPURLResponse) != nil {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            completionHandler(image)
                        }
                    } else {
                        completionHandler(nil)
                        print("Image file is currupted")
                    }
                } else {
                    completionHandler(nil)
                    print("No response from server")
                }
            }
        }
        imageURL.resume()
        
    }

    static func LoginToApp(_ Mobile : String, authType : String , authId : String , pass : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {

        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
        var paramString = String()

        paramString = "MobileNumber=\(Mobile)&Password=\(pass)"

        let loginMethod = "POST /api/PassengerAccount/AddMobile"
        let urlString = Constants.BASEURL+loginMethod

        let urlRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        urlRequest.timeoutInterval = 10000

        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString.data(using: String.Encoding.utf8)


        // Handling Basic HTTPS Authorization
        let authString = "Neuron:Neuro@4321"
        let authData = authString.data(using: String.Encoding.utf8)
        let authValue = "Basic \(authData!.base64EncodedString(options: NSData.Base64EncodingOptions.lineLength64Characters))"
        urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")

        let task = defaultSession.dataTask(with: urlRequest as URLRequest) { ( data, response, error) -> Void in

            if error != nil {
                print("Error occurred: "+(error?.localizedDescription)!)
                //                Constants.appDelegate.stopIndicator()
                return;
            }
            do {
                let responseObjc = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: AnyObject]
                completionHandler(responseObjc)
            }
            catch {
                print("Error occurred parsing data: \(error)")
                completionHandler([:])
            }
        }
        task.resume()
    }

  
    
    static func RegisterToApp(fname: String, lname : String,pass : String, channel: String, email : String, mobile: String, refferal : String , completionHandler:@escaping (_ response: Dictionary <String, AnyObject>) -> Void)
    {
        
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: nil)
      //  var paramString = String()
        let paramString1 = "firstName=\(fname)&lastName=\(lname)&password=\(pass)&channel=\(channel)&emailAddress=\(email)&mobilePrimary=\(mobile)&referalCode=\(refferal)"
        print(paramString1)
        let loginMethod = "Account/Register"
        let urlString = Constants.BASEURL+loginMethod
        
        
        let urlRequest = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        // Handling Basic HTTPS Authorization
       /* / let authString = "Neuron:Neuro@4321"
        
       // let authData = authString.data(using: String.Encoding.utf8)
        
        //let authData = authString.data(using: .utf8)
        //let authValue = "Basic \(String(describing: authData?.base64EncodedData(options: [])))"
        
      //  print("auth value is",authValue)
        //let authValue = "Basic \(authData!.base64EncodedStringWithOptions(NSData.Base64EncodingOptions.Encoding64CharacterLineLength))"
        //urlRequest.setValue(authValue, forHTTPHeaderField: "Authorization")
        */
      //
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = paramString1.data(using: String.Encoding.utf8)
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        print(urlRequest)
        
        
        let task = defaultSession.dataTask(with: urlRequest as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    completionHandler(json as Dictionary<String, AnyObject>)
                    // handle json...
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
     
        task.resume()
    }
  }



